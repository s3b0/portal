<?php
/**
 * Limits & pages for Db2Php
 *
 * @author Sebastian Krupa <sebo.poczta@wp.pl>
 */
class DPC {
    private $limit;
    private $offset;
    
    function __construct($offset, $limit=null) {
        if(is_null($limit) && !defined('RETURN_ROWS'))
            throw new Exception('Variable RETURN_ROWS is not defined - set $limit or define RETURN_ROWS!');
        elseif(is_null($limit))
            $limit = RETURN_ROWS;

        $this->limit = $limit;
        $this->offset = $offset;
    }
    public function getLimitsForQuery() {
        return " limit $this->limit offset $this->offset ";
    }
}

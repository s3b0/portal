<?php
/**
* Copyright (c) 2009, Andreas Schnaiter
*
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
* 3. Neither the name of  Andreas Schnaiter nor the names
*    of its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * single filter element to determine a criteria for matching a field
 */
class DFC implements DFCInterface {
	/**
	 * match exact value
	 */
	const EXACT=0;
	/**
	 * fields value contains
	 */
	const CONTAINS=1;
	/**
	 * fields value begins with
	 */
	const BEGINS_WITH=2;
	/**
	 * fields value ends with
	 */
	const ENDS_WITH=4;
	/**
	 * fields value is greater than
	 */
	const GREATER=8;
	/**
	 * fields value is smaller than
	 */
	const SMALLER=16;
	/**
	 * fields value matches wildcards ('.' and '*')
	 */
	const WILDCARDS=32;
	/**
	 * fields value is null
	 */
	const IS_NULL=64;
        /**
	 * fields value should be between parameter
	 */
	const BETWEEN=128;
        /**
	 * fields value should be between parameter
	 */
	const IN=256;
	/**
	 * negate condition.
	 * For example: DFC::NOT|DFC::CONTAINS will match field not containing the value
	 */
	const NOT=65536;
        const FALSE = 0;
        const TRUE = 1;
       /**
        * @var Logger standard logger
        */
        protected $logger;
	/**
	 * fields id
	 *
	 * @var int
	 */
	private $field;
	/**
	 * fields value
	 *
	 * @var string
	 */
	private $value;
        /**
	 * fields value for some operators (ex. BETWEEN, IN)
	 *
	 * @var array
	 */
	private $values;
	/**
	 * match mode
	 *
	 * @var int
	 */
	private $mode;
        /**
         *
         * @var array array of sql parameters identifieres
         */
        private $sqlParametersIdentifiers = array();

	/**
	 * CTOR
	 *
	 * @param int $field
	 * @param string $value
	 * @param int $mode
	 */
	function __construct($field, $value, $mode=0, ...$values) {
            if($mode == self::BETWEEN && count($values) != 1)
                throw new BadFunctionCallException('BETWEEN mode should has one value in $values array!');
            if($mode == self::IN && count($values) <= 0)
                throw new BadFunctionCallException('IN mode should has at least one value in $values array!');
            
            $this->values = $values;
            $this->field=$field;
            $this->value=$value;
            $this->mode=$mode;
            //$this->uniqId=uniqid();//MathUtil::md5Base36(uniqid(rand(), true));
            $this->logger = new Logger(get_class());
	}
        
        /**
         * get valuse for sql operator fullfilment (for example in BETWEEN mode)
         * @return array
         */
        public function getValues() {
            return $this->values;
        }
        /**
         * set values for sql operator fullfilment (for example in BETWEEN mode)
         * @param array $values
         */        
        public function setValues($values) {
            $this->values = $values;
        }

        /**
	 * get the fields id
	 *
	 * @return int
	 */
	public function getField() {
		return $this->field;
	}

	/**
	 * set the fields id
	 *
	 * @param int $field
	 */
	public function setField($field) {
		$this->field=$field;
	}

	/**
	 * get the fields value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * set the fields value
	 *
	 * @param string $value
	 */
	public function setValue($value) {
		$this->value=$value;
	}

	/**
	 * get the match mode
	 *
	 * @return int
	 */
	public function getMode() {
		return $this->mode;
	}

	/**
	 * set the match mode
	 *
	 * @param int $mode
	 */
	public function setMode($mode) {
		$this->mode=$mode;
	}
	/**
	 * get uniq id to identify filter
	 *
	 * @return string
	 */
	public function getUniqId() {
		return uniqid();
	}
	/**
	 * set uniq id - unused!
	 * 
	 * @param string $uniqId
	 */
	//public function setUniqId($uniqId) {
        //    throw new NotUsedException('Not used - unnecessary method!');
        //}

        function &getSqlParametersIdentifiers() {
            return $this->sqlParametersIdentifiers;
        }

        function setSqlParametersIdentifiers($sqlParameterIdentifier) {
            array_push($this->sqlParametersIdentifiers,  $sqlParameterIdentifier);
        }
	/**
	 * get SQL operator
	 *
	 * @return string
	 */
	public function getSqlOperator() {
		$mode=$this->getMode();
		$not=0!=(self::NOT&$mode);
		if(self::EXACT==$mode)
			return '=';
		elseif(self::NOT==$mode)
			return '!=';
		elseif(0!=(self::GREATER&$mode))
                {
                    if($not) 
                        return '<=';
                    return '>';
		} 
                elseif(0!=(self::SMALLER&$mode))  
                {
                    if($not) 
			return '>=';	
                    return '<';
		} 
                elseif(0!=((self::CONTAINS|self::BEGINS_WITH|self::ENDS_WITH|self::WILDCARDS)&$mode)) 
                {
                    if($not)
			return ' NOT LIKE ';	
                    return ' LIKE ';
		} 
                elseif (0!=(self::IS_NULL&$mode)) 
                {
                    if($not)
                        return ' IS NOT NULL';
                    return ' IS NULL';
		}
                elseif(self::BETWEEN == $mode) 
                {
                    if($not)
                        return ' NOT BETWEEN ';
                    return ' BETWEEN ';
		}
                elseif(self::IN == $mode) 
                {
                    $this->logger->logDebug('IN---------');
                    if($not)
                        return ' NOT IN ';
                    return ' IN ' ;
		}
		throw new UnexpectedValueException('can not handle mode:' . $mode);
	}
        
        /**
         * get sql fullfilment for some operators
         * @return string with values for some sql operators
         */
        private function fullfillSqlOperator() {
            $fullfilments = '';
            foreach($this->getValues() as $value)
                $fullfilments .= "$value,";
            return rtrim($fullfilments, ',');
        }

	/**
	 * get SQL operator and value assignment
	 *
	 * @return string
	 */
	public function getSqlOperatorPrepared() {
		if (0!=(self::IS_NULL&$this->getMode())) {
			return $this->getSqlOperator();
		}
		return $this->getSqlOperator() . '?';
	}

	/**
	 * get value for use in SQL
	 *
	 * @return string
	 */
	public function getSqlValue() {
		$mode=$this->getMode();
		if(0!=(self::CONTAINS&$mode))
                    return '%' . $this->getValue() . '%';
		elseif (0!=(self::BEGINS_WITH&$mode))
                    return $this->getValue() . '%';
		elseif (0!=(self::ENDS_WITH&$mode))
                    return '%' . $this->getValue();
		elseif (0!=(self::WILDCARDS&$mode))
                    return str_replace(array('.', '*'), array('_', '%'), $this->getValue());
		elseif (0!=(self::IS_NULL&$mode)) 
                    return null;
		return $this->getValue();
	}

	/**
	 * build sql WHERE statement
	 *
	 * @param Db2PhpEntity $entity
	 * @param bool $fullyQualifiedNames
	 * @param bool $prependWhere
	 * @return string
	 */
	public function buildSqlWhere(Db2PhpEntity $entity, $fullyQualifiedNames=true, $prependWhere=false) {
            if(!array_key_exists($this->getField(), $entity->getFieldNames()))
                return null;
            $sql = $entity->getFieldNameByFieldId($this->getField(), $fullyQualifiedNames).$this->getSqlOperator().$this->getSqlParameterIdentifier();
            if($prependWhere)
                $sql = " WHERE $sql";
            switch($this->getMode())
            {
                case(self::BETWEEN):
                    $sql .= ' AND '.$this->getSqlParameterIdentifier();//$this->fullfillSqlOperator();
                break;
                case(self::IN):
                    $sql .= ' ('.$this->getSqlParameterIdentifier().') ';
                break;
            }
            return $sql;
	}

	/**
	 * get identifier used in prepared statement
	 *
	 * @param Db2PhpEntity $entity
	 * @param bool $fullyQualifiedNames
	 * @return string
	 */
	private function getSqlParameterIdentifier() {
            if(DFC::IS_NULL != $this->mode && (DFC::IS_NULL|DFC::NOT) != $this->mode)
            {
                $sqlParameterIdentifier = ':DFC'.$this->getUniqId();
                $this->setSqlParametersIdentifiers($sqlParameterIdentifier);
                return $sqlParameterIdentifier;
            }
	}

	/**
	 * bind values to statement
	 *
	 * @param Db2PhpEntity $entity
	 * @param PDOStatement $stmt
	 */
    public function bindValuesForFilter(Db2PhpEntity $entity, PDOStatement &$stmt) {
        switch($this->getMode())
        {
            case DFC::IS_NULL:
            case DFC::NOT|DFC::IS_NULL:
                return;
            case(self::BETWEEN):
            case(self::IN):
                $identifier = array_pop($this->getSqlParametersIdentifiers());
                $this->logger->logDebug("$identifier = ".$this->fullfillSqlOperator());
                $stmt->bindValue($identifier, $this->fullfillSqlOperator());
            break;
        }
        $identifier = array_pop($this->getSqlParametersIdentifiers());
        $this->logger->logDebug("$identifier = ".$this->getSqlValue());
        $stmt->bindValue($identifier, $this->getSqlValue());
    }
}

?>
<?php
const TMP_DIR = '/tmp';
const SESSION_LIFETIME = 1440;
ini_set("session.use_cookies", 0);
ini_set("session.use_only_cookies", 0);
ini_set("session.use_trans_sid", 0);
ini_set("session.save_path", TMP_DIR);
ini_set('session.gc_maxlifetime', SESSION_LIFETIME); //czas życia sesji 24 minuty(odświeżenie przedłuża jej żywotność
ini_set('upload_tmp_dir', TMP_DIR); 

error_reporting(E_ALL);
const LOG_LEVEL = LOG_DEBUG;
setlocale(LC_ALL, array('pl_PL.UTF-8'));
//default galeries root
const GALLERIES_ROOT_CATALOG = '../photos/galleries/';
//defult galleries perms
const GALLERIES_CATALOG_PERMS = 0755;
const PHOTO_WATERMARK = 'CMS (ɔ)';
const ARTICLES_ROOT_CATALOG = '../articles/';
const ARTICLES_FILES_EXTENSION = '.html';


const TIMEZONE = 'Europe/Warsaw';
date_default_timezone_set(TIMEZONE);

const CAPTCHA_SIGNS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,-';
const CAPTCHA_LENGTH = 5;
const APP_VERSION = 'Version 0.1';
const PORTAL_NAME = 'cms';
const AREA_PARAMETER_NAME = 'area';
const OPERATION_PARAMETER_NAME = 'operation';
const OPTIONAL_PARAMETER_NAME = 'parameter';
//domyślna ilość wierszy jaka będzie zwracana do klienta (tabele html)
const DB_RETURN_ROWS_LIMIT = 20;
const DB_HOST = 'localhost';
const DB_PORT = 3306;
const DB_SCHEMA = 'cms';
const DB_USER = 'cms';
const DB_PASSWORD = 'cms';
/**
* @var string Ścieżka do logów
*/
const LOGS_ROOT_CATALOG = '/var/log/apache2/app/cms';
const LOG_DATE_FORMAT = 'd-m-Y G:i:s';
/**
* @var string Wzorzec wyjściowy linij wpisywaniej do logu
*/
const LOG_MSG_PATTERN = 'Wyjatek: %s w pliku %s w wierszu %d. Wiadomość: %s';
const TIMESTAMP_FORMAT = 'Y-m-d H:i:s';
const DATE_FORMAT = 'd-m-Y';
const PDO_DATE_FORMAT = 'Y-m-d';
const TIME_FORMAT = 'H:i';
const SQL_DATE_FORMAT = DATE_FORMAT;
const HOSTNAME = 'HOSTNAME';
const PROTOCOL = 'PROTOCOL';
const CONTEXT_ROOT = 'CONTEXT_ROOT';
const PORT = 'PORT';
const URI = 'URI';
unset($GLOBALS); //clear - just in case ;)
$GLOBALS[HOSTNAME] = $_SERVER['SERVER_NAME'];
$GLOBALS[PROTOCOL] = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
$GLOBALS[CONTEXT_ROOT] = $_SERVER['PHP_SELF'];
$GLOBALS[PORT] = $_SERVER['SERVER_PORT'];
$GLOBALS[URI] = $GLOBALS[PROTOCOL].$GLOBALS[HOSTNAME].':'.$GLOBALS[PORT].$GLOBALS[CONTEXT_ROOT];
/* @var array of voivodeships */
const VOIVODESHIPS = array(
    'POLAND' => array(
        2 => 'dolnośląskie',
        4 => 'kujawsko-pomorskie',
        6 => 'lubelskie', 
        8 => 'lubuskie',
        10 => 'łódzkie',	
        12 => 'małopolskie',
        14 => 'mazowieckie',
        16 => 'opolskie',
        18 => 'podkarpackie',
        20 => 'podlaskie',
        22 => 'pomorskie',
        24 => 'śląskie',
        26 => 'świętokrzyskie',
        28 => 'warmińsko-mazurskie',
        30 => 'wielkopolskie',
        32 => 'zachodniopomorskie')
);

const ALPHABET = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
const PASSWORD_SIGNS = ALPHABET.'!@#$%^&*()_+';
const PASSWORD_LENGHT = 8;
const PORTAL_CHARSET = 'UTF-8';
const SMTP_HOST = 'smtp.wp.pl';
const SMTP_USER = '';
const SMTP_PASSWORD = '';
const SMTP_PORT = '465';
const SMTP_TYPE = 'ssl';
const PORTAL_ADMIN_EMAIL_ADDRESS = '';
const PORTAL_DEFAULT_EMAIL_VARS = array('footer' => 'Pozdrowienia od ekipy portalu '.PORTAL_NAME, 'portal' => PORTAL_NAME);
const PORTAL_DEFAULT_EMAIL_SUBJECT = 'Wiadomość z portalu.';
/* ściezka do katalogu z templatkami */
const PORTAL_TEMPLATE_EMAIL_PATH = '../spare/mail/';

const ENCRYPT_METHOD = "AES-256-CBC";
const ENCRYPT_METHOD_SECRET_KEY = '3478475kdsjdksjdld@!@#!#!~$@%@^$#';
const ENCRYPT_METHOD_SECRET_IV = 'WWEWEE@!$#@#^#&%$#&#*%';

function __autoload($className) {
    $directory = new RecursiveDirectoryIterator('../webservices');
    $iterator = new RecursiveIteratorIterator($directory);
    foreach($iterator as $filename => $fileCursor)
    {
        $load = NULL;
        if(!$fileCursor->isDir() && (strpos($filename, "$className.php") || strpos($filename, str_replace('lsolesen\\pel\\', '', $className).'.php')))
            $load = $filename;
        if(!is_null($load))
        {
            require_once($load);   
            break;
        }
    }
}

function errorHandler($errno, $errstr, $errfile, $errline) {
    if(E_RECOVERABLE_ERROR === $errno || $errno == E_NOTICE || $errno == E_WARNING)//'catched' catchable fatal error
        throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    return false;
}
set_error_handler('errorHandler');

?>

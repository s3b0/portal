<?php
require_once 'config/PortalConfig.php';

/**
 * 
 *
 * @version 1.108
 * @package entity
 */
class AdsEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='AdsEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='ADS';
	const SQL_INSERT='INSERT INTO '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' %s VALUES %s';
	const SQL_UPDATE='UPDATE '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' SET %s  WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const FIELD_ID=1926214457;
	const FIELD_ADD_DATE=256214058;
	const FIELD_AD=1926214209;
	const FIELD_ADDER_IP=262170006;
	const FIELD_TYPE=-38459944;
	const FIELD_ACCEPTER=-654002349;
	const FIELD_ACCEPT_DATE=-1418185689;
	const FIELD_ACCEPTED=-654002363;
	const FIELD_TOKEN=-1192560869;
	const FIELD_ADDER_EMAIL=2052417741;
	const FIELD_ADDER_PHONE=2062441119;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_ADD_DATE=>'add_date',
		self::FIELD_AD=>'ad',
		self::FIELD_ADDER_IP=>'adder_ip',
		self::FIELD_TYPE=>'type',
		self::FIELD_ACCEPTER=>'accepter',
		self::FIELD_ACCEPT_DATE=>'accept_date',
		self::FIELD_ACCEPTED=>'accepted',
		self::FIELD_TOKEN=>'token',
		self::FIELD_ADDER_EMAIL=>'adder_email',
		self::FIELD_ADDER_PHONE=>'adder_phone');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_ADD_DATE=>'addDate',
		self::FIELD_AD=>'ad',
		self::FIELD_ADDER_IP=>'adderIp',
		self::FIELD_TYPE=>'type',
		self::FIELD_ACCEPTER=>'acceptEr',
		self::FIELD_ACCEPT_DATE=>'acceptDate',
		self::FIELD_ACCEPTED=>'accepted',
		self::FIELD_TOKEN=>'token',
		self::FIELD_ADDER_EMAIL=>'adderEmail',
		self::FIELD_ADDER_PHONE=>'adderPhone');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ADD_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_AD=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ADDER_IP=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_TYPE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ACCEPTER=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ACCEPT_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ACCEPTED=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_TOKEN=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ADDER_EMAIL=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ADDER_PHONE=>Db2PhpEntity::PHP_TYPE_STRING);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_ADD_DATE=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_AD=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,500,0,false),
		self::FIELD_ADDER_IP=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,false),
		self::FIELD_TYPE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,30,0,false),
		self::FIELD_ACCEPTER=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_ACCEPT_DATE=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,true),
		self::FIELD_ACCEPTED=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_TOKEN=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,40,0,false),
		self::FIELD_ADDER_EMAIL=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,50,0,true),
		self::FIELD_ADDER_PHONE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,50,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_ADD_DATE=>'CURRENT_TIMESTAMP',
		self::FIELD_AD=>'',
		self::FIELD_ADDER_IP=>'',
		self::FIELD_TYPE=>'',
		self::FIELD_ACCEPTER=>null,
		self::FIELD_ACCEPT_DATE=>null,
		self::FIELD_ACCEPTED=>'0',
		self::FIELD_TOKEN=>'',
		self::FIELD_ADDER_EMAIL=>null,
		self::FIELD_ADDER_PHONE=>null);
	private $id;
	private $addDate;
	private $ad;
	private $adderIp;
	private $type;
	private $acceptEr;
	private $acceptDate;
	private $accepted;
	private $token;
	private $adderEmail;
	private $adderPhone;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return AdsEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @param mixed $addDate
	 * @return AdsEntity
	 */
	public function &setAddDate($addDate) {
		$this->notifyChanged(self::FIELD_ADD_DATE,$this->addDate,$addDate);
		$this->addDate=$addDate;
		return $this;
	}

	/**
	 * get value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @return mixed
	 */
	public function getAddDate() {
		return $this->addDate;
	}

	/**
	 * set value for ad 
	 *
	 * type:VARCHAR,size:500,default:null
	 *
	 * @param mixed $ad
	 * @return AdsEntity
	 */
	public function &setAd($ad) {
		$this->notifyChanged(self::FIELD_AD,$this->ad,$ad);
		$this->ad=$ad;
		return $this;
	}

	/**
	 * get value for ad 
	 *
	 * type:VARCHAR,size:500,default:null
	 *
	 * @return mixed
	 */
	public function getAd() {
		return $this->ad;
	}

	/**
	 * set value for adder_ip 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @param mixed $adderIp
	 * @return AdsEntity
	 */
	public function &setAdderIp($adderIp) {
		$this->notifyChanged(self::FIELD_ADDER_IP,$this->adderIp,$adderIp);
		$this->adderIp=$adderIp;
		return $this;
	}

	/**
	 * get value for adder_ip 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @return mixed
	 */
	public function getAdderIp() {
		return $this->adderIp;
	}

	/**
	 * set value for type 
	 *
	 * type:VARCHAR,size:30,default:null,index
	 *
	 * @param mixed $type
	 * @return AdsEntity
	 */
	public function &setType($type) {
		$this->notifyChanged(self::FIELD_TYPE,$this->type,$type);
		$this->type=$type;
		return $this;
	}

	/**
	 * get value for type 
	 *
	 * type:VARCHAR,size:30,default:null,index
	 *
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * set value for accepter 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $acceptEr
	 * @return AdsEntity
	 */
	public function &setAcceptEr($acceptEr) {
		$this->notifyChanged(self::FIELD_ACCEPTER,$this->acceptEr,$acceptEr);
		$this->acceptEr=$acceptEr;
		return $this;
	}

	/**
	 * get value for accepter 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getAcceptEr() {
		return $this->acceptEr;
	}

	/**
	 * set value for accept_date 
	 *
	 * type:TIMESTAMP,size:19,default:null,nullable
	 *
	 * @param mixed $acceptDate
	 * @return AdsEntity
	 */
	public function &setAcceptDate($acceptDate) {
		$this->notifyChanged(self::FIELD_ACCEPT_DATE,$this->acceptDate,$acceptDate);
		$this->acceptDate=$acceptDate;
		return $this;
	}

	/**
	 * get value for accept_date 
	 *
	 * type:TIMESTAMP,size:19,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getAcceptDate() {
		return $this->acceptDate;
	}

	/**
	 * set value for accepted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $accepted
	 * @return AdsEntity
	 */
	public function &setAccepted($accepted) {
		$this->notifyChanged(self::FIELD_ACCEPTED,$this->accepted,$accepted);
		$this->accepted=$accepted;
		return $this;
	}

	/**
	 * get value for accepted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getAccepted() {
		return $this->accepted;
	}

	/**
	 * set value for token 
	 *
	 * type:VARCHAR,size:40,default:null
	 *
	 * @param mixed $token
	 * @return AdsEntity
	 */
	public function &setToken($token) {
		$this->notifyChanged(self::FIELD_TOKEN,$this->token,$token);
		$this->token=$token;
		return $this;
	}

	/**
	 * get value for token 
	 *
	 * type:VARCHAR,size:40,default:null
	 *
	 * @return mixed
	 */
	public function getToken() {
		return $this->token;
	}

	/**
	 * set value for adder_email 
	 *
	 * type:VARCHAR,size:50,default:null,nullable
	 *
	 * @param mixed $adderEmail
	 * @return AdsEntity
	 */
	public function &setAdderEmail($adderEmail) {
		$this->notifyChanged(self::FIELD_ADDER_EMAIL,$this->adderEmail,$adderEmail);
		$this->adderEmail=$adderEmail;
		return $this;
	}

	/**
	 * get value for adder_email 
	 *
	 * type:VARCHAR,size:50,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getAdderEmail() {
		return $this->adderEmail;
	}

	/**
	 * set value for adder_phone 
	 *
	 * type:VARCHAR,size:50,default:null,nullable
	 *
	 * @param mixed $adderPhone
	 * @return AdsEntity
	 */
	public function &setAdderPhone($adderPhone) {
		$this->notifyChanged(self::FIELD_ADDER_PHONE,$this->adderPhone,$adderPhone);
		$this->adderPhone=$adderPhone;
		return $this;
	}

	/**
	 * get value for adder_phone 
	 *
	 * type:VARCHAR,size:50,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getAdderPhone() {
		return $this->adderPhone;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_ADD_DATE=>$this->getAddDate(),
			self::FIELD_AD=>$this->getAd(),
			self::FIELD_ADDER_IP=>$this->getAdderIp(),
			self::FIELD_TYPE=>$this->getType(),
			self::FIELD_ACCEPTER=>$this->getAcceptEr(),
			self::FIELD_ACCEPT_DATE=>$this->getAcceptDate(),
			self::FIELD_ACCEPTED=>$this->getAccepted(),
			self::FIELD_TOKEN=>$this->getToken(),
			self::FIELD_ADDER_EMAIL=>$this->getAdderEmail(),
			self::FIELD_ADDER_PHONE=>$this->getAdderPhone());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (empty(self::$stmts[$statement][$dbInstanceId])) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}
	
	/**
	 * check if this instance exists in the database
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public function existsInDatabase(PDO $db) {
		$filter=array();
		foreach ($this->getPrimaryKeyValues() as $fieldId=>$value) {
			$filter[]=new DFC($fieldId, $value, DFC::EXACT);
		}
		return 0!=count(self::findByFilter($db, $filter, true));
	}
	
	/**
	 * Update to database if exists, otherwise insert
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateInsertToDatabase(PDO $db) {
		if ($this->existsInDatabase($db)) {
			return $this->updateToDatabase($db);
		} else {
			return $this->insertIntoDatabase($db);
		}
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of AdsEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param AdsEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return AdsEntity[]
	 */
	public static function findByExample(PDO $db,AdsEntity $example, $and=true, $sort=null, DPC $paginator=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort, $paginator);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of AdsEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return AdsEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null, DPC $paginator=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `ADS`'.self::buildSqlWhere($filter, $and, false, true);
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of AdsEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return AdsEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of AdsEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return AdsEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new AdsEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of AdsEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return AdsEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findBySql): $sql", get_called_class());
		return self::fromExecutedStatement($stmt);
	}
	
	/**
	 * Execute select query and return all rows as an array of AdsEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @return AdsEntity[]
	 */
	public static function getAll(PDO $db, $sort=null, DPC $paginator=null) {
		$sql='SELECT * FROM `ADS`';
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(getAll): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		return self::fromStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `ADS`'.self::buildSqlWhere($filter, $and, false, true);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(deleteByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		$stmt->closeCursor();
		if ($affected === false) 
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		return $stmt->rowCount();
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setAddDate($result['add_date']);
		$this->setAd($result['ad']);
		$this->setAdderIp($result['adder_ip']);
		$this->setType($result['type']);
		$this->setAcceptEr($result['accepter']);
		$this->setAcceptDate($result['accept_date']);
		$this->setAccepted($result['accepted']);
		$this->setToken($result['token']);
		$this->setAdderEmail($result['adder_email']);
		$this->setAdderPhone($result['adder_phone']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return AdsEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new AdsEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getAddDate());
		$stmt->bindValue(3,$this->getAd());
		$stmt->bindValue(4,$this->getAdderIp());
		$stmt->bindValue(5,$this->getType());
		$stmt->bindValue(6,$this->getAcceptEr());
		$stmt->bindValue(7,$this->getAcceptDate());
		$stmt->bindValue(8,$this->getAccepted());
		$stmt->bindValue(9,$this->getToken());
		$stmt->bindValue(10,$this->getAdderEmail());
		$stmt->bindValue(11,$this->getAdderPhone());
	}
	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		$columns = '(';
		$values = '(';
		foreach($this->toArray() as $fieldId => $value)
		{
			if($value !== NULL && self::$DEFAULT_VALUES[$fieldId] !== $value)
			{
				$columns .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.',';
                                if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                    $values .= "$value,";
                                else
                                    $values .= "'$value',";
			}
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$columns = rtrim($columns, ',') . ')';
		$values = rtrim($values, ',') . ')';                
		$sql = sprintf(self::SQL_INSERT, $columns, $values);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(insertIntoDatabase): $sql", get_called_class());
		$stmt = self::prepareStatement($db, $sql);        
		$affected=$stmt->execute();
		if(false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		//if ($lastInsertId !== false)
		if ($lastInsertId != false) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$values = '';
                foreach($this->toArray() as $fieldId => $value)
		{
                    if(self::$DEFAULT_VALUES[$fieldId] !== $value || $value === NULL)
                    {
			$values .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.'=';
                            if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                $values .= "$value,";
                            elseif($value === NULL)
                                $values .= "NULL,";
                            else
                                $values .= "'$value',";
                    }
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$values = rtrim($values, ','); 
                $sql = sprintf(self::SQL_UPDATE, $values);
		$stmt=self::prepareStatement($db, $sql);
                Logger::LogMessage(LOG_DEBUG, "SQL Query(updateToDatabase): $sql", get_called_class());
		$stmt->bindValue(1,$this->getOldInstance()->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch UsersEntity which references this AdsEntity. Will return null in case reference is invalid.
	 * `ADS`.`accepter` -> `USERS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity
	 */
	public function fetchUsersEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ID=>$this->getAcceptEr());
		$result=UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch AdsTypesDictEntity which references this AdsEntity. Will return null in case reference is invalid.
	 * `ADS`.`type` -> `ADS_TYPES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return AdsTypesDictEntity
	 */
	public function fetchAdsTypesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(AdsTypesDictEntity::FIELD_NAME=>$this->getType());
		$result=AdsTypesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'AdsEntity');
	}

	/**
	 * get single AdsEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return AdsEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new AdsEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of AdsEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return AdsEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('AdsEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>
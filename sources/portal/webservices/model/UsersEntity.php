<?php
require_once 'config/PortalConfig.php';

/**
 * 
 *
 * @version 1.108
 * @package entity
 */
class UsersEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='UsersEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='USERS';
	const SQL_INSERT='INSERT INTO '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' %s VALUES %s';
	const SQL_UPDATE='UPDATE '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' SET %s  WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const FIELD_ID=517044961;
	const FIELD_NAME=-1335849263;
	const FIELD_LOGIN=1536910307;
	const FIELD_EMAIL=1530380310;
	const FIELD_ACTIVE=73963980;
	const FIELD_AVATAR=90954783;
	const FIELD_ROLE=-1335716676;
	const FIELD_PASSWORD=937901025;
	const FIELD_PERSONAL_DATA_ACCEPTED=-2140817501;
	const FIELD_ADD_DATE=-1515393582;
	const FIELD_ADDER=1526420872;
	const FIELD_DELETABLE=-420517426;
	const FIELD_HIDDEN=279427184;
	const FIELD_ADDRESS=-1987978770;
	const FIELD_NOTES=1538769723;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_NAME=>'name',
		self::FIELD_LOGIN=>'login',
		self::FIELD_EMAIL=>'email',
		self::FIELD_ACTIVE=>'active',
		self::FIELD_AVATAR=>'avatar',
		self::FIELD_ROLE=>'role',
		self::FIELD_PASSWORD=>'password',
		self::FIELD_PERSONAL_DATA_ACCEPTED=>'personal_data_accepted',
		self::FIELD_ADD_DATE=>'add_date',
		self::FIELD_ADDER=>'adder',
		self::FIELD_DELETABLE=>'deletable',
		self::FIELD_HIDDEN=>'hidden',
		self::FIELD_ADDRESS=>'address',
		self::FIELD_NOTES=>'notes');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_NAME=>'name',
		self::FIELD_LOGIN=>'logIn',
		self::FIELD_EMAIL=>'email',
		self::FIELD_ACTIVE=>'active',
		self::FIELD_AVATAR=>'avatar',
		self::FIELD_ROLE=>'role',
		self::FIELD_PASSWORD=>'password',
		self::FIELD_PERSONAL_DATA_ACCEPTED=>'personalDataAccepted',
		self::FIELD_ADD_DATE=>'addDate',
		self::FIELD_ADDER=>'adder',
		self::FIELD_DELETABLE=>'deletable',
		self::FIELD_HIDDEN=>'hidden',
		self::FIELD_ADDRESS=>'address',
		self::FIELD_NOTES=>'notes');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_NAME=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_LOGIN=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_EMAIL=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ACTIVE=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_AVATAR=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ROLE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_PASSWORD=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_PERSONAL_DATA_ACCEPTED=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_ADD_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ADDER=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_DELETABLE=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_HIDDEN=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_ADDRESS=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_NOTES=>Db2PhpEntity::PHP_TYPE_STRING);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_NAME=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,false),
		self::FIELD_LOGIN=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,false),
		self::FIELD_EMAIL=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,false),
		self::FIELD_ACTIVE=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_AVATAR=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,true),
		self::FIELD_ROLE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,7,0,false),
		self::FIELD_PASSWORD=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,32,0,false),
		self::FIELD_PERSONAL_DATA_ACCEPTED=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_ADD_DATE=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_ADDER=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_DELETABLE=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_HIDDEN=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_ADDRESS=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,500,0,true),
		self::FIELD_NOTES=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,500,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_NAME=>'',
		self::FIELD_LOGIN=>'',
		self::FIELD_EMAIL=>'',
		self::FIELD_ACTIVE=>'1',
		self::FIELD_AVATAR=>null,
		self::FIELD_ROLE=>'',
		self::FIELD_PASSWORD=>'',
		self::FIELD_PERSONAL_DATA_ACCEPTED=>'0',
		self::FIELD_ADD_DATE=>'CURRENT_TIMESTAMP',
		self::FIELD_ADDER=>0,
		self::FIELD_DELETABLE=>'1',
		self::FIELD_HIDDEN=>'0',
		self::FIELD_ADDRESS=>null,
		self::FIELD_NOTES=>null);
	private $id;
	private $name;
	private $logIn;
	private $email;
	private $active;
	private $avatar;
	private $role;
	private $password;
	private $personalDataAccepted;
	private $addDate;
	private $adder;
	private $deletable;
	private $hidden;
	private $address;
	private $notes;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return UsersEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for name 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @param mixed $name
	 * @return UsersEntity
	 */
	public function &setName($name) {
		$this->notifyChanged(self::FIELD_NAME,$this->name,$name);
		$this->name=$name;
		return $this;
	}

	/**
	 * get value for name 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * set value for login 
	 *
	 * type:VARCHAR,size:45,default:null,unique
	 *
	 * @param mixed $logIn
	 * @return UsersEntity
	 */
	public function &setLogIn($logIn) {
		$this->notifyChanged(self::FIELD_LOGIN,$this->logIn,$logIn);
		$this->logIn=$logIn;
		return $this;
	}

	/**
	 * get value for login 
	 *
	 * type:VARCHAR,size:45,default:null,unique
	 *
	 * @return mixed
	 */
	public function getLogIn() {
		return $this->logIn;
	}

	/**
	 * set value for email 
	 *
	 * type:VARCHAR,size:45,default:null,unique
	 *
	 * @param mixed $email
	 * @return UsersEntity
	 */
	public function &setEmail($email) {
		$this->notifyChanged(self::FIELD_EMAIL,$this->email,$email);
		$this->email=$email;
		return $this;
	}

	/**
	 * get value for email 
	 *
	 * type:VARCHAR,size:45,default:null,unique
	 *
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * set value for active 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @param mixed $active
	 * @return UsersEntity
	 */
	public function &setActive($active) {
		$this->notifyChanged(self::FIELD_ACTIVE,$this->active,$active);
		$this->active=$active;
		return $this;
	}

	/**
	 * get value for active 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @return mixed
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * set value for avatar 
	 *
	 * type:VARCHAR,size:255,default:null,nullable
	 *
	 * @param mixed $avatar
	 * @return UsersEntity
	 */
	public function &setAvatar($avatar) {
		$this->notifyChanged(self::FIELD_AVATAR,$this->avatar,$avatar);
		$this->avatar=$avatar;
		return $this;
	}

	/**
	 * get value for avatar 
	 *
	 * type:VARCHAR,size:255,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getAvatar() {
		return $this->avatar;
	}

	/**
	 * set value for role 
	 *
	 * type:VARCHAR,size:7,default:null,index
	 *
	 * @param mixed $role
	 * @return UsersEntity
	 */
	public function &setRole($role) {
		$this->notifyChanged(self::FIELD_ROLE,$this->role,$role);
		$this->role=$role;
		return $this;
	}

	/**
	 * get value for role 
	 *
	 * type:VARCHAR,size:7,default:null,index
	 *
	 * @return mixed
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * set value for password 
	 *
	 * type:VARCHAR,size:32,default:null
	 *
	 * @param mixed $password
	 * @return UsersEntity
	 */
	public function &setPassword($password) {
		$this->notifyChanged(self::FIELD_PASSWORD,$this->password,$password);
		$this->password=$password;
		return $this;
	}

	/**
	 * get value for password 
	 *
	 * type:VARCHAR,size:32,default:null
	 *
	 * @return mixed
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * set value for personal_data_accepted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $personalDataAccepted
	 * @return UsersEntity
	 */
	public function &setPersonalDataAccepted($personalDataAccepted) {
		$this->notifyChanged(self::FIELD_PERSONAL_DATA_ACCEPTED,$this->personalDataAccepted,$personalDataAccepted);
		$this->personalDataAccepted=$personalDataAccepted;
		return $this;
	}

	/**
	 * get value for personal_data_accepted 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getPersonalDataAccepted() {
		return $this->personalDataAccepted;
	}

	/**
	 * set value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @param mixed $addDate
	 * @return UsersEntity
	 */
	public function &setAddDate($addDate) {
		$this->notifyChanged(self::FIELD_ADD_DATE,$this->addDate,$addDate);
		$this->addDate=$addDate;
		return $this;
	}

	/**
	 * get value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @return mixed
	 */
	public function getAddDate() {
		return $this->addDate;
	}

	/**
	 * set value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @param mixed $adder
	 * @return UsersEntity
	 */
	public function &setAdder($adder) {
		$this->notifyChanged(self::FIELD_ADDER,$this->adder,$adder);
		$this->adder=$adder;
		return $this;
	}

	/**
	 * get value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index
	 *
	 * @return mixed
	 */
	public function getAdder() {
		return $this->adder;
	}

	/**
	 * set value for deletable 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @param mixed $deletable
	 * @return UsersEntity
	 */
	public function &setDeletable($deletable) {
		$this->notifyChanged(self::FIELD_DELETABLE,$this->deletable,$deletable);
		$this->deletable=$deletable;
		return $this;
	}

	/**
	 * get value for deletable 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @return mixed
	 */
	public function getDeletable() {
		return $this->deletable;
	}

	/**
	 * set value for hidden 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $hidden
	 * @return UsersEntity
	 */
	public function &setHidden($hidden) {
		$this->notifyChanged(self::FIELD_HIDDEN,$this->hidden,$hidden);
		$this->hidden=$hidden;
		return $this;
	}

	/**
	 * get value for hidden 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * set value for address 
	 *
	 * type:VARCHAR,size:500,default:null,nullable
	 *
	 * @param mixed $address
	 * @return UsersEntity
	 */
	public function &setAddress($address) {
		$this->notifyChanged(self::FIELD_ADDRESS,$this->address,$address);
		$this->address=$address;
		return $this;
	}

	/**
	 * get value for address 
	 *
	 * type:VARCHAR,size:500,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * set value for notes 
	 *
	 * type:VARCHAR,size:500,default:null,nullable
	 *
	 * @param mixed $notes
	 * @return UsersEntity
	 */
	public function &setNotes($notes) {
		$this->notifyChanged(self::FIELD_NOTES,$this->notes,$notes);
		$this->notes=$notes;
		return $this;
	}

	/**
	 * get value for notes 
	 *
	 * type:VARCHAR,size:500,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getNotes() {
		return $this->notes;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_NAME=>$this->getName(),
			self::FIELD_LOGIN=>$this->getLogIn(),
			self::FIELD_EMAIL=>$this->getEmail(),
			self::FIELD_ACTIVE=>$this->getActive(),
			self::FIELD_AVATAR=>$this->getAvatar(),
			self::FIELD_ROLE=>$this->getRole(),
			self::FIELD_PASSWORD=>$this->getPassword(),
			self::FIELD_PERSONAL_DATA_ACCEPTED=>$this->getPersonalDataAccepted(),
			self::FIELD_ADD_DATE=>$this->getAddDate(),
			self::FIELD_ADDER=>$this->getAdder(),
			self::FIELD_DELETABLE=>$this->getDeletable(),
			self::FIELD_HIDDEN=>$this->getHidden(),
			self::FIELD_ADDRESS=>$this->getAddress(),
			self::FIELD_NOTES=>$this->getNotes());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (empty(self::$stmts[$statement][$dbInstanceId])) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}
	
	/**
	 * check if this instance exists in the database
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public function existsInDatabase(PDO $db) {
		$filter=array();
		foreach ($this->getPrimaryKeyValues() as $fieldId=>$value) {
			$filter[]=new DFC($fieldId, $value, DFC::EXACT);
		}
		return 0!=count(self::findByFilter($db, $filter, true));
	}
	
	/**
	 * Update to database if exists, otherwise insert
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateInsertToDatabase(PDO $db) {
		if ($this->existsInDatabase($db)) {
			return $this->updateToDatabase($db);
		} else {
			return $this->insertIntoDatabase($db);
		}
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of UsersEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param UsersEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return UsersEntity[]
	 */
	public static function findByExample(PDO $db,UsersEntity $example, $and=true, $sort=null, DPC $paginator=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort, $paginator);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of UsersEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return UsersEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null, DPC $paginator=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `USERS`'.self::buildSqlWhere($filter, $and, false, true);
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of UsersEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return UsersEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of UsersEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return UsersEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new UsersEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of UsersEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return UsersEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findBySql): $sql", get_called_class());
		return self::fromExecutedStatement($stmt);
	}
	
	/**
	 * Execute select query and return all rows as an array of UsersEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @return UsersEntity[]
	 */
	public static function getAll(PDO $db, $sort=null, DPC $paginator=null) {
		$sql='SELECT * FROM `USERS`';
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(getAll): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		return self::fromStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `USERS`'.self::buildSqlWhere($filter, $and, false, true);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(deleteByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		$stmt->closeCursor();
		if ($affected === false) 
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		return $stmt->rowCount();
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setName($result['name']);
		$this->setLogIn($result['login']);
		$this->setEmail($result['email']);
		$this->setActive($result['active']);
		$this->setAvatar($result['avatar']);
		$this->setRole($result['role']);
		$this->setPassword($result['password']);
		$this->setPersonalDataAccepted($result['personal_data_accepted']);
		$this->setAddDate($result['add_date']);
		$this->setAdder($result['adder']);
		$this->setDeletable($result['deletable']);
		$this->setHidden($result['hidden']);
		$this->setAddress($result['address']);
		$this->setNotes($result['notes']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return UsersEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new UsersEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getName());
		$stmt->bindValue(3,$this->getLogIn());
		$stmt->bindValue(4,$this->getEmail());
		$stmt->bindValue(5,$this->getActive());
		$stmt->bindValue(6,$this->getAvatar());
		$stmt->bindValue(7,$this->getRole());
		$stmt->bindValue(8,$this->getPassword());
		$stmt->bindValue(9,$this->getPersonalDataAccepted());
		$stmt->bindValue(10,$this->getAddDate());
		$stmt->bindValue(11,$this->getAdder());
		$stmt->bindValue(12,$this->getDeletable());
		$stmt->bindValue(13,$this->getHidden());
		$stmt->bindValue(14,$this->getAddress());
		$stmt->bindValue(15,$this->getNotes());
	}
	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		$columns = '(';
		$values = '(';
		foreach($this->toArray() as $fieldId => $value)
		{
			if($value !== NULL && self::$DEFAULT_VALUES[$fieldId] !== $value)
			{
				$columns .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.',';
                                if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                    $values .= "$value,";
                                else
                                    $values .= "'$value',";
			}
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$columns = rtrim($columns, ',') . ')';
		$values = rtrim($values, ',') . ')';                
		$sql = sprintf(self::SQL_INSERT, $columns, $values);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(insertIntoDatabase): $sql", get_called_class());
		$stmt = self::prepareStatement($db, $sql);        
		$affected=$stmt->execute();
		if(false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		//if ($lastInsertId !== false)
		if ($lastInsertId != false) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$values = '';
                foreach($this->toArray() as $fieldId => $value)
		{
                    if(self::$DEFAULT_VALUES[$fieldId] !== $value || $value === NULL)
                    {
			$values .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.'=';
                            if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                $values .= "$value,";
                            elseif($value === NULL)
                                $values .= "NULL,";
                            else
                                $values .= "'$value',";
                    }
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$values = rtrim($values, ','); 
                $sql = sprintf(self::SQL_UPDATE, $values);
		$stmt=self::prepareStatement($db, $sql);
                Logger::LogMessage(LOG_DEBUG, "SQL Query(updateToDatabase): $sql", get_called_class());
		$stmt->bindValue(1,$this->getOldInstance()->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch AdsEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `ADS`.`accepter`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return AdsEntity[]
	 */
	public function fetchAdsEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(AdsEntity::FIELD_ACCEPTER=>$this->getId());
		return AdsEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch EventsEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `EVENTS`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return EventsEntity[]
	 */
	public function fetchEventsEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(EventsEntity::FIELD_ADDER=>$this->getId());
		return EventsEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch GroupsEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `GROUPS`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return GroupsEntity[]
	 */
	public function fetchGroupsEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(GroupsEntity::FIELD_ADDER=>$this->getId());
		return GroupsEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch GroupRolesOfUserEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `GROUP_ROLES_OF_USER`.`user`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return GroupRolesOfUserEntity[]
	 */
	public function fetchGroupRolesOfUserEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(GroupRolesOfUserEntity::FIELD_USER=>$this->getId());
		return GroupRolesOfUserEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsArticlesEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `LEAFS_ARTICLES`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsArticlesEntity[]
	 */
	public function fetchLeafsArticlesEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsArticlesEntity::FIELD_ADDER=>$this->getId());
		return LeafsArticlesEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsGalleriesEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `LEAFS_GALLERIES`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsGalleriesEntity[]
	 */
	public function fetchLeafsGalleriesEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsGalleriesEntity::FIELD_ADDER=>$this->getId());
		return LeafsGalleriesEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsPostsEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `LEAFS_POSTS`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsPostsEntity[]
	 */
	public function fetchLeafsPostsEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsPostsEntity::FIELD_ADDER=>$this->getId());
		return LeafsPostsEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsStaticEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `LEAFS_STATIC`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsStaticEntity[]
	 */
	public function fetchLeafsStaticEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsStaticEntity::FIELD_ADDER=>$this->getId());
		return LeafsStaticEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch MenuEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `MENU`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuEntity[]
	 */
	public function fetchMenuEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuEntity::FIELD_ADDER=>$this->getId());
		return MenuEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch UsersEntity's which this UsersEntity references.
	 * `USERS`.`id` -> `USERS`.`adder`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity[]
	 */
	public function fetchUsersEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ADDER=>$this->getId());
		return UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch UsersEntity which references this UsersEntity. Will return null in case reference is invalid.
	 * `USERS`.`adder` -> `USERS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity
	 */
	public function fetchUsersEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ID=>$this->getAdder());
		$result=UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch UsersRolesDictEntity which references this UsersEntity. Will return null in case reference is invalid.
	 * `USERS`.`role` -> `USERS_ROLES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersRolesDictEntity
	 */
	public function fetchUsersRolesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersRolesDictEntity::FIELD_NAME=>$this->getRole());
		$result=UsersRolesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'UsersEntity');
	}

	/**
	 * get single UsersEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return UsersEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new UsersEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of UsersEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return UsersEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('UsersEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>
<?php
require_once 'config/PortalConfig.php';

/**
 * 
 *
 * @version 1.108
 * @package entity
 */
class LeafsGalleriesEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='LeafsGalleriesEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='LEAFS_GALLERIES';
	const SQL_INSERT='INSERT INTO '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' %s VALUES %s';
	const SQL_UPDATE='UPDATE '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' SET %s  WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const FIELD_ID=-1693347901;
	const FIELD_DESCRIPTION=2095611476;
	const FIELD_PHOTOS_DIR=-366514569;
	const FIELD_THUMBNAILS_DIR=-952669187;
	const FIELD_LEAF=485365766;
	const FIELD_TYPE=485623778;
	const FIELD_GROUP=-2137746729;
	const FIELD_ADDER=-2143715994;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_DESCRIPTION=>'description',
		self::FIELD_PHOTOS_DIR=>'photos_dir',
		self::FIELD_THUMBNAILS_DIR=>'thumbnails_dir',
		self::FIELD_LEAF=>'leaf',
		self::FIELD_TYPE=>'type',
		self::FIELD_GROUP=>'group',
		self::FIELD_ADDER=>'adder');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_DESCRIPTION=>'description',
		self::FIELD_PHOTOS_DIR=>'photosDir',
		self::FIELD_THUMBNAILS_DIR=>'thumbnailsDir',
		self::FIELD_LEAF=>'leaf',
		self::FIELD_TYPE=>'type',
		self::FIELD_GROUP=>'group',
		self::FIELD_ADDER=>'adder');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_DESCRIPTION=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_PHOTOS_DIR=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_THUMBNAILS_DIR=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_LEAF=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_TYPE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_GROUP=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ADDER=>Db2PhpEntity::PHP_TYPE_INT);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_DESCRIPTION=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,true),
		self::FIELD_PHOTOS_DIR=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_THUMBNAILS_DIR=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_LEAF=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_TYPE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,true),
		self::FIELD_GROUP=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_ADDER=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_DESCRIPTION=>null,
		self::FIELD_PHOTOS_DIR=>'',
		self::FIELD_THUMBNAILS_DIR=>'',
		self::FIELD_LEAF=>0,
		self::FIELD_TYPE=>'INTERNAL',
		self::FIELD_GROUP=>null,
		self::FIELD_ADDER=>null);
	private $id;
	private $description;
	private $photosDir;
	private $thumbnailsDir;
	private $leaf;
	private $type;
	private $group;
	private $adder;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return LeafsGalleriesEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for description 
	 *
	 * type:VARCHAR,size:255,default:null,nullable
	 *
	 * @param mixed $description
	 * @return LeafsGalleriesEntity
	 */
	public function &setDescription($description) {
		$this->notifyChanged(self::FIELD_DESCRIPTION,$this->description,$description);
		$this->description=$description;
		return $this;
	}

	/**
	 * get value for description 
	 *
	 * type:VARCHAR,size:255,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * set value for photos_dir 
	 *
	 * type:VARCHAR,size:255,default:null,unique
	 *
	 * @param mixed $photosDir
	 * @return LeafsGalleriesEntity
	 */
	public function &setPhotosDir($photosDir) {
		$this->notifyChanged(self::FIELD_PHOTOS_DIR,$this->photosDir,$photosDir);
		$this->photosDir=$photosDir;
		return $this;
	}

	/**
	 * get value for photos_dir 
	 *
	 * type:VARCHAR,size:255,default:null,unique
	 *
	 * @return mixed
	 */
	public function getPhotosDir() {
		return $this->photosDir;
	}

	/**
	 * set value for thumbnails_dir 
	 *
	 * type:VARCHAR,size:255,default:null,unique
	 *
	 * @param mixed $thumbnailsDir
	 * @return LeafsGalleriesEntity
	 */
	public function &setThumbnailsDir($thumbnailsDir) {
		$this->notifyChanged(self::FIELD_THUMBNAILS_DIR,$this->thumbnailsDir,$thumbnailsDir);
		$this->thumbnailsDir=$thumbnailsDir;
		return $this;
	}

	/**
	 * get value for thumbnails_dir 
	 *
	 * type:VARCHAR,size:255,default:null,unique
	 *
	 * @return mixed
	 */
	public function getThumbnailsDir() {
		return $this->thumbnailsDir;
	}

	/**
	 * set value for leaf 
	 *
	 * type:INT UNSIGNED,size:10,default:null,unique
	 *
	 * @param mixed $leaf
	 * @return LeafsGalleriesEntity
	 */
	public function &setLeaf($leaf) {
		$this->notifyChanged(self::FIELD_LEAF,$this->leaf,$leaf);
		$this->leaf=$leaf;
		return $this;
	}

	/**
	 * get value for leaf 
	 *
	 * type:INT UNSIGNED,size:10,default:null,unique
	 *
	 * @return mixed
	 */
	public function getLeaf() {
		return $this->leaf;
	}

	/**
	 * set value for type 
	 *
	 * type:VARCHAR,size:45,default:INTERNAL,index,nullable
	 *
	 * @param mixed $type
	 * @return LeafsGalleriesEntity
	 */
	public function &setType($type) {
		$this->notifyChanged(self::FIELD_TYPE,$this->type,$type);
		$this->type=$type;
		return $this;
	}

	/**
	 * get value for type 
	 *
	 * type:VARCHAR,size:45,default:INTERNAL,index,nullable
	 *
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * set value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $group
	 * @return LeafsGalleriesEntity
	 */
	public function &setGroup($group) {
		$this->notifyChanged(self::FIELD_GROUP,$this->group,$group);
		$this->group=$group;
		return $this;
	}

	/**
	 * get value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getGroup() {
		return $this->group;
	}

	/**
	 * set value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $adder
	 * @return LeafsGalleriesEntity
	 */
	public function &setAdder($adder) {
		$this->notifyChanged(self::FIELD_ADDER,$this->adder,$adder);
		$this->adder=$adder;
		return $this;
	}

	/**
	 * get value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getAdder() {
		return $this->adder;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_DESCRIPTION=>$this->getDescription(),
			self::FIELD_PHOTOS_DIR=>$this->getPhotosDir(),
			self::FIELD_THUMBNAILS_DIR=>$this->getThumbnailsDir(),
			self::FIELD_LEAF=>$this->getLeaf(),
			self::FIELD_TYPE=>$this->getType(),
			self::FIELD_GROUP=>$this->getGroup(),
			self::FIELD_ADDER=>$this->getAdder());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (empty(self::$stmts[$statement][$dbInstanceId])) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}
	
	/**
	 * check if this instance exists in the database
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public function existsInDatabase(PDO $db) {
		$filter=array();
		foreach ($this->getPrimaryKeyValues() as $fieldId=>$value) {
			$filter[]=new DFC($fieldId, $value, DFC::EXACT);
		}
		return 0!=count(self::findByFilter($db, $filter, true));
	}
	
	/**
	 * Update to database if exists, otherwise insert
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateInsertToDatabase(PDO $db) {
		if ($this->existsInDatabase($db)) {
			return $this->updateToDatabase($db);
		} else {
			return $this->insertIntoDatabase($db);
		}
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of LeafsGalleriesEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param LeafsGalleriesEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return LeafsGalleriesEntity[]
	 */
	public static function findByExample(PDO $db,LeafsGalleriesEntity $example, $and=true, $sort=null, DPC $paginator=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort, $paginator);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of LeafsGalleriesEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return LeafsGalleriesEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null, DPC $paginator=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `LEAFS_GALLERIES`'.self::buildSqlWhere($filter, $and, false, true);
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of LeafsGalleriesEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return LeafsGalleriesEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of LeafsGalleriesEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return LeafsGalleriesEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new LeafsGalleriesEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of LeafsGalleriesEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return LeafsGalleriesEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findBySql): $sql", get_called_class());
		return self::fromExecutedStatement($stmt);
	}
	
	/**
	 * Execute select query and return all rows as an array of LeafsGalleriesEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @return LeafsGalleriesEntity[]
	 */
	public static function getAll(PDO $db, $sort=null, DPC $paginator=null) {
		$sql='SELECT * FROM `LEAFS_GALLERIES`';
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(getAll): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		return self::fromStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `LEAFS_GALLERIES`'.self::buildSqlWhere($filter, $and, false, true);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(deleteByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		$stmt->closeCursor();
		if ($affected === false) 
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		return $stmt->rowCount();
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setDescription($result['description']);
		$this->setPhotosDir($result['photos_dir']);
		$this->setThumbnailsDir($result['thumbnails_dir']);
		$this->setLeaf($result['leaf']);
		$this->setType($result['type']);
		$this->setGroup($result['group']);
		$this->setAdder($result['adder']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return LeafsGalleriesEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new LeafsGalleriesEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getDescription());
		$stmt->bindValue(3,$this->getPhotosDir());
		$stmt->bindValue(4,$this->getThumbnailsDir());
		$stmt->bindValue(5,$this->getLeaf());
		$stmt->bindValue(6,$this->getType());
		$stmt->bindValue(7,$this->getGroup());
		$stmt->bindValue(8,$this->getAdder());
	}
	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		$columns = '(';
		$values = '(';
		foreach($this->toArray() as $fieldId => $value)
		{
			if($value !== NULL && self::$DEFAULT_VALUES[$fieldId] !== $value)
			{
				$columns .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.',';
                                if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                    $values .= "$value,";
                                else
                                    $values .= "'$value',";
			}
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$columns = rtrim($columns, ',') . ')';
		$values = rtrim($values, ',') . ')';                
		$sql = sprintf(self::SQL_INSERT, $columns, $values);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(insertIntoDatabase): $sql", get_called_class());
		$stmt = self::prepareStatement($db, $sql);        
		$affected=$stmt->execute();
		if(false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		//if ($lastInsertId !== false)
		if ($lastInsertId != false) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$values = '';
                foreach($this->toArray() as $fieldId => $value)
		{
                    if(self::$DEFAULT_VALUES[$fieldId] !== $value || $value === NULL)
                    {
			$values .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.'=';
                            if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                $values .= "$value,";
                            elseif($value === NULL)
                                $values .= "NULL,";
                            else
                                $values .= "'$value',";
                    }
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$values = rtrim($values, ','); 
                $sql = sprintf(self::SQL_UPDATE, $values);
		$stmt=self::prepareStatement($db, $sql);
                Logger::LogMessage(LOG_DEBUG, "SQL Query(updateToDatabase): $sql", get_called_class());
		$stmt->bindValue(1,$this->getOldInstance()->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch LeafsGalleriesTypesDictEntity which references this LeafsGalleriesEntity. Will return null in case reference is invalid.
	 * `LEAFS_GALLERIES`.`type` -> `LEAFS_GALLERIES_TYPES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsGalleriesTypesDictEntity
	 */
	public function fetchLeafsGalleriesTypesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsGalleriesTypesDictEntity::FIELD_NAME=>$this->getType());
		$result=LeafsGalleriesTypesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch GroupsEntity which references this LeafsGalleriesEntity. Will return null in case reference is invalid.
	 * `LEAFS_GALLERIES`.`group` -> `GROUPS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return GroupsEntity
	 */
	public function fetchGroupsEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(GroupsEntity::FIELD_ID=>$this->getGroup());
		$result=GroupsEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch MenuEntity which references this LeafsGalleriesEntity. Will return null in case reference is invalid.
	 * `LEAFS_GALLERIES`.`leaf` -> `MENU`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuEntity
	 */
	public function fetchMenuEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuEntity::FIELD_ID=>$this->getLeaf());
		$result=MenuEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch UsersEntity which references this LeafsGalleriesEntity. Will return null in case reference is invalid.
	 * `LEAFS_GALLERIES`.`adder` -> `USERS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity
	 */
	public function fetchUsersEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ID=>$this->getAdder());
		$result=UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'LeafsGalleriesEntity');
	}

	/**
	 * get single LeafsGalleriesEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return LeafsGalleriesEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new LeafsGalleriesEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of LeafsGalleriesEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return LeafsGalleriesEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('LeafsGalleriesEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>
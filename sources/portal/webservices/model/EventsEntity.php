<?php
require_once 'config/PortalConfig.php';

/**
 * 
 *
 * @version 1.108
 * @package entity
 */
class EventsEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='EventsEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='EVENTS';
	const SQL_INSERT='INSERT INTO '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' %s VALUES %s';
	const SQL_UPDATE='UPDATE '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' SET %s  WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const FIELD_ID=-1476058608;
	const FIELD_CYCLE=-1391631407;
	const FIELD_TYPE=-1152763153;
	const FIELD_START_DATE=-1285316256;
	const FIELD_END_DATE=-737199673;
	const FIELD_START_HOUR=-1285183594;
	const FIELD_END_HOUR=-737067011;
	const FIELD_ADD_DATE=596391361;
	const FIELD_NAME=-1152965056;
	const FIELD_DESCRIPTION=-1376767321;
	const FIELD_OCURRENCE=-1504416041;
	const FIELD_ADDER=-1394103303;
	const FIELD_GROUP=-1388134038;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_CYCLE=>'cycle',
		self::FIELD_TYPE=>'type',
		self::FIELD_START_DATE=>'start_date',
		self::FIELD_END_DATE=>'end_date',
		self::FIELD_START_HOUR=>'start_hour',
		self::FIELD_END_HOUR=>'end_hour',
		self::FIELD_ADD_DATE=>'add_date',
		self::FIELD_NAME=>'name',
		self::FIELD_DESCRIPTION=>'description',
		self::FIELD_OCURRENCE=>'ocurrence',
		self::FIELD_ADDER=>'adder',
		self::FIELD_GROUP=>'group');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_CYCLE=>'cycle',
		self::FIELD_TYPE=>'type',
		self::FIELD_START_DATE=>'startDate',
		self::FIELD_END_DATE=>'endDate',
		self::FIELD_START_HOUR=>'startHour',
		self::FIELD_END_HOUR=>'endHour',
		self::FIELD_ADD_DATE=>'addDate',
		self::FIELD_NAME=>'name',
		self::FIELD_DESCRIPTION=>'description',
		self::FIELD_OCURRENCE=>'oCurrEncE',
		self::FIELD_ADDER=>'adder',
		self::FIELD_GROUP=>'group');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_CYCLE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_TYPE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_START_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_END_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_START_HOUR=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_END_HOUR=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_ADD_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_NAME=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_DESCRIPTION=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_OCURRENCE=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ADDER=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_GROUP=>Db2PhpEntity::PHP_TYPE_INT);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_CYCLE=>array(Db2PhpEntity::JDBC_TYPE_CHAR,1,0,false),
		self::FIELD_TYPE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,true),
		self::FIELD_START_DATE=>array(Db2PhpEntity::JDBC_TYPE_DATE,10,0,false),
		self::FIELD_END_DATE=>array(Db2PhpEntity::JDBC_TYPE_DATE,10,0,true),
		self::FIELD_START_HOUR=>array(Db2PhpEntity::JDBC_TYPE_TIME,8,0,false),
		self::FIELD_END_HOUR=>array(Db2PhpEntity::JDBC_TYPE_TIME,8,0,false),
		self::FIELD_ADD_DATE=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_NAME=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,255,0,false),
		self::FIELD_DESCRIPTION=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,1024,0,true),
		self::FIELD_OCURRENCE=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_ADDER=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_GROUP=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_CYCLE=>'',
		self::FIELD_TYPE=>null,
		self::FIELD_START_DATE=>'',
		self::FIELD_END_DATE=>null,
		self::FIELD_START_HOUR=>'',
		self::FIELD_END_HOUR=>'',
		self::FIELD_ADD_DATE=>'CURRENT_TIMESTAMP',
		self::FIELD_NAME=>'',
		self::FIELD_DESCRIPTION=>null,
		self::FIELD_OCURRENCE=>null,
		self::FIELD_ADDER=>null,
		self::FIELD_GROUP=>null);
	private $id;
	private $cycle;
	private $type;
	private $startDate;
	private $endDate;
	private $startHour;
	private $endHour;
	private $addDate;
	private $name;
	private $description;
	private $oCurrEncE;
	private $adder;
	private $group;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return EventsEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for cycle 
	 *
	 * type:CHAR,size:1,default:null,index
	 *
	 * @param mixed $cycle
	 * @return EventsEntity
	 */
	public function &setCycle($cycle) {
		$this->notifyChanged(self::FIELD_CYCLE,$this->cycle,$cycle);
		$this->cycle=$cycle;
		return $this;
	}

	/**
	 * get value for cycle 
	 *
	 * type:CHAR,size:1,default:null,index
	 *
	 * @return mixed
	 */
	public function getCycle() {
		return $this->cycle;
	}

	/**
	 * set value for type 
	 *
	 * type:VARCHAR,size:45,default:null,index,nullable
	 *
	 * @param mixed $type
	 * @return EventsEntity
	 */
	public function &setType($type) {
		$this->notifyChanged(self::FIELD_TYPE,$this->type,$type);
		$this->type=$type;
		return $this;
	}

	/**
	 * get value for type 
	 *
	 * type:VARCHAR,size:45,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * set value for start_date 
	 *
	 * type:DATE,size:10,default:null
	 *
	 * @param mixed $startDate
	 * @return EventsEntity
	 */
	public function &setStartDate($startDate) {
		$this->notifyChanged(self::FIELD_START_DATE,$this->startDate,$startDate);
		$this->startDate=$startDate;
		return $this;
	}

	/**
	 * get value for start_date 
	 *
	 * type:DATE,size:10,default:null
	 *
	 * @return mixed
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * set value for end_date 
	 *
	 * type:DATE,size:10,default:null,nullable
	 *
	 * @param mixed $endDate
	 * @return EventsEntity
	 */
	public function &setEndDate($endDate) {
		$this->notifyChanged(self::FIELD_END_DATE,$this->endDate,$endDate);
		$this->endDate=$endDate;
		return $this;
	}

	/**
	 * get value for end_date 
	 *
	 * type:DATE,size:10,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * set value for start_hour 
	 *
	 * type:TIME,size:8,default:null
	 *
	 * @param mixed $startHour
	 * @return EventsEntity
	 */
	public function &setStartHour($startHour) {
		$this->notifyChanged(self::FIELD_START_HOUR,$this->startHour,$startHour);
		$this->startHour=$startHour;
		return $this;
	}

	/**
	 * get value for start_hour 
	 *
	 * type:TIME,size:8,default:null
	 *
	 * @return mixed
	 */
	public function getStartHour() {
		return $this->startHour;
	}

	/**
	 * set value for end_hour 
	 *
	 * type:TIME,size:8,default:null
	 *
	 * @param mixed $endHour
	 * @return EventsEntity
	 */
	public function &setEndHour($endHour) {
		$this->notifyChanged(self::FIELD_END_HOUR,$this->endHour,$endHour);
		$this->endHour=$endHour;
		return $this;
	}

	/**
	 * get value for end_hour 
	 *
	 * type:TIME,size:8,default:null
	 *
	 * @return mixed
	 */
	public function getEndHour() {
		return $this->endHour;
	}

	/**
	 * set value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @param mixed $addDate
	 * @return EventsEntity
	 */
	public function &setAddDate($addDate) {
		$this->notifyChanged(self::FIELD_ADD_DATE,$this->addDate,$addDate);
		$this->addDate=$addDate;
		return $this;
	}

	/**
	 * get value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @return mixed
	 */
	public function getAddDate() {
		return $this->addDate;
	}

	/**
	 * set value for name 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @param mixed $name
	 * @return EventsEntity
	 */
	public function &setName($name) {
		$this->notifyChanged(self::FIELD_NAME,$this->name,$name);
		$this->name=$name;
		return $this;
	}

	/**
	 * get value for name 
	 *
	 * type:VARCHAR,size:255,default:null
	 *
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * set value for description 
	 *
	 * type:VARCHAR,size:1024,default:null,nullable
	 *
	 * @param mixed $description
	 * @return EventsEntity
	 */
	public function &setDescription($description) {
		$this->notifyChanged(self::FIELD_DESCRIPTION,$this->description,$description);
		$this->description=$description;
		return $this;
	}

	/**
	 * get value for description 
	 *
	 * type:VARCHAR,size:1024,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * set value for ocurrence 
	 *
	 * type:INT,size:10,default:null,nullable
	 *
	 * @param mixed $oCurrEncE
	 * @return EventsEntity
	 */
	public function &setOCurrEncE($oCurrEncE) {
		$this->notifyChanged(self::FIELD_OCURRENCE,$this->oCurrEncE,$oCurrEncE);
		$this->oCurrEncE=$oCurrEncE;
		return $this;
	}

	/**
	 * get value for ocurrence 
	 *
	 * type:INT,size:10,default:null,nullable
	 *
	 * @return mixed
	 */
	public function getOCurrEncE() {
		return $this->oCurrEncE;
	}

	/**
	 * set value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $adder
	 * @return EventsEntity
	 */
	public function &setAdder($adder) {
		$this->notifyChanged(self::FIELD_ADDER,$this->adder,$adder);
		$this->adder=$adder;
		return $this;
	}

	/**
	 * get value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getAdder() {
		return $this->adder;
	}

	/**
	 * set value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $group
	 * @return EventsEntity
	 */
	public function &setGroup($group) {
		$this->notifyChanged(self::FIELD_GROUP,$this->group,$group);
		$this->group=$group;
		return $this;
	}

	/**
	 * get value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getGroup() {
		return $this->group;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_CYCLE=>$this->getCycle(),
			self::FIELD_TYPE=>$this->getType(),
			self::FIELD_START_DATE=>$this->getStartDate(),
			self::FIELD_END_DATE=>$this->getEndDate(),
			self::FIELD_START_HOUR=>$this->getStartHour(),
			self::FIELD_END_HOUR=>$this->getEndHour(),
			self::FIELD_ADD_DATE=>$this->getAddDate(),
			self::FIELD_NAME=>$this->getName(),
			self::FIELD_DESCRIPTION=>$this->getDescription(),
			self::FIELD_OCURRENCE=>$this->getOCurrEncE(),
			self::FIELD_ADDER=>$this->getAdder(),
			self::FIELD_GROUP=>$this->getGroup());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (empty(self::$stmts[$statement][$dbInstanceId])) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}
	
	/**
	 * check if this instance exists in the database
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public function existsInDatabase(PDO $db) {
		$filter=array();
		foreach ($this->getPrimaryKeyValues() as $fieldId=>$value) {
			$filter[]=new DFC($fieldId, $value, DFC::EXACT);
		}
		return 0!=count(self::findByFilter($db, $filter, true));
	}
	
	/**
	 * Update to database if exists, otherwise insert
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateInsertToDatabase(PDO $db) {
		if ($this->existsInDatabase($db)) {
			return $this->updateToDatabase($db);
		} else {
			return $this->insertIntoDatabase($db);
		}
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of EventsEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param EventsEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return EventsEntity[]
	 */
	public static function findByExample(PDO $db,EventsEntity $example, $and=true, $sort=null, DPC $paginator=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort, $paginator);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of EventsEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return EventsEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null, DPC $paginator=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `EVENTS`'.self::buildSqlWhere($filter, $and, false, true);
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of EventsEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return EventsEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of EventsEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return EventsEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new EventsEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of EventsEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return EventsEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findBySql): $sql", get_called_class());
		return self::fromExecutedStatement($stmt);
	}
	
	/**
	 * Execute select query and return all rows as an array of EventsEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @return EventsEntity[]
	 */
	public static function getAll(PDO $db, $sort=null, DPC $paginator=null) {
		$sql='SELECT * FROM `EVENTS`';
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(getAll): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		return self::fromStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `EVENTS`'.self::buildSqlWhere($filter, $and, false, true);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(deleteByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		$stmt->closeCursor();
		if ($affected === false) 
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		return $stmt->rowCount();
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setCycle($result['cycle']);
		$this->setType($result['type']);
		$this->setStartDate($result['start_date']);
		$this->setEndDate($result['end_date']);
		$this->setStartHour($result['start_hour']);
		$this->setEndHour($result['end_hour']);
		$this->setAddDate($result['add_date']);
		$this->setName($result['name']);
		$this->setDescription($result['description']);
		$this->setOCurrEncE($result['ocurrence']);
		$this->setAdder($result['adder']);
		$this->setGroup($result['group']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return EventsEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new EventsEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getCycle());
		$stmt->bindValue(3,$this->getType());
		$stmt->bindValue(4,$this->getStartDate());
		$stmt->bindValue(5,$this->getEndDate());
		$stmt->bindValue(6,$this->getStartHour());
		$stmt->bindValue(7,$this->getEndHour());
		$stmt->bindValue(8,$this->getAddDate());
		$stmt->bindValue(9,$this->getName());
		$stmt->bindValue(10,$this->getDescription());
		$stmt->bindValue(11,$this->getOCurrEncE());
		$stmt->bindValue(12,$this->getAdder());
		$stmt->bindValue(13,$this->getGroup());
	}
	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		$columns = '(';
		$values = '(';
		foreach($this->toArray() as $fieldId => $value)
		{
			if($value !== NULL && self::$DEFAULT_VALUES[$fieldId] !== $value)
			{
				$columns .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.',';
                                if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                    $values .= "$value,";
                                else
                                    $values .= "'$value',";
			}
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$columns = rtrim($columns, ',') . ')';
		$values = rtrim($values, ',') . ')';                
		$sql = sprintf(self::SQL_INSERT, $columns, $values);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(insertIntoDatabase): $sql", get_called_class());
		$stmt = self::prepareStatement($db, $sql);        
		$affected=$stmt->execute();
		if(false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		//if ($lastInsertId !== false)
		if ($lastInsertId != false) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$values = '';
                foreach($this->toArray() as $fieldId => $value)
		{
                    if(self::$DEFAULT_VALUES[$fieldId] !== $value || $value === NULL)
                    {
			$values .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.'=';
                            if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                $values .= "$value,";
                            elseif($value === NULL)
                                $values .= "NULL,";
                            else
                                $values .= "'$value',";
                    }
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$values = rtrim($values, ','); 
                $sql = sprintf(self::SQL_UPDATE, $values);
		$stmt=self::prepareStatement($db, $sql);
                Logger::LogMessage(LOG_DEBUG, "SQL Query(updateToDatabase): $sql", get_called_class());
		$stmt->bindValue(1,$this->getOldInstance()->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch UsersEntity which references this EventsEntity. Will return null in case reference is invalid.
	 * `EVENTS`.`adder` -> `USERS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity
	 */
	public function fetchUsersEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ID=>$this->getAdder());
		$result=UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch EventsCyclesDictEntity which references this EventsEntity. Will return null in case reference is invalid.
	 * `EVENTS`.`cycle` -> `EVENTS_CYCLES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return EventsCyclesDictEntity
	 */
	public function fetchEventsCyclesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(EventsCyclesDictEntity::FIELD_NAME=>$this->getCycle());
		$result=EventsCyclesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch GroupsEntity which references this EventsEntity. Will return null in case reference is invalid.
	 * `EVENTS`.`group` -> `GROUPS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return GroupsEntity
	 */
	public function fetchGroupsEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(GroupsEntity::FIELD_ID=>$this->getGroup());
		$result=GroupsEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch EventsTypesDictEntity which references this EventsEntity. Will return null in case reference is invalid.
	 * `EVENTS`.`type` -> `EVENTS_TYPES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return EventsTypesDictEntity
	 */
	public function fetchEventsTypesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(EventsTypesDictEntity::FIELD_NAME=>$this->getType());
		$result=EventsTypesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'EventsEntity');
	}

	/**
	 * get single EventsEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return EventsEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new EventsEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of EventsEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return EventsEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('EventsEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>
<?php
require_once 'config/PortalConfig.php';

/**
 * 
 *
 * @version 1.108
 * @package entity
 */
class MenuEntity extends Db2PhpEntityBase implements Db2PhpEntityModificationTracking {
	private static $CLASS_NAME='MenuEntity';
	const SQL_IDENTIFIER_QUOTE='`';
	const SQL_TABLE_NAME='MENU';
	const SQL_INSERT='INSERT INTO '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' %s VALUES %s';
	const SQL_UPDATE='UPDATE '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.' SET %s  WHERE `id`=?';
	const SQL_SELECT_PK='SELECT * FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const SQL_DELETE_PK='DELETE FROM '.self::SQL_IDENTIFIER_QUOTE.self::SQL_TABLE_NAME.self::SQL_IDENTIFIER_QUOTE.'  WHERE `id`=?';
	const FIELD_ID=1668332554;
	const FIELD_ORDER=-60375553;
	const FIELD_ADDER=-73721921;
	const FIELD_ADD_DATE=-1527321541;
	const FIELD_PARENT=-1858295783;
	const FIELD_PUBLIC=-1840295464;
	const FIELD_HIDDEN=-2080359143;
	const FIELD_GROUP=-67752656;
	const FIELD_TYPE=1245134441;
	const FIELD_NAME=1244932538;
	const FIELD_VARIANT=-742433034;
	const FIELD_LEAF_TYPE=767166892;
	private static $PRIMARY_KEYS=array(self::FIELD_ID);
	private static $AUTOINCREMENT_FIELDS=array(self::FIELD_ID);
	private static $FIELD_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_ORDER=>'order',
		self::FIELD_ADDER=>'adder',
		self::FIELD_ADD_DATE=>'add_date',
		self::FIELD_PARENT=>'parent',
		self::FIELD_PUBLIC=>'public',
		self::FIELD_HIDDEN=>'hidden',
		self::FIELD_GROUP=>'group',
		self::FIELD_TYPE=>'type',
		self::FIELD_NAME=>'name',
		self::FIELD_VARIANT=>'variant',
		self::FIELD_LEAF_TYPE=>'leaf_type');
	private static $PROPERTY_NAMES=array(
		self::FIELD_ID=>'id',
		self::FIELD_ORDER=>'order',
		self::FIELD_ADDER=>'adder',
		self::FIELD_ADD_DATE=>'addDate',
		self::FIELD_PARENT=>'parent',
		self::FIELD_PUBLIC=>'public',
		self::FIELD_HIDDEN=>'hidden',
		self::FIELD_GROUP=>'group',
		self::FIELD_TYPE=>'type',
		self::FIELD_NAME=>'name',
		self::FIELD_VARIANT=>'variant',
		self::FIELD_LEAF_TYPE=>'leafType');
	private static $PROPERTY_TYPES=array(
		self::FIELD_ID=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ORDER=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ADDER=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_ADD_DATE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_PARENT=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_PUBLIC=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_HIDDEN=>Db2PhpEntity::PHP_TYPE_BOOL,
		self::FIELD_GROUP=>Db2PhpEntity::PHP_TYPE_INT,
		self::FIELD_TYPE=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_NAME=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_VARIANT=>Db2PhpEntity::PHP_TYPE_STRING,
		self::FIELD_LEAF_TYPE=>Db2PhpEntity::PHP_TYPE_STRING);
	private static $FIELD_TYPES=array(
		self::FIELD_ID=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,false),
		self::FIELD_ORDER=>array(Db2PhpEntity::JDBC_TYPE_TINYINT,3,0,false),
		self::FIELD_ADDER=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_ADD_DATE=>array(Db2PhpEntity::JDBC_TYPE_TIMESTAMP,19,0,false),
		self::FIELD_PARENT=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_PUBLIC=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_HIDDEN=>array(Db2PhpEntity::JDBC_TYPE_BIT,0,0,false),
		self::FIELD_GROUP=>array(Db2PhpEntity::JDBC_TYPE_INTEGER,10,0,true),
		self::FIELD_TYPE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,8,0,false),
		self::FIELD_NAME=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,false),
		self::FIELD_VARIANT=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,9,0,true),
		self::FIELD_LEAF_TYPE=>array(Db2PhpEntity::JDBC_TYPE_VARCHAR,45,0,true));
	private static $DEFAULT_VALUES=array(
		self::FIELD_ID=>null,
		self::FIELD_ORDER=>0,
		self::FIELD_ADDER=>null,
		self::FIELD_ADD_DATE=>'CURRENT_TIMESTAMP',
		self::FIELD_PARENT=>null,
		self::FIELD_PUBLIC=>'1',
		self::FIELD_HIDDEN=>'0',
		self::FIELD_GROUP=>null,
		self::FIELD_TYPE=>'',
		self::FIELD_NAME=>'',
		self::FIELD_VARIANT=>'NORMAL',
		self::FIELD_LEAF_TYPE=>null);
	private $id;
	private $order;
	private $adder;
	private $addDate;
	private $parent;
	private $public;
	private $hidden;
	private $group;
	private $type;
	private $name;
	private $variant;
	private $leafType;

	/**
	 * set value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @param mixed $id
	 * @return MenuEntity
	 */
	public function &setId($id) {
		$this->notifyChanged(self::FIELD_ID,$this->id,$id);
		$this->id=$id;
		return $this;
	}

	/**
	 * get value for id 
	 *
	 * type:INT UNSIGNED,size:10,default:null,primary,unique,autoincrement
	 *
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * set value for order 
	 *
	 * type:TINYINT UNSIGNED,size:3,default:null
	 *
	 * @param mixed $order
	 * @return MenuEntity
	 */
	public function &setOrder($order) {
		$this->notifyChanged(self::FIELD_ORDER,$this->order,$order);
		$this->order=$order;
		return $this;
	}

	/**
	 * get value for order 
	 *
	 * type:TINYINT UNSIGNED,size:3,default:null
	 *
	 * @return mixed
	 */
	public function getOrder() {
		return $this->order;
	}

	/**
	 * set value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $adder
	 * @return MenuEntity
	 */
	public function &setAdder($adder) {
		$this->notifyChanged(self::FIELD_ADDER,$this->adder,$adder);
		$this->adder=$adder;
		return $this;
	}

	/**
	 * get value for adder 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getAdder() {
		return $this->adder;
	}

	/**
	 * set value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @param mixed $addDate
	 * @return MenuEntity
	 */
	public function &setAddDate($addDate) {
		$this->notifyChanged(self::FIELD_ADD_DATE,$this->addDate,$addDate);
		$this->addDate=$addDate;
		return $this;
	}

	/**
	 * get value for add_date 
	 *
	 * type:TIMESTAMP,size:19,default:CURRENT_TIMESTAMP
	 *
	 * @return mixed
	 */
	public function getAddDate() {
		return $this->addDate;
	}

	/**
	 * set value for parent 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $parent
	 * @return MenuEntity
	 */
	public function &setParent($parent) {
		$this->notifyChanged(self::FIELD_PARENT,$this->parent,$parent);
		$this->parent=$parent;
		return $this;
	}

	/**
	 * get value for parent 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getParent() {
		return $this->parent;
	}

	/**
	 * set value for public 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @param mixed $public
	 * @return MenuEntity
	 */
	public function &setPublic($public) {
		$this->notifyChanged(self::FIELD_PUBLIC,$this->public,$public);
		$this->public=$public;
		return $this;
	}

	/**
	 * get value for public 
	 *
	 * type:BIT,size:0,default:1
	 *
	 * @return mixed
	 */
	public function getPublic() {
		return $this->public;
	}

	/**
	 * set value for hidden 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @param mixed $hidden
	 * @return MenuEntity
	 */
	public function &setHidden($hidden) {
		$this->notifyChanged(self::FIELD_HIDDEN,$this->hidden,$hidden);
		$this->hidden=$hidden;
		return $this;
	}

	/**
	 * get value for hidden 
	 *
	 * type:BIT,size:0,default:0
	 *
	 * @return mixed
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * set value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @param mixed $group
	 * @return MenuEntity
	 */
	public function &setGroup($group) {
		$this->notifyChanged(self::FIELD_GROUP,$this->group,$group);
		$this->group=$group;
		return $this;
	}

	/**
	 * get value for group 
	 *
	 * type:INT UNSIGNED,size:10,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getGroup() {
		return $this->group;
	}

	/**
	 * set value for type 
	 *
	 * type:VARCHAR,size:8,default:null,index
	 *
	 * @param mixed $type
	 * @return MenuEntity
	 */
	public function &setType($type) {
		$this->notifyChanged(self::FIELD_TYPE,$this->type,$type);
		$this->type=$type;
		return $this;
	}

	/**
	 * get value for type 
	 *
	 * type:VARCHAR,size:8,default:null,index
	 *
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * set value for name 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @param mixed $name
	 * @return MenuEntity
	 */
	public function &setName($name) {
		$this->notifyChanged(self::FIELD_NAME,$this->name,$name);
		$this->name=$name;
		return $this;
	}

	/**
	 * get value for name 
	 *
	 * type:VARCHAR,size:45,default:null
	 *
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * set value for variant 
	 *
	 * type:VARCHAR,size:9,default:NORMAL,index,nullable
	 *
	 * @param mixed $variant
	 * @return MenuEntity
	 */
	public function &setVariant($variant) {
		$this->notifyChanged(self::FIELD_VARIANT,$this->variant,$variant);
		$this->variant=$variant;
		return $this;
	}

	/**
	 * get value for variant 
	 *
	 * type:VARCHAR,size:9,default:NORMAL,index,nullable
	 *
	 * @return mixed
	 */
	public function getVariant() {
		return $this->variant;
	}

	/**
	 * set value for leaf_type 
	 *
	 * type:VARCHAR,size:45,default:null,index,nullable
	 *
	 * @param mixed $leafType
	 * @return MenuEntity
	 */
	public function &setLeafType($leafType) {
		$this->notifyChanged(self::FIELD_LEAF_TYPE,$this->leafType,$leafType);
		$this->leafType=$leafType;
		return $this;
	}

	/**
	 * get value for leaf_type 
	 *
	 * type:VARCHAR,size:45,default:null,index,nullable
	 *
	 * @return mixed
	 */
	public function getLeafType() {
		return $this->leafType;
	}

	/**
	 * Get table name
	 *
	 * @return string
	 */
	public static function getTableName() {
		return self::SQL_TABLE_NAME;
	}

	/**
	 * Get array with field id as index and field name as value
	 *
	 * @return array
	 */
	public static function getFieldNames() {
		return self::$FIELD_NAMES;
	}

	/**
	 * Get array with field id as index and property name as value
	 *
	 * @return array
	 */
	public static function getPropertyNames() {
		return self::$PROPERTY_NAMES;
	}

	/**
	 * get the field name for the passed field id.
	 *
	 * @param int $fieldId
	 * @param bool $fullyQualifiedName true if field name should be qualified by table name
	 * @return string field name for the passed field id, null if the field doesn't exist
	 */
	public static function getFieldNameByFieldId($fieldId, $fullyQualifiedName=true) {
		if (!array_key_exists($fieldId, self::$FIELD_NAMES)) {
			return null;
		}
		$fieldName=self::SQL_IDENTIFIER_QUOTE . self::$FIELD_NAMES[$fieldId] . self::SQL_IDENTIFIER_QUOTE;
		if ($fullyQualifiedName) {
			return self::SQL_IDENTIFIER_QUOTE . self::SQL_TABLE_NAME . self::SQL_IDENTIFIER_QUOTE . '.' . $fieldName;
		}
		return $fieldName;
	}

	/**
	 * Get array with field ids of identifiers
	 *
	 * @return array
	 */
	public static function getIdentifierFields() {
		return self::$PRIMARY_KEYS;
	}

	/**
	 * Get array with field ids of autoincrement fields
	 *
	 * @return array
	 */
	public static function getAutoincrementFields() {
		return self::$AUTOINCREMENT_FIELDS;
	}

	/**
	 * Get array with field id as index and property type as value
	 *
	 * @return array
	 */
	public static function getPropertyTypes() {
		return self::$PROPERTY_TYPES;
	}

	/**
	 * Get array with field id as index and field type as value
	 *
	 * @return array
	 */
	public static function getFieldTypes() {
		return self::$FIELD_TYPES;
	}

	/**
	 * Assign default values according to table
	 * 
	 */
	public function assignDefaultValues() {
		$this->assignByArray(self::$DEFAULT_VALUES);
	}


	/**
	 * return hash with the field name as index and the field value as value.
	 *
	 * @return array
	 */
	public function toHash() {
		$array=$this->toArray();
		$hash=array();
		foreach ($array as $fieldId=>$value) {
			$hash[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		return $hash;
	}

	/**
	 * return array with the field id as index and the field value as value.
	 *
	 * @return array
	 */
	public function toArray() {
		return array(
			self::FIELD_ID=>$this->getId(),
			self::FIELD_ORDER=>$this->getOrder(),
			self::FIELD_ADDER=>$this->getAdder(),
			self::FIELD_ADD_DATE=>$this->getAddDate(),
			self::FIELD_PARENT=>$this->getParent(),
			self::FIELD_PUBLIC=>$this->getPublic(),
			self::FIELD_HIDDEN=>$this->getHidden(),
			self::FIELD_GROUP=>$this->getGroup(),
			self::FIELD_TYPE=>$this->getType(),
			self::FIELD_NAME=>$this->getName(),
			self::FIELD_VARIANT=>$this->getVariant(),
			self::FIELD_LEAF_TYPE=>$this->getLeafType());
	}


	/**
	 * return array with the field id as index and the field value as value for the identifier fields.
	 *
	 * @return array
	 */
	public function getPrimaryKeyValues() {
		return array(
			self::FIELD_ID=>$this->getId());
	}

	/**
	 * cached statements
	 *
	 * @var array<string,array<string,PDOStatement>>
	 */
	private static $stmts=array();
	private static $cacheStatements=true;
	
	/**
	 * prepare passed string as statement or return cached if enabled and available
	 *
	 * @param PDO $db
	 * @param string $statement
	 * @return PDOStatement
	 */
	protected static function prepareStatement(PDO $db, $statement) {
		if(self::isCacheStatements()) {
			if (in_array($statement, array(self::SQL_INSERT, self::SQL_UPDATE, self::SQL_SELECT_PK, self::SQL_DELETE_PK))) {
				$dbInstanceId=spl_object_hash($db);
				if (empty(self::$stmts[$statement][$dbInstanceId])) {
					self::$stmts[$statement][$dbInstanceId]=$db->prepare($statement);
				}
				return self::$stmts[$statement][$dbInstanceId];
			}
		}
		return $db->prepare($statement);
	}

	/**
	 * Enable statement cache
	 *
	 * @param bool $cache
	 */
	public static function setCacheStatements($cache) {
		self::$cacheStatements=true==$cache;
	}

	/**
	 * Check if statement cache is enabled
	 *
	 * @return bool
	 */
	public static function isCacheStatements() {
		return self::$cacheStatements;
	}
	
	/**
	 * check if this instance exists in the database
	 *
	 * @param PDO $db
	 * @return bool
	 */
	public function existsInDatabase(PDO $db) {
		$filter=array();
		foreach ($this->getPrimaryKeyValues() as $fieldId=>$value) {
			$filter[]=new DFC($fieldId, $value, DFC::EXACT);
		}
		return 0!=count(self::findByFilter($db, $filter, true));
	}
	
	/**
	 * Update to database if exists, otherwise insert
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateInsertToDatabase(PDO $db) {
		if ($this->existsInDatabase($db)) {
			return $this->updateToDatabase($db);
		} else {
			return $this->insertIntoDatabase($db);
		}
	}

	/**
	 * Query by Example.
	 *
	 * Match by attributes of passed example instance and return matched rows as an array of MenuEntity instances
	 *
	 * @param PDO $db a PDO Database instance
	 * @param MenuEntity $example an example instance defining the conditions. All non-null properties will be considered a constraint, null values will be ignored.
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return MenuEntity[]
	 */
	public static function findByExample(PDO $db,MenuEntity $example, $and=true, $sort=null, DPC $paginator=null) {
		$exampleValues=$example->toArray();
		$filter=array();
		foreach ($exampleValues as $fieldId=>$value) {
			if (null!==$value) {
				$filter[$fieldId]=$value;
			}
		}
		return self::findByFilter($db, $filter, $and, $sort, $paginator);
	}

	/**
	 * Query by filter.
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * Will return matched rows as an array of MenuEntity instances.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $filter array of DFC instances defining the conditions
	 * @param boolean $and true if conditions should be and'ed, false if they should be or'ed
	 * @param array $sort array of DSC instances
	 * @return MenuEntity[]
	 */
	public static function findByFilter(PDO $db, $filter, $and=true, $sort=null, DPC $paginator=null) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		$sql='SELECT * FROM `MENU`'.self::buildSqlWhere($filter, $and, false, true);
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		return self::fromStatement($stmt);
	}

	/**
	 * Will execute the passed statement and return the result as an array of MenuEntity instances
	 *
	 * @param PDOStatement $stmt
	 * @return MenuEntity[]
	 */
	public static function fromStatement(PDOStatement $stmt) {
		$affected=$stmt->execute();
		if (false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		return self::fromExecutedStatement($stmt);
	}

	/**
	 * returns the result as an array of MenuEntity instances without executing the passed statement
	 *
	 * @param PDOStatement $stmt
	 * @return MenuEntity[]
	 */
	public static function fromExecutedStatement(PDOStatement $stmt) {
		$resultInstances=array();
		while($result=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$o=new MenuEntity();
			$o->assignByHash($result);
			$o->notifyPristine();
			$resultInstances[]=$o;
		}
		$stmt->closeCursor();
		return $resultInstances;
	}

	/**
	 * Get sql WHERE part from filter.
	 *
	 * @param array $filter
	 * @param bool $and
	 * @param bool $fullyQualifiedNames true if field names should be qualified by table name
	 * @param bool $prependWhere true if WHERE should be prepended to conditions
	 * @return string
	 */
	public static function buildSqlWhere($filter, $and, $fullyQualifiedNames=true, $prependWhere=false) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		return $filter->buildSqlWhere(new self::$CLASS_NAME, $fullyQualifiedNames, $prependWhere);
	}

	/**
	 * get sql ORDER BY part from DSCs
	 *
	 * @param array $sort array of DSC instances
	 * @return string
	 */
	protected static function buildSqlOrderBy($sort) {
		return DSC::buildSqlOrderBy(new self::$CLASS_NAME, $sort);
	}

	/**
	 * bind values from filter to statement
	 *
	 * @param PDOStatement $stmt
	 * @param DFCInterface $filter
	 */
	public static function bindValuesForFilter(PDOStatement &$stmt, DFCInterface $filter) {
		$filter->bindValuesForFilter(new self::$CLASS_NAME, $stmt);
	}

	/**
	 * Execute select query and return matched rows as an array of MenuEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @param string $sql
	 * @return MenuEntity[]
	 */
	public static function findBySql(PDO $db, $sql) {
		$stmt=$db->query($sql);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(findBySql): $sql", get_called_class());
		return self::fromExecutedStatement($stmt);
	}
	
	/**
	 * Execute select query and return all rows as an array of MenuEntity instances.
	 *
	 * The query should of course be on the table for this entity class and return all fields.
	 *
	 * @param PDO $db a PDO Database instance
	 * @return MenuEntity[]
	 */
	public static function getAll(PDO $db, $sort=null, DPC $paginator=null) {
		$sql='SELECT * FROM `MENU`';
		if($sort != null)
			$sql.= ' '.self::buildSqlOrderBy($sort);
		if($paginator != null)
			$sql.= ' '.$paginator->getLimitsForQuery();
		Logger::LogMessage(LOG_DEBUG, "SQL Query(getAll): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		return self::fromStatement($stmt);
	}

	/**
	 * Delete rows matching the filter
	 *
	 * The filter can be either an hash with the field id as index and the value as filter value,
	 * or a array of DFC instances.
	 *
	 * @param PDO $db
	 * @param array $filter
	 * @param bool $and
	 * @return mixed
	 */
	public static function deleteByFilter(PDO $db, $filter, $and=true) {
		if (!($filter instanceof DFCInterface)) {
			$filter=new DFCAggregate($filter, $and);
		}
		if (0==count($filter)) {
			throw new InvalidArgumentException('refusing to delete without filter'); // just comment out this line if you are brave
		}
		$sql='DELETE FROM `MENU`'.self::buildSqlWhere($filter, $and, false, true);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(deleteByFilter): $sql", get_called_class());
		$stmt=self::prepareStatement($db, $sql);
		self::bindValuesForFilter($stmt, $filter);
		$affected=$stmt->execute();
		$stmt->closeCursor();
		if ($affected === false) 
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		return $stmt->rowCount();
	}

	/**
	 * Assign values from array with the field id as index and the value as value
	 *
	 * @param array $array
	 */
	public function assignByArray($array) {
		$result=array();
		foreach ($array as $fieldId=>$value) {
			$result[self::$FIELD_NAMES[$fieldId]]=$value;
		}
		$this->assignByHash($result);
	}

	/**
	 * Assign values from hash where the indexes match the tables field names
	 *
	 * @param array $result
	 */
	public function assignByHash($result) {
		$this->setId($result['id']);
		$this->setOrder($result['order']);
		$this->setAdder($result['adder']);
		$this->setAddDate($result['add_date']);
		$this->setParent($result['parent']);
		$this->setPublic($result['public']);
		$this->setHidden($result['hidden']);
		$this->setGroup($result['group']);
		$this->setType($result['type']);
		$this->setName($result['name']);
		$this->setVariant($result['variant']);
		$this->setLeafType($result['leaf_type']);
	}

	/**
	 * Get element instance by it's primary key(s).
	 * Will return null if no row was matched.
	 *
	 * @param PDO $db
	 * @return MenuEntity
	 */
	public static function findById(PDO $db,$id) {
		$stmt=self::prepareStatement($db,self::SQL_SELECT_PK);
		$stmt->bindValue(1,$id);
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$result=$stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		if(!$result) {
			return null;
		}
		$o=new MenuEntity();
		$o->assignByHash($result);
		$o->notifyPristine();
		return $o;
	}

	/**
	 * Bind all values to statement
	 *
	 * @param PDOStatement $stmt
	 */
	protected function bindValues(PDOStatement &$stmt) {
		$stmt->bindValue(1,$this->getId());
		$stmt->bindValue(2,$this->getOrder());
		$stmt->bindValue(3,$this->getAdder());
		$stmt->bindValue(4,$this->getAddDate());
		$stmt->bindValue(5,$this->getParent());
		$stmt->bindValue(6,$this->getPublic());
		$stmt->bindValue(7,$this->getHidden());
		$stmt->bindValue(8,$this->getGroup());
		$stmt->bindValue(9,$this->getType());
		$stmt->bindValue(10,$this->getName());
		$stmt->bindValue(11,$this->getVariant());
		$stmt->bindValue(12,$this->getLeafType());
	}
	/**
	 * Insert this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function insertIntoDatabase(PDO $db) {
		$columns = '(';
		$values = '(';
		foreach($this->toArray() as $fieldId => $value)
		{
			if($value !== NULL && self::$DEFAULT_VALUES[$fieldId] !== $value)
			{
				$columns .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.',';
                                if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                    $values .= "$value,";
                                else
                                    $values .= "'$value',";
			}
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$columns = rtrim($columns, ',') . ')';
		$values = rtrim($values, ',') . ')';                
		$sql = sprintf(self::SQL_INSERT, $columns, $values);
		Logger::LogMessage(LOG_DEBUG, "SQL Query(insertIntoDatabase): $sql", get_called_class());
		$stmt = self::prepareStatement($db, $sql);        
		$affected=$stmt->execute();
		if(false===$affected) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$lastInsertId=$db->lastInsertId();
		//if ($lastInsertId !== false)
		if ($lastInsertId != false) {
			$this->setId($lastInsertId);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Update this instance into the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function updateToDatabase(PDO $db) {
		$values = '';
                foreach($this->toArray() as $fieldId => $value)
		{
                    if(self::$DEFAULT_VALUES[$fieldId] !== $value || $value === NULL)
                    {
			$values .= self::SQL_IDENTIFIER_QUOTE.self::$FIELD_NAMES[$fieldId].self::SQL_IDENTIFIER_QUOTE.'=';
                            if(preg_match('/[A-Z_]+\((.*)\)/', $value))
                                $values .= "$value,";
                            elseif($value === NULL)
                                $values .= "NULL,";
                            else
                                $values .= "'$value',";
                    }
		}
		//wyczyść nadmiarowy - ostatni - przecinenk
		$values = rtrim($values, ','); 
                $sql = sprintf(self::SQL_UPDATE, $values);
		$stmt=self::prepareStatement($db, $sql);
                Logger::LogMessage(LOG_DEBUG, "SQL Query(updateToDatabase): $sql", get_called_class());
		$stmt->bindValue(1,$this->getOldInstance()->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		$this->notifyPristine();
		return $affected;
	}


	/**
	 * Delete this instance from the database
	 *
	 * @param PDO $db
	 * @return mixed
	 */
	public function deleteFromDatabase(PDO $db) {
		$stmt=self::prepareStatement($db,self::SQL_DELETE_PK);
		$stmt->bindValue(1,$this->getId());
		$affected=$stmt->execute();
		if ($affected === false) {
			$stmt->closeCursor();
			throw new Exception($stmt->errorCode() . ':' . var_export($stmt->errorInfo(), true), 0);
		}
		$stmt->closeCursor();
		return $affected;
	}

	/**
	 * Fetch LeafsArticlesEntity's which this MenuEntity references.
	 * `MENU`.`id` -> `LEAFS_ARTICLES`.`leaf`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsArticlesEntity[]
	 */
	public function fetchLeafsArticlesEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsArticlesEntity::FIELD_LEAF=>$this->getId());
		return LeafsArticlesEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsGalleriesEntity's which this MenuEntity references.
	 * `MENU`.`id` -> `LEAFS_GALLERIES`.`leaf`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsGalleriesEntity[]
	 */
	public function fetchLeafsGalleriesEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsGalleriesEntity::FIELD_LEAF=>$this->getId());
		return LeafsGalleriesEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsPostsEntity's which this MenuEntity references.
	 * `MENU`.`id` -> `LEAFS_POSTS`.`leaf`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsPostsEntity[]
	 */
	public function fetchLeafsPostsEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsPostsEntity::FIELD_LEAF=>$this->getId());
		return LeafsPostsEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch LeafsStaticEntity's which this MenuEntity references.
	 * `MENU`.`id` -> `LEAFS_STATIC`.`leaf`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return LeafsStaticEntity[]
	 */
	public function fetchLeafsStaticEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(LeafsStaticEntity::FIELD_LEAF=>$this->getId());
		return LeafsStaticEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch MenuEntity's which this MenuEntity references.
	 * `MENU`.`id` -> `MENU`.`parent`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuEntity[]
	 */
	public function fetchMenuEntityCollection(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuEntity::FIELD_PARENT=>$this->getId());
		return MenuEntity::findByFilter($db, $filter, true, $sort, $paginator);
	}

	/**
	 * Fetch UsersEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`adder` -> `USERS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return UsersEntity
	 */
	public function fetchUsersEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(UsersEntity::FIELD_ID=>$this->getAdder());
		$result=UsersEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch MenuEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`parent` -> `MENU`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuEntity
	 */
	public function fetchMenuEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuEntity::FIELD_ID=>$this->getParent());
		$result=MenuEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch GroupsEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`group` -> `GROUPS`.`id`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return GroupsEntity
	 */
	public function fetchGroupsEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(GroupsEntity::FIELD_ID=>$this->getGroup());
		$result=GroupsEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch MenuLeafTypeDictEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`leaf_type` -> `MENU_LEAF_TYPE_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuLeafTypeDictEntity
	 */
	public function fetchMenuLeafTypeDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuLeafTypeDictEntity::FIELD_NAME=>$this->getLeafType());
		$result=MenuLeafTypeDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch MenuVariantsDictEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`variant` -> `MENU_VARIANTS_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuVariantsDictEntity
	 */
	public function fetchMenuVariantsDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuVariantsDictEntity::FIELD_NAME=>$this->getVariant());
		$result=MenuVariantsDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}

	/**
	 * Fetch MenuTypesDictEntity which references this MenuEntity. Will return null in case reference is invalid.
	 * `MENU`.`type` -> `MENU_TYPES_DICT`.`name`
	 *
	 * @param PDO $db a PDO Database instance
	 * @param array $sort array of DSC instances
	 * @return MenuTypesDictEntity
	 */
	public function fetchMenuTypesDictEntity(PDO $db, $sort=null, DPC $paginator=null) {
		$filter=array(MenuTypesDictEntity::FIELD_NAME=>$this->getType());
		$result=MenuTypesDictEntity::findByFilter($db, $filter, true, $sort, $paginator);
		return empty($result) ? null : $result[0];
	}


	/**
	 * get element as DOM Document
	 *
	 * @return DOMDocument
	 */
	public function toDOM() {
		return self::hashToDomDocument($this->toHash(), 'MenuEntity');
	}

	/**
	 * get single MenuEntity instance from a DOMElement
	 *
	 * @param DOMElement $node
	 * @return MenuEntity
	 */
	public static function fromDOMElement(DOMElement $node) {
		$o=new MenuEntity();
		$o->assignByHash(self::domNodeToHash($node, self::$FIELD_NAMES, self::$DEFAULT_VALUES, self::$FIELD_TYPES));
			$o->notifyPristine();
		return $o;
	}

	/**
	 * get all instances of MenuEntity from the passed DOMDocument
	 *
	 * @param DOMDocument $doc
	 * @return MenuEntity[]
	 */
	public static function fromDOMDocument(DOMDocument $doc) {
		$instances=array();
		foreach ($doc->getElementsByTagName('MenuEntity') as $node) {
			$instances[]=self::fromDOMElement($node);
		}
		return $instances;
	}

}
?>
<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Exception for webserivces - indicates that somethig was incorectly passed to webservice method
 *
 * @author h3x0r
 */
final class WrongRequestException extends ExceptionAbstr {
    /**
     * Constructor
     * @param string $message exception message
     * @param Exception $previous prevoisue exception
     */
    public function __construct(string $message, Exception $previous = null) {
	parent::__construct($message, ExceptionAbstr::WS_SCHEMA_EXCEPTION, $previous);
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Security excpetion
 *
 * @author h3x0r
 */
final class SecurityException extends ExceptionAbstr {
    /**
     * Constructor
     * @param string $message exception message
     * @param Exception $previous prevoisue exception
     */
    public function __construct(string $message, Exception $previous = null) {
	parent::__construct($message, ExceptionAbstr::SECURITY_EXCEPTION, $previous);
        $this->getLogger()->logError(self::GetSecurityLogInfo());
    }
    /**
     * Gets logs for security threat
     * @return string log input
     */
    private static function GetSecurityLogInfo() {
        return 'Data: '.date(DATE_FORMAT).', ip z jakiego przyszło żądanie: '.$_SERVER['REMOTE_ADDR'];
    }
}


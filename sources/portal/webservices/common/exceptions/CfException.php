<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Config file(ini) db exception
 *
 * @author sebo
 */
final class CfException extends ExceptionAbstr {
    /**
     * Constructor
     * @param string $message exception message
     * @param Exception $previous prevoisue exception
     */
    public function __construct(string $message, Exception $previous = null) {
	parent::__construct($message, ExceptionAbstr::CF_EXCEPTION, $previous);
    }
}

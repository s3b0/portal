<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Proxy for method invokation. 
 * We don't use direct invokation becouse of validation, it simplifiets the whole coding
 *
 * @author h3x0r
 */
final class ClassProxy {
    /**
    * @var Logger Loger
    */
    private $logger;
    /**
     *
     * @var string name of invoking class
     */
    private $className;
    /**
     *
     * @var WsAbstr child of WsAbstr (all webservices inheritade from this class)
     */
    private $object;
    /**
     *
     * @var string method name to invoke 
     */
    private $methodName;
    /**
     * Constructor
     * @param string $className name of class that will be created for invokation
     * @param WsAbstr $object instance of object that will be used for invokation
     * @param string $methodName name on method to invoke
     */
    public function __construct(string $className, WsAbstr $object, string $methodName) {
	$this->logger = new Logger(get_class($this));
        $this->className = $className;
        $this->object = $object;
        $this->methodName = $methodName;
    }
    /**
     * Here we do whole magic - earlier prepared args array is used to invoke method for object passed in constructor
     * @param array $args optional array of arguments for method
     * @return mixed method result
     */
    private function invokeMethod(array $args = NULL) {
        $class = new ReflectionClass($this->className);
        $method = $class->getMethod($this->methodName);
        $parameters = $method->getParameters();
        $this->logger->logDebug('Invoking method '.$this->className.'::'.$this->methodName.' ');
        if($args != NULL && is_array($args) && count($parameters) > 0)//when we have arguments for method and method has arguments
        {
            //$this->logger->logDebug('...with arguments');
            $arguments = array();//przygotuj argumenty dla metody
            try
            {
                //jeśli mamy obiekt filtra a metoda nie posiada filtra (np insert/delete)
                if($parameters[0]->getClass()->name != 'RequestCommonFiltersType')
                    unset($args[0]);//to go unsetuj
                else//w innym przypadku dodaj go - zawszej jako pierwszy
                    $arguments[0] = $args[0];
            }
            catch(ReflectionException $e)
            {
                unset($args[0]);//to go unsetuj - dubel, ponieważ może wystąpić wyjątek!
                $this->logger->logDebug("...method doesn't contains RequestCommonFiltersType as a first parameter");
            }
            /* @var $parameter ReflectionParameter  */
            foreach($parameters as $parameter)//fill(create) all method parameters
            {
                $parameterName = $parameter->getName();
                $paramType = $parameter->getClass();
                if(array_key_exists($parameterName, $args))//if $agrs has input parameter for method - we check by input parameter name
                {
                    $arguments[$parameterName] = $paramType->newInstance($args[$parameterName]);//create parameter for method
                    continue;
                }
                elseif($parameter->isDefaultValueAvailable())//if it is optonal parameter and it wasn't set in request(input $args array)
                {//so set it to it's default value (it will simplifiy implemetation of ws methods)
                    $arguments[$parameterName] = $paramType->newInstance();
                    continue;
                }
                elseif($paramType->getName() == 'RequestCommonFiltersType')//just continue - if method has this parameter it has been set in invoke method
                    continue;
                throw new WrongRequestException("Argument $parameterName jest wymagany!");//if $parameter is require and we dont have value for it - exception
            }
            $this->logger->logDebug('with agrs list: '.Utils::VarDumptoString($arguments));
            return $method->invokeArgs($this->object, $arguments);
        }
        else
        {
            $this->logger->logDebug('...without arguments');
            return $method->invoke($this->object);//invoked without arguments
        }
    }
    /**
     * Method prepares arguments for invoke, and (on end) it invokes passed in constructor method
     * If method has RequestCommonFiltersType as argument and user didn't pass it we create one here
     * with filled (default values setted in PortalConfig.php) parameters
     * @param array $requestArray array of parameters from HTTP request
     * @return mixed method results
     * @throws WrongRequestException
     */
    public function invoke(array $requestArray) {
        $rcfilter = new RequestCommonFiltersType();
        if(is_array($requestArray))//it can happen with getMethods invokation - need to hardcode setup
        {
            $filterArrayExist = array_key_exists(RequestCommonFiltersType::FILTER, $requestArray);
            $orderArrayExist = array_key_exists(RequestCommonFiltersType::ORDER, $requestArray);
            if($filterArrayExist || $orderArrayExist)//filter/order object(array) exist
            {
                if($filterArrayExist)
                {
                    $filterOffsetExist = array_key_exists(RequestCommonFiltersType::OFFSET, $requestArray[RequestCommonFiltersType::FILTER]);
                    $filterLimitExist = array_key_exists(RequestCommonFiltersType::LIMIT, $requestArray[RequestCommonFiltersType::FILTER]);
                    if($filterOffsetExist)
                    {
                        $rcfilter->setPageOffset($requestArray[RequestCommonFiltersType::FILTER][RequestCommonFiltersType::OFFSET]);
                        unset($requestArray[RequestCommonFiltersType::FILTER][RequestCommonFiltersType::OFFSET]);
                    }
                    if($filterLimitExist)
                    {
                        $rcfilter->setPageLimit($requestArray[RequestCommonFiltersType::FILTER][RequestCommonFiltersType::LIMIT]);
                        unset($requestArray[RequestCommonFiltersType::FILTER][RequestCommonFiltersType::LIMIT]);
                    }
                    unset($requestArray[RequestCommonFiltersType::FILTER]);
                    if(!$filterOffsetExist && !$filterLimitExist) 
                        throw new WrongRequestException("Filter object exist in request but without parameters! Put offset or limit into Filter object!", 
                            ExceptionAbstr::WS_SCHEMA_EXCEPTION);
                }
                if($orderArrayExist)
                {
                    if(is_array($requestArray[RequestCommonFiltersType::ORDER]))
                    {
                        if(count($requestArray[RequestCommonFiltersType::ORDER]) >= 1)
                        {
                            foreach($requestArray[RequestCommonFiltersType::ORDER] as $column => $order)
                            {
                                if($order != 'ASC' && $order != 'DESC')
                                    throw new WrongRequestException("Wrong order in $column - allowed ASC or DESC");
                                else
                                    $requestArray[RequestCommonFiltersType::ORDER][$column] = ($order == 'ASC' ? DSC::ASC : DSC::DESC);
                            }
                            $rcfilter->setSort($requestArray[RequestCommonFiltersType::ORDER]);
                            unset($requestArray[RequestCommonFiltersType::ORDER]);
                        }
                        else
                            throw new WrongRequestException("Order object exist in request but without parameters! Put at least one column name and order into object!");
                    }
                }
            }
            //if request was array than move filter at the beginnig of args array
            array_unshift($requestArray, $rcfilter);
        }
        else
            // if request was without arguments (can happen) than add default filter - for insert/update it will be removed in 
            // InvokeMethod becouse those methods doesn't cotnains RequestCommonFiltersType as first parameter
            $requestArray = array($rcfilter);
        $this->logger->logDebug("Invoking ClassProxy::invokeMethod with args : ".Utils::VarDumptoString($requestArray));
        return $this->invokeMethod($requestArray);
    }
}

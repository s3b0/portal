<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Utilty class with main tools for portal
 * @author h3x0r
 */
final class Utils {
    /**
     * Code taken from http://php.net/manual/en/function.array-key-exists.php
     * @param array $arrNeedles
     * @param array $arrHaystack
     * @param type $blnMatchAll
     * @return boolean
     */
    public static function ArrayMultiKeyExists(array $arrNeedles, array $arrHaystack, $blnMatchAll=true){
        $blnFound = array_key_exists(array_shift($arrNeedles), $arrHaystack);
   
        if($blnFound && (count($arrNeedles) == 0 || !$blnMatchAll))
            return true;
        if(!$blnFound && count($arrNeedles) == 0 || $blnMatchAll)
            return false;
        
        return array_multi_key_exists($arrNeedles, $arrHaystack, $blnMatchAll);
    }    
    /**
     * Change object to array
     * @param mixed $obj object for conversion
     * @return array array representation of pased object
     */
    public static function ObjectToArray($obj) {
	$_arr = is_object($obj) ? get_object_vars($obj) : $obj;
	foreach($_arr as $key => $val)
	{
	    $val = (is_array($val) || is_object($val)) ? self::ObjectToArray($val) : $val;
	    $arr[$key] = $val;
	}
	return $arr;
    }
    /**
     * Replace $search in $subject on $replace
     *
     * @param string search needle
     * @param string replace replacer
     * @param string subject string that will be parsed
     * @return string changed string
     */
    public static function StrLastReplace(string $search, string $replace, string $subject) {
	$search = trim($search);
	$pos = strrpos($subject, $search);
	if($pos !== false)
	    $subject = substr_replace($subject, $replace, $pos, strlen($search));
	return $subject;
    }
    /**
     * Redirect to error page with proper description
     * @param int HTTP code
     */
    public static function OperateError(int $code) {
	switch($code)
	{
	    case 100:
		$text = 'Continue';
	    break;
	    case 101:
		$text = 'Switching Protocols';
	    break;
	    case 200:
		$text = 'OK';
	    break;
	    case 201:
		$text = 'Created';
	    break;
	    case 202:
		$text = 'Accepted';
	    break;
	    case 203:
		$text = 'Non-Authoritative Information';
	    break;
	    case 204:
		$text = 'No Content';
	    break;
	    case 205:
		$text = 'Reset Content';
	    break;
	    case 206:
		$text = 'Partial Content';
	    break;
	    case 300:
		$text = 'Multiple Choices';
	    break;
	    case 301:
		$text = 'Moved Permanently';
	    break;
	    case 302:
		$text = 'Moved Temporarily';
	    break;
	    case 303:
		$text = 'See Other';
	    break;
	    case 304:
		$text = 'Not Modified';
	    break;
	    case 305:
		$text = 'Use Proxy';
	    break;
	    case 400:
		$text = 'Bad Request';
	    break;
	    case 401:
		$text = 'Unauthorized';
	    break;
	    case 402:
		$text = 'Payment Required';
	    break;
	    case 403:
		$text = 'Forbidden';
	    break;
	    case 404:
		$text = 'Not Found';
	    break;
	    case 405:
		$text = 'Method Not Allowed';
	    break;
	    case 406:
		$text = 'Not Acceptable';
	    break;
	    case 407:
		$text = 'Proxy Authentication Required';
	    break;
	    case 408:
		$text = 'Request TimeType-out';
	    break;
	    case 409:
		$text = 'Conflict';
	    break;
	    case 410:
		$text = 'Gone';
	    break;
	    case 411:
		$text = 'Length Required';
	    break;
	    case 412:
		$text = 'Precondition Failed';
	    break;
	    case 413:
		$text = 'Request Entity Too Large';
	    break;
	    case 414:
		$text = 'Request-URI Too Large';
	    break;
	    case 415:
		$text = 'Unsupported Media Type';
	    break;
	    case 500:
		$text = 'Internal Server Error';
	    break;
	    case 501:
		$text = 'Not Implemented';
	    break;
	    case 502:
		$text = 'Bad Gateway';
	    break;
	    case 503:
		$text = 'Service Unavailable';
	    break;
	    case 504:
		$text = 'Gateway TimeType-out';
	    break;
	    case 505:
		$text = 'HTTP Version not supported';
	    break;
	    default:
		$text= "Unknown http status code $httpCode";
	}
	$page = 'http://'.$GLOBALS[HOSTNAME]."/sources/portal/view/public/error/error.html?code=$code&msg=$text";
        header("Location: $page");
	die();
    } 
    /**
     * Changes var_dump($var) to string
     * @param mixed $var varaible to change
     * @return string representation of $var
     */
    public static function VarDumptoString($var) {
	ob_start();
	var_dump($var);
	$varToString = ob_get_clean();
	return $varToString;
    }
    /**
     * Check whenever $variable is null, empty(even if it is array)
     * @param mixed $variable
     * @return true if $variable is not empty
     */
    public static function IsNullOrEmptyVaraible($variable) {
        return ($variable == NULL || 
                ($variable != NULL && 
                 ( 
                    (is_array($variable) && count($variable) == 0) || (!is_array($variable) && (!isset($variable) || trim($variable)==='')) 
                 ) 
                )
               );
    }
    /**
     * Strips special signs from alaphabet
     * @param string to convert
     * @return string ASCII string
     */
    public static function ConvertToAsciString($string) {
	return iconv('UTF-8', 'us-ascii//TRANSLIT//IGNORE', $string);
    }
    /**
     * Removes $dir form filesystem
     * @param string $dir path for dir to remove
     */
    public static function RemoveDirs(string $dir) {
        $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) 
        {
            if($file->isDir())       
                rmdir($file->getRealPath());
            else 
                unlink($file->getRealPath());
        }
        rmdir($dir);
    }
    /**
     * Gets SHA1
     * @param string $seed more entropy
     * @return string unique sha1
     */
    public static function GetSha(string $seed = NULL) {
        if(self::IsNullOrEmptyVaraible($seed))
            $seed = uniqid(mt_rand(), true);
        return sha1($seed); //generuj token 40 znakowy;
    }
}

?>

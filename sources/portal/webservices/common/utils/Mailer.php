<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Out portal mailer - singleton
 *
 * @author sebo
 */
final class Mailer extends SingletonAbstr {
    /**
     * @var Logger loger
     */
    private $logger;
    /**
     * @var Mailer mailer instance
     */
    protected static $INSTANCE;
    /**
     * @var PHPMailer mailer 
     */
    private $mail;   
    /**
     * @var string tag that has been used in out mail temaplaits - it will be changed to text
     */
    private static $TEMPLATE_TAG = '{{%s}}';
    /*
     * List of defined templates
     */
    /**
     * Use this template: when user is added
     */
    const MAIL_TEMPLATE_USER_ADDED = 'user_added.html';
    /**
     * Use this template: when password for user was reset
     */
    const MAIL_TEMPLATE_USER_PASSWORD_RESET = 'password_reset.html';
    /**
     * Use this template: when someone send to us mail throught portal
     */
    const MAIL_TEMPLATE_CONTACT_FORM = 'contact_form.html';
    /**
     * Constructor
     */
    protected function __construct() {
        parent::__construct();
	$this->logger = new Logger(__CLASS__);
	$this->mail = new PHPMailer();
	$this->mail->IsSMTP();
	$this->mail->SMTPAuth = TRUE;       
	$this->mail->Host = SMTP_HOST;
	$this->mail->Username = SMTP_USER;
	$this->mail->Password = SMTP_PASSWORD;
	$this->mail->Port = SMTP_PORT;
	$this->mail->SMTPDebug = 0;
	$this->mail->CharSet = PORTAL_CHARSET;
	$this->mail->SMTPSecure = SMTP_TYPE;
	$this->mail->isHTML(TRUE);
    }
    /**
     * Prepares and send email to someone
     * @param string $mailBody mail body with replaced tags
     * @param string $subject subject for email
     * @param string $from sender email
     * @param array $to array of recipients
     * @return boolean true if email was send properly
     * @throws MailerException
     */
    private function sendMail(string $mailBody, string $subject, string $from, array $to) {       
        $this->logger->logDebug(Utils::VarDumptoString($to));

        $recipients = '';
        foreach($to as $email)
        {
            $this->mail->addAddress($email);
            $recipients .= "$email,";
        }
	$recipients = rtrim($recipients, ',');
	$this->mail->setFrom($from, PORTAL_NAME);
	$this->mail->addReplyTo($from);
	$this->mail->setLanguage('pl', dirname(__FILE__).'/../libs/PHPMailer/language/');
	$this->mail->Subject = $subject;
	$this->mail->Body = $mailBody;
        
        $log = "Mail(temat: $subject) do $recipients od $from został wysłany ";
        
        try
        {
            $this->mail->send();
        } 
        catch (Exception $e)
        {
            throw new MailerException($log.' nieprawidłowo!', $e);
        }

        $this->logger->logDebug($log.' prawidłowo.');
        return true;
    }
    /**
     * Sends email with given parameters
     * @param array $mailBodyReplacerArray replacer for placeholders (tags) in templates
     * @param string $templateName name of temaplte
     * @param string $subject subject of email
     * @param string $from sender of email
     * @parma array/string $to array of recipients
     * @return type
     */
    public function sendMailWithTemplate(array $mailBodyReplacerArray, string $templateName, string $subject = NULL, string $from = NULL, ...$to) {
        if(Utils::IsNullOrEmptyVaraible($subject))
            $subject = PORTAL_DEFAULT_EMAIL_SUBJECT;
        if(Utils::IsNullOrEmptyVaraible($from))
            $from = PORTAL_ADMIN_EMAIL_ADDRESS;
        
	$template = file_get_contents(PORTAL_TEMPLATE_EMAIL_PATH.$templateName);
	
	foreach($mailBodyReplacerArray as $key => $value)
	    $template = str_replace(sprintf(self::$TEMPLATE_TAG, $key), $value, $template);
                
	foreach(PORTAL_DEFAULT_EMAIL_VARS as $key => $value)//dopisz domyślne wartości
	    $mailBody = str_replace(sprintf(self::$TEMPLATE_TAG, $key), $value, $template);
                
        if (count($to) != count($to, COUNT_RECURSIVE))//sprawdź czy przekazano maile czy tablice maili - jeśli to drugie to zmień na jeden wymiar
            $to = call_user_func_array('array_merge', array_map('array_values', $to));
        
	return $this->sendMail($mailBody, $subject, $from, $to);
    }
}

<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Portal cache (APC) manager - <b>singleton</b>
 *
 * @author h3x0r
 */
final class Apc extends SingletonAbstr {
    /**
     * @var Apc instance
     */
    protected static $INSTANCE;
    /**
     * Ads/updates value in cache
     * @param string $key
     * @param string $value
     * @throws CacheException
     */
    public function maitain(string $key, string $value) {
        if(apcu_exists($key))
        {
            if(!apcu_store($key, $value))
                throw new CacheException("Wartoś dla klucza $key nie została uaktualniona!");
        }
        else
        {
            if(!apcu_add($key, $value))
                throw new CacheException("Wartoś dla klucza $key nie została dodana!");
        }
    }
    /**
     * Gets value form cache
     * @param string $key
     * @return mixed value from cache
     */
    public function get(string $key) {
        $value = NULL;
        if(apcu_exists($key))
            $value = apcu_fetch($key);
        return $value;
    }
    /**
     * Deletes value in cache
     * @param string $key of value to delete
     * @throws CacheException
     */
    public function delete(string $key) {
        if(apcu_exists($key))
        {
            if(!apcu_delete($key))
                throw new CacheException("Nie można usunąć podanego klucza $key!");    
        }
        else
            throw new CacheException("Nie można usunąć: podany klucz $key nie istnieje w cache-u!");
    }
    /**
     * Clears whole cache
     */
    public function clear() {
        apcu_clear_cache();
    }
}

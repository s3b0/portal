
<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Logger for portal
 *
 * @author h3x0r
 */
final class Logger {
    /**
     * @var KLogger logger implementation
     */
    private static $LOGGER = NULL;
    /**
     * @var string class name for wich we are creating this logger (will be displayed in log)
     */
    private $className = NULL;
    /**
     * Constructor
     * @param string @see Logger::$className
     */
    function __construct(string $className) {
        self::CreateInstance();
	$this->className = $className;
    }
    /**
     * Creates KLogger object for this logger @see Logger::$LOGGER
     */
    private static function CreateInstance() {
        if(self::$LOGGER == NULL)
	{
	    self::$LOGGER = new KLogger(LOGS_ROOT_CATALOG, LOG_LEVEL);
	    self::$LOGGER->setDateFormat(LOG_DATE_FORMAT);
	}
    }
    /**
     * Logs Errors (LOG_ERR)
     * @param string $message error message
     * @param Exception $e optional exception that was thrown
     */
    public function logError(string $message, Exception $e = NULL) {
        if(LOG_LEVEL >= LOG_ERR)
        {
            if($e == NULL)
                self::$LOGGER->log(LOG_ERR, $message, $this->className);
            else
                self::$LOGGER->log(LOG_ERR, $this->makeMessage($e, $message), $this->className, $e->getTrace());
        }
    }
    /**
     * Logs Warnings
     * @param string $message warning message
     * @param Exception $e optional exception that was thrown
     */
    public function logWarning(string $message, Exception $e = NULL) {
        if(LOG_LEVEL >= LOG_WARNING)
        {
            if($e == NULL)
                self::$LOGGER->log(LOG_WARNING, $message, $this->className);
            else
                self::$LOGGER->log(LOG_WARNING, $this->makeMessage($e, $message), $this->className, $e->getTrace());
        }
    }
    /**
     * Logs infos
     * @param string $message info message
     * @param Exception $e optional exception that was thrown
     */
    public function logInfo(string $message, Exception $e = NULL) {
        if(LOG_LEVEL >= LOG_INFO)
        {
            if($e == NULL)
                self::$LOGGER->log(LOG_INFO, $message, $this->className);
            else
                self::$LOGGER->log(LOG_INFO, $this->makeMessage($e, $message), $this->className, $e->getTrace());
        }
    }
    /**
     * Logs debuging message
     * @param string $message debug message
     * @param Exception $e optional exception that was thrown
     */
    public function logDebug(string $message, Exception $e = NULL) {
        if(LOG_LEVEL == LOG_DEBUG)
        {
            if($e == NULL)
                self::$LOGGER->log(LOG_DEBUG, $message, $this->className);
            else
                self::$LOGGER->log(LOG_DEBUG, $this->makeMessage($e, $message), $this->className, $e->getTrace());
        }
    }
    /**
     * Makes full message for log
     * @param Exception $e see any of method ex. @see Logger::logDebug($message, $e)
     * @param string $message see any of method ex. @see Logger::logDebug($message, $e)
     * @return string message in required format @link PortalConfig.php
     */
    private function makeMessage(Exception $e, string $message) {
        if(is_null($e))
            return $message;
        return sprintf(LOG_MSG_PATTERN, $e->getMessage(), $e->getFile(), $e->getLine(), $message);
    }
    /**
     * Used in any static method
     * @param int $severity log message severity
     * @param string $message see any of method ex. @see Logger::logDebug($message)
     * @param string $classname @see Logger::__construct($className)
     */
    public static function LogMessage(int $severity, string $message, string $classname) {
        if(LOG_LEVEL >= $severity)   
        {
            self::CreateInstance();
            self::$LOGGER->log($severity, $message, $classname);
        }
    }
}

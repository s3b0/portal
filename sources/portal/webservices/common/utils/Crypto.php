<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Crypto factory for portal
 *
 * @author h3x0r
 */
final class Crypto extends SingletonAbstr {
    /**
     * @var Crypto instance
     */
    protected static $INSTANCE;
    /**
     * Encrypts data
     * @param string $input string to encryption
     * @return string in base64 format encrypted with portal secret @const ENCRYPT_METHOD_SECRET_KEY and @const ENCRYPT_METHOD_SECRET_IV
     */
    public function encrypt(string $input) {
        return base64_encode(openssl_encrypt($input, ENCRYPT_METHOD, $this->getKey(), 0, $this->getIv()));
    }
    /**
     * Decrypts data
     * @param string $input encrypted with @see Crypto::encrypt($input) string
     * @return string decrypted string
     */
    public function decrypt(string $input) {
        return openssl_decrypt(base64_decode($input), ENCRYPT_METHOD, $this->getKey(), 0, $this->getIv());
    }
    /**
     * Returns hashed @const ENCRYPT_METHOD_SECRET_KEY
     * @return string hash of @const ENCRYPT_METHOD_SECRET_KEY
     */
    private function getKey() {
        // hash
        return hash('sha256', ENCRYPT_METHOD_SECRET_KEY);
    }
    /**
     * Returns hashed and trimed @const ENCRYPT_METHOD_SECRET_IV
     * @return string hashed and trimed @const ENCRYPT_METHOD_SECRET_IV
     */
    private function getIv() {
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        return substr(hash('sha256', ENCRYPT_METHOD_SECRET_IV), 0, 16);
    }

}

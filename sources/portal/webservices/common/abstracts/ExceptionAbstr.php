<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Abstract class of portal excpetions
 *
 * @author h3x0r
 */
class ExceptionAbstr extends Exception {
    /**
     * @var Logger standard logger 
     */
    private $logger;
    /**
     * @var Exception prevoise exception
     */
    protected $priorException;
    
    /**
     * Code for SQL excpetions
     */
    const SQL_EXCEPTION = 1;
    /**
     * Code for schema exceptions
     */
    const WS_SCHEMA_EXCEPTION = 2;
    /**
     * Code for webservices expeptions
     */
    const WS_EXCEPTION = 3;
    /**
     * Code for auth(tentication/orization) expeptions
     */
    const AUTH_EXCEPTION = 4;
    /**
     * Code for for not used methods expeptions
     */
    const NOT_USED_EXCEPTION = 5;
    /**
     * Code for utils  expeptions
     */
    const UTILS_EXCEPTION = 6;
    /**
     * Code for general security expeptions
     */
    const SECURITY_EXCEPTION = 7;
    /**
     * Code for general expeptions
     */
    const GENERAL_EXCEPTION = 8;
    /**
     * Code for mailers expeptions
     */
    const MAIL_EXCEPTION = 9;
    /**
     * Code for file operations expeptions
     */
    const FILE_EXCEPTION = 10;
    /**
     * Code for config-param expeptions
     */
    const CF_EXCEPTION = 11;
    /**
     * Code for bad function calls expeptions
     */
    const BAD_FUNCTION_CALL_EXCEPTION = 12;
    /**
     * Code for cache expeptions
     */
    const CACHE_EXCEPTION = 13;
    /**
     * Constructor
     * @param string $message exception messege
     * @param int $code exception code
     * @param Exception $previous previouse excpetion (if avaiable)
     */
    function __construct(string $message, int $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
	$this->logger = new Logger(get_class($this));
	$this->logger->logError("Error: $message, Code: $code");
	if(!Utils::IsNullOrEmptyVaraible($previous))
	    $this->logger->logError('Previous error: ', $previous);
    }
    /**
     * Gets prevoiuse exception
     * @return Exception 
     */
    public function getPrior() {
	return $this->priorException;
    }
    /**
     * Gets logger
     * @return Logger
     */
    protected function getLogger() {
        return $this->logger;
    }
}

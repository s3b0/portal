<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Abstract class of portal singletons
 *
 * @author h3x0r
 */
abstract class SingletonAbstr {
    /**
     * @var mixed instance of child class
     */
    protected static $INSTANCE;
    /**
     * @var Logger Loger
     */
    private $logger;
    /**
     * Constructor
     */
    protected function __construct() {
	$this->logger = new Logger(get_class($this));     
    }
    /**
     * Gets singleton logger
     * @return Logger
     */
    protected function getLogger() {
        return $this->logger;
    }
    /**
     * Gets instance
     * @return Singleton
     */
    public final static function getInstance() {
        if (static::$INSTANCE === NULL)
            static::$INSTANCE = new static();
        return static::$INSTANCE;
    }
}

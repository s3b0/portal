<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Events class - only for authorized users
 *
 * @author h3x0r
 */
final class EventsWsAuthorized extends EventsWs implements DictAuthInter, DeleteAuthInter {
    use AuthTrait;
    use DictAuthTrait;
    /**
     * Creates EventType @see EventsTypesDictEntity
     * @param StringType $name name of event type
     * @param StringType $description description of event type
     * @return BooleanType true on success
     */
    public function createType(StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->createCommonDictType(new EventsTypesDictEntity(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Deletes EventType @see EventsTypesDictEntity
     * @param IntegerType $id of event type
     * @param StringType $name name of event type
     * @return BooleanType true on success
     * @throws WrongRequestException no type in db
     */
    public function deleteType(StringType $name) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->deleteCommonDictType(new EventsTypesDictEntity(), $name->getRequest());
    }
    /**
     * Updates EventType @see EventsTypesDictEntity
     * @param IntegerType $id of event type
     * @param StringType $name new name of event type
     * @param StringType $description new description of event type
     * @return BooleanType true on success
     * @throws WrongRequestException no type in db
     */
    public function updateType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->updateCommonDictType(new EventsTypesDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Creates an event @see EventsEntity
     * @param StringType $name name of event
     * @param StringType $description description of event
     * @param char $cycle cycle of event @see EventsCyclesDictEntity
     * @param byte $ocurrence occurnce of event as byte example: event only in monaday 00000001, whole week event 111111111
     * @param StringType $type type of event @see EventsTypesDictEntity
     * @param date $startDate start date
     * @param time $startTime start hour
     * @param time $endTime end date 
     * @param date $endDate end hour
     * @param IntegerType $group group that event belongs to
     * @return BooleanType true on success
     */
    public function create(StringType $name, StringType $description, CharType $cycle, StringType $type, DateType $startDate,  
            TimeType $startTime, TimeType $endTime, ByteType $ocurrence = NULL, DateType $endDate = NULL, IntegerType $group = NULL) {
        $nameType = $name->getRequest();
        $descriptionType = $description->getRequest();
        $cycleType = $cycle->getRequest();
        $typeType = $type->getRequest();
        $startDateType = $startDate->getRequest();
        $startTimeType = $startTime->getRequest();
        $endTimeType = $endTime->getRequest();
        $ocurrenceType = $ocurrence->getRequest();
        $endDateType = $endDate->getRequest();
        $groupType = $group->getRequest();
        
        $this->authorize($this->getAllowedGroupRoles($groupType, GroupsWsAuthorized::ADMIN), 
                  UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
        $this->checkEndtime($startTimeType, $endTimeType);
        
        $event = new EventsEntity();
        $event->setCycle($cycleType);
        $event->setDescription($descriptionType);
        $event->setEndDate($this->checkCycleDependParameters($cycleType, $ocurrenceType, $startDateType, $endDateType));
        $event->setGroup($groupType);
        $event->setAdder($this->getLoggedUserId());
        $event->setEndHour($endTimeType);
        $event->setName($nameType);
        $event->setOCurrEncE($ocurrenceType);
        $event->setStartDate($this->getSQL()->createSqlDateString($startDateType));
        $event->setStartHour($startTimeType);
        $event->setType($typeType);
        $event->insertIntoDatabase($this->getPDO());
    }
    /**
     * Updates an event @see EventsEntity
     * @param $id id of event
     * @param StringType $name name of event
     * @param StringType $description description of event
     * @param char $cycle cycle of event @see EventsCyclesDictEntity
     * @param byte $ocurrence occurnce of event as byte example: event only in monaday 00000001, whole week event 111111111
     * @param StringType $type type of event @see EventsTypesDictEntity
     * @param date $startDate start date
     * @param time $endDate start hour
     * @param time $startTime end date 
     * @param date $endTime end hour
     * @param IntegerType $group group that event belongs to
     * @return BooleanType true on success
     * @throws WrongRequestException no event in db
     */
    public function update(IntegerType $id, StringType $name, CharType $cycle, DateType $startDate, TimeType $endTime,
            TimeType $startTime, ByteType $ocurrence = NULL, StringType $description = NULL, DateType $endDate = NULL, IntegerType $group = NULL, 
            StringType $type = NULL) {
        $idType = $id->getRequest();
        $nameType = $name->getRequest();
        $descriptionType = $description->getRequest();
        $cycleType = $cycle->getRequest();
        $typeType = $type->getRequest();
        $startDateType = $startDate->getRequest();
        $startTimeType = $startTime->getRequest();
        $endTimeType = $endTime->getRequest();
        $ocurrenceType = $ocurrence->getRequest();
        $endDateType = $endDate->getRequest();
        $groupType = $group->getRequest();
        
        $this->authorize($this->getAllowedGroupRoles($groupType, GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
                  UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
        $this->checkEndtime($startTimeType, $endTimeType);
        
        $event = EventsEntity::findById($this->getPDO(), $idType);
        if(!Utils::IsNullOrEmptyVaraible($event))
        {
            $event->setCycle($cycleType);
            $event->setDescription($descriptionType);            
            $event->setEndDate($this->checkCycleDependParameters($cycleType, $ocurrenceType, $startDateType, $endDateType));
            $event->setGroup($group->getRequest());
            $event->setEndHour($endTimeType);
            $event->setName($nameType);
            $event->setOCurrEncE($ocurrenceType);
            $event->setStartDate($this->getSQL()->createSqlDateString($startDateType));
            $event->setStartHour($startTimeType);
            $event->setType($typeType);
            $event->updateToDatabase($this->getPDO());
        }
        else
            throw new WrongRequestException('Podany termin nie istnieje!');
    }
    /**
     * Deletes event
     * @param $id id of event
     * @return BooleanType true on success
     * @throws WrongRequestException no event in db
     */
    public function delete(IntegerType $id) {
        $idType = $id->getRequest();
        
        $event = EventsEntity::findById($this->getPDO(), $idType);
        
        if(Utils::IsNullOrEmptyVaraible($event))
            throw new WrongRequestException("Zdarzenie o podanym id $idType nie istnieje!");
        
        $this->authorize($this->getAllowedGroupRoles($event->getGroup(), 
                GroupsWsAuthorized::ADMIN), 
                  UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
        $event->deleteFromDatabase($this->getPDO());
    }
    /**
     * Checks correctness of end date of event, cyclic event doesn't have end date 
     * @param char $cycle cycle of event
     * @param date $endDate of event
     * @throws WrongRequestException if cycle must have end date
     */
    private function checkCycleDependParameters($cycle, $ocurrence, $startDate, $endDate) {
        switch($cycle)
        {
            case EventsWs::PERIOD_EVENT:
                if(Utils::IsNullOrEmptyVaraible($endDate))
                    throw new WrongRequestException('Wydarzenie okresowe musi mieć datę zakończenia!');
                elseif(self::CheckIfFromTimeGraterThanToTime($startDate, $endDate))
                    throw new WrongRequestException('Data zakończenia musi nastąpić po dacie rozpoczęcia!');
            return $this->getSQL()->createSqlDateString($endDate);
            case EventsWs::ONETIME_EVENT:
                if(!Utils::IsNullOrEmptyVaraible($endDate))
                    throw new WrongRequestException('Wydarzenie jednorazowe nie może mieć podanej daty zakończenia(data zakończenia = data rozpoczęcia)!');
                elseif(!Utils::IsNullOrEmptyVaraible($ocurrence))
                    throw new WrongRequestException('Wydarzenie jednorazowe nie wymaga podawania wystąpięnia!');
            return $this->getSQL()->createSqlDateString($startDate);
            case EventsWs::CYCLIC_EVENT:
                if(!Utils::IsNullOrEmptyVaraible($endDate))
                    throw new WrongRequestException('Wydarzenie cykliczne nie może mieć daty zakończenia!');
            break;
            default:
                throw new WrongRequestException('Nieznay typ cyklu!');
        }
    }
    private function checkEndtime($startTime, $endTime) {
        if(self::CheckIfFromTimeGraterThanToTime($startTime, $endTime))
            throw new WrongRequestException('Czas zakończenia musi nastąpić po czasie rozpoczęcia!');
    }
    
    private static function CheckIfFromTimeGraterThanToTime($from, $to) {
        return strtotime($from) > strtotime($to);
    }
}

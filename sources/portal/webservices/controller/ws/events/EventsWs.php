<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Events class - only for authorized users
 *
 * @author h3x0r
 */
class EventsWs extends WsAbstr implements DictInter {
    
    const CYCLIC_EVENT = 'C';
    const ONETIME_EVENT= 'O';
    const PERIOD_EVENT = 'P';
    
    const WHOLE_WEEK = 127; //cały tydzień
    const MONDAY = 1;// Poniedziałek
    const TUESDAY = 2; // Wtorek
    const WEDNESDAY = 4; // Środa
    const THURSDAY = 8; //Czwartek
    const FRIDAY = 16; //Piątek
    const SATURDAY = 32; //sobota
    const SUNDAY = 64; //Niedziela
    
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }

    private function filterEvents(RequestCommonFiltersType $rcfilter, string $from, string $to, string $type = NULL) {
        $query = 'SELECT * FROM EVENTS WHERE
                    -- onetime
                    (start_date between '.$this->getSQL()->createSqlDateString($from).' and '.$this->getSQL()->createSqlDateString($to).' and cycle = \'O\')
                    or --  and period
                    ('.$this->getSQL()->createSqlDateString($from).' between '.$this->getSQL()->createSqlDateString($from).' and end_date and cycle = \'P\')
                    or -- cycle
                    ('.$this->getSQL()->createSqlDateString($to).' between start_date and '.$this->getSQL()->createSqlDateString($to).'and end_date is null)';
        
	if(!Utils::IsNullOrEmptyVaraible($type))
	    $query .= " AND type = '".$type."' ";
        
        $query .=  $rcfilter->getAsSql(new EventsEntity());
        
 	return $this->getSQL()->dql($query);
    }
    /**
     * Pobiera zdarzenia z kalendarza o podanych parametrach
     * @param RequestCommonFiltersType standard request filter
     * @param StringType from Data od
     * @param StringType to Data do
     * @param StringType eventType Typ wydarzenia
     */
    public function getEvents(RequestCommonFiltersType $rcfilter, DateType $from, DateType $to, StringType $type = NULL) {             
 	return $this->filterEvents($rcfilter, $from->getRequest(), $to->getRequest(), $type->getRequest());
    }
    /**
     * Zwraca wydarzenia dla danego miesiąca
     * @param RequestCommonFiltersType standard request filter
     * @param int monthInYear Miesiąc w roku - opcjonalnie, jeśli nie podano zwraca wydarzenia dla bieżącego miesiąca
     * @return array events for month
     */
    public function getMonthEvents(RequestCommonFiltersType $rcfilter, IntegerType $month = NULL) {
        $monthType = $month->getRequest();
	if(!Utils::IsNullOrEmptyVaraible($monthType))
	{
	    $monthInYear = date('m');
	    //$monthInYear--;
	}
        else
            $monthInYear = $monthType;
        
        $from = new DateTime();
	$to = new DateTime();
	$from->setDate(date('Y'), $monthInYear, '01');
	$to->setDate(date('Y'), $monthInYear, date('t'));
	return $this->filterEvents($rcfilter, $from->format(DATE_FORMAT), $to->format(DATE_FORMAT));
    }
    /**
     * Zwraca wydarzenia dla wskazanego tygodnia
     * @param RequestCommonFiltersType standard request filter
     * @param int weekInYear Numer tygodnia w roku - opcjonalna, jeśli nie podano zwraca wydarzenia dla bieżącego miesiąca
     * @return array events for week
     */
    public function getWeekEvents(RequestCommonFiltersType $rcfilter, IntegerType $week = NULL) {
	$from = new DateTime();
        
        $weekInYear = $week->getRequest();
	if(Utils::IsNullOrEmptyVaraible($weekInYear))
	    $weekInYear = date('W');
        
	$from->setISODate(date('Y'), $weekInYear, 1);
        $fromDateString = $from->format(DATE_FORMAT);
	$to = new DateTime($fromDateString);
	$to->modify('+ 6 days');
	return $this->filterEvents($rcfilter, $fromDateString, $to->format(DATE_FORMAT));
    }
    /**
     * Pobiera wydarzenia dla wskazanego dnia
     * @param RequestCommonFiltersType standard request filter
     * @param int dayInYear Dzień w roku - jeśli nie podany zwracany są dzisiejsze wydarzenia
     * @return array events for day
     * @access public
     */
    public function getDayEvents(RequestCommonFiltersType $rcfilter, IntegerType $day = NULL) {
        $dayType = $day->getRequest();
	$date = date(DATE_FORMAT); //pobierz datę dla dzisiaj
	if(!Utils::IsNullOrEmptyVaraible($dayType))//jeśli przekazano dzień
	{
	    $dateTime = new DateTime('01-01-' . date('Y'));
	    $dateTime->modify('+'.($dayType-1).' days');
	    $date = $dateTime->format(DATE_FORMAT);
	}
        return $this->filterEvents($rcfilter, $date, $date);
    }
    /**
     * Pobiera opisy cykli do legendy
     * @param RequestCommonFiltersType standard request filter
     * @return EventsCyclesDictEntity[] cykle wydarzeń
     */
    public function getEventsCycles(RequestCommonFiltersType $rcfilter) {
	return EventsCyclesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new EventsCyclesDictEntity));
    }
    /**
     * Returns types of events
     * @param RequestCommonFiltersType standard request filter
     * @return EventsTypesDictEntity[] types of events
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
	return EventsTypesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new EventsTypesDictEntity()), $rcfilter->getPaginator());
    }
    
}
?>

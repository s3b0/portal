<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Contact class - public
 *
 * @author h3x0r
 */
final class ContactsWsAuthorized extends ContactsWs implements DictAuthInter {
    use AuthTrait;
    use DictAuthTrait;
    /**
     * Creates subject
     * @param StringType $name of subject
     */
    public function createType(StringType $name, StringType $description) {       
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->createCommonDictType(new ContactsTypesDictEntity(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Deletes subject
     * @param IntegerType $id of subject
     */
    public function deleteType(StringType $name) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->deleteCommonDictType(new ContactsTypesDictEntity(), $name->getRequest());
    }
    /**
     * Updates subject
     * @param IntegerType $id of subject
     * @param StringType $name pf subject
     * @throws WrongRequestException
     */
    public function updateType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->updateCommonDictType(new ContactsTypesDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }
    
    /**
     * Appends/removes system roles from subjects (contacts)
     * @param StringType $role
     * @param array $subjects
     */
    public function appendSystemRoleToSubject(BundleType $roles, BundleType $types) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $values = array();
        $rolesIn = NULL;
        foreach($roles->getRequest() as $role)
        {
            $rolesIn.="'$role',";
            $value = "('$role',";
            foreach($types->getRequest() as $subject)
                $value .= "'$subject')";
            array_push($values, $value);
        }
        $this->getPDO()->beginTransaction();
        try
        {
            //clean in db
            $this->getSQL()->dml('delete from CONTACTS_SUBJECTS_OF_ROLES WHERE `role` in ('.rtrim($rolesIn, ',').')');
            $this->getSQL()->dml('INSERT INTO CONTACTS_SUBJECTS_OF_ROLES (`role`,`subject`) VALUES '.implode(',', $values));
            $this->getPDO()->commit();            
        }
        catch(Exception $e)
        {
            $this->getPDO()->rollBack();
            throw new SqlException('Powiązanie ról z tematami nieudane!', $e);
        }
    }
}
?>

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Contact class - public
 *
 * @author h3x0r
 */
class ContactsWs extends WsAbstr implements DictInter {
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }
    /**
     * Pobiera tematy do formularza kontaktowego
     * @return ContactsSubjectsEntity[] tematy w jakich można kontaktować się z obsługą portalu
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
	return ContactsTypesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new ContactsTypesDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Wysyła maila do grup na portalu
     *
     * @param StringType senderMail Mail wysyłającego
     * @param StringType subject Temat wybrany z listy
     * @param bool copy Określa czy wysłać kopię do nadawcy
     * @param StringType contents Treść maila
     * @param StringType captcha CaptchaType do walidacji
     * @return BooleanType true on success
     * @throws WrongRequestException if captcha is incorrect
     */
    public function send(EmailType $sender, StringType $type, BooleanType $copy, StringType $content, CaptchaType $captcha) {
        $typeType = $type->getRequest();
        $copyType= $copy->getRequest();
        $contentType = $content->getRequest();
        
	$rows = $this->getSQL()->dql("SELECT email from USERS as u, CONTACTS_SUBJECTS_OF_ROLES as csor where u.`role` = csor.`role` and csor.subject = '$typeType'");
        if(!Utils::IsNullOrEmptyVaraible($rows))
        {
            $mails = array();
            foreach($rows as $row)
                array_push($mails, $row['email']);
            if($copyType)
                array_push($mails, $sender->getRequest());
            Mailer::GetInstance()->sendMailWithTemplate(array('content' => $contentType), Mailer::MAIL_TEMPLATE_CONTACT_FORM, $typeType, NULL, $mails);    
        }
        else
            throw new WrongRequestException('Podałeś niepoprawne dane!');
    }
}
?>

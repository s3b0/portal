<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Portal class - only for authorized users
 *
 * @author h3x0r
 */
final class ConfigWsAuthorized extends ConfigWs {
    use AuthTrait;
    const DB_PARAM_NAME_FORBIDDEN_PHRASES = 'forbidden_phrases';
    const DB_PARAM_NAME_FORBIDDEN_PHONES = 'forbidden_phones';
    const DB_PARAM_NAME_FORBIDDEN_MAILS = 'forbidden_emails';
    const DB_PARAM_NAME_RULES_URL = 'rules_url';
    const CF_PARAM_NAME_SLIDER_GROUP_NAME = 'slider';
    const CF_PARAM_NAME_SLIDER_COUNT = 'count';
    const CF_PARAM_NAME_SLIDER_COUNT_INTERVAL = 'count_interval';
    const CF_PARAM_NAME_ARTICLES_GROUP_NAME = 'articles';
    const CF_PARAM_NAME_ARTICLES_STICKY_COUNT = 'sticky_count';
    const CF_PARAM_NAME_ARTICLES_ORDYNARY_COUNT = 'ordynary_count';
    const CF_PARAM_NAME_LINK_PREFIX_GROUP_NAME = 'link';
    const CF_PARAM_NAME_LINK_COUNT = 4;
    const CF_PARAM_NAME_LINK_PREFIX_URL = 'url';
    const CF_PARAM_NAME_LINK_PREFIX_LOGO_URL = 'logo_url';
    const CF_PARAM_NAME_LINK_PREFIX_LOGO_LOCAL_PATH = 'logo_local_path';
    
    public function getParametersNames() {
        $parametersNames = array(
            self::DB_PARAM_NAME_FORBIDDEN_PHRASES => array(), 
            self::DB_PARAM_NAME_FORBIDDEN_PHONES => array(),
            self::DB_PARAM_NAME_RULES_URL => array(),
            self::CF_PARAM_NAME_SLIDER_GROUP_NAME => array(
                    self::CF_PARAM_NAME_SLIDER_COUNT, self::CF_PARAM_NAME_SLIDER_COUNT_INTERVAL, self::CF_PARAM_NAME_SLIDER_COUNT_INTERVAL
            ),
            self::CF_PARAM_NAME_ARTICLES_GROUP_NAME => array(
                self::CF_PARAM_NAME_ARTICLES_GROUP_NAME, self::CF_PARAM_NAME_ARTICLES_STICKY_COUNT, self::CF_PARAM_NAME_ARTICLES_ORDYNARY_COUNT
            )
        );
        $linkParamsArray = array(
                self::CF_PARAM_NAME_LINK_PREFIX_URL, self::CF_PARAM_NAME_LINK_PREFIX_LOGO_URL, self::CF_PARAM_NAME_LINK_PREFIX_LOGO_LOCAL_PATH
        );
        for($i=1; $i<=self::CF_PARAM_NAME_LINK_COUNT; $i++)
        {
            $link = self::CF_PARAM_NAME_LINK_PREFIX_GROUP_NAME.'#'.$i;
            $parametersNames[$link] = $linkParamsArray;
        }
        return $parametersNames;
    }
    /**
     * Gets configuration parameter from Db (relation or file)
     * @param StringType $paramName name of parameter
     * @param StringType $sectionName section of parameter (only for file db accesss)
     * @return StringType value of parameter
     */
    public function getParameter(StringType $name, StringType $section = NULL) {
        $nameType = $name->getRequest();
        $sectionType = $section->getRequest();
        
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $parameter = NULL;
        if(Utils::IsNullOrEmptyVaraible($sectionType))
        {
            $parameter = Apc::getInstance()->get($nameType);
            if(Utils::IsNullOrEmptyVaraible($parameter))//if we dont have value aleredy cached
            {//cache only DB params!
                $parameter = $this->getDbParameter($nameType);
                Apc::getInstance()->maitain($nameType, $parameter);
            }
        }
        else
            $parameter = $this->getCfParameterValue($sectionType, $nameType);
        
        if(Utils::IsNullOrEmptyVaraible($parameter))
            throw new WrongRequestException('Niepoprawne żądanie!');
        return $parameter;
    }
    /**
     * Gets relation db parameter
     * @param StringType $paramName name of parameter
     * @return StringType value of parameter
     * @throws WrongRequestException if parameter does not exist
     */
    private function getDbParameter($paramName) {
        $portalConfigParamValue = PortalConFigEntity::findById($this->getPDO(), $paramName);
        if(!Utils::IsNullOrEmptyVaraible($portalConfigParamValue))
            return $portalConfigParamValue->getValue();
        return NULL;
    }
    /**
     * Gets file db parameter
     * @param type $sectionName section of parameter
     * @param StringType $paramName name of parameter
     * @return StringType value of parameter
     * @throws WrongRequestException if parameter does not exist
     */
    private function getCfParameterValue($sectionName, $paramName) {
        $portalMainParamValue = $this->GetCF()->getConfigParams($sectionName, $paramName);
        if(!Utils::IsNullOrEmptyVaraible($portalMainParamValue))
            return $portalMainParamValue;
        return NULL;
    }
    /**
     * Gets configuration parameter from Db (relation or file)
     * @param StringType $name of parameter
     * @param StringType $value of parameter
     * @param StringType $section of parameter (only for file db accesss)
     * @return BooleanType true on success
     */
    public function maintain(StringType $name, StringType $value, StringType $section = NULL) {
        $nameType = $name->getRequest();
        $valueType = $value->getRequest();
        $sectionType = $section->getRequest();
        
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $parameters = $this->getParametersNames();
        echo (!Utils::IsNullOrEmptyVaraible($sectionType) && (!array_key_exists($sectionType, $parameters) || !in_array($nameType, $parameters[$sectionType])));
        
        if((Utils::IsNullOrEmptyVaraible($sectionType) && !array_key_exists($nameType, $parameters)) || 
           (!Utils::IsNullOrEmptyVaraible($sectionType) && (!array_key_exists($sectionType, $parameters) || !in_array($nameType, $parameters[$sectionType]))))
            throw new WrongRequestException("Parametr $nameType nie istnieje! Utwórz go w bazie(db lub plik) i spróbuj ponownie!");
                
        if(Utils::IsNullOrEmptyVaraible($sectionType))
            $this->setDbParameter($nameType, $valueType);
        else
            $this->setCfParameter($sectionType, $nameType, $valueType);
    }
    /**
     * Sets relation db parameter
     * @param StringType $paramName name of parameter
     * @param StringType $paramValue value of parameter
     */
    private function setDbParameter($paramName, $paramValue) {
        $portalConfigParamValue = $portalConfigParamValue = PortalConFigEntity::findById($this->getPDO(), $paramName);
        $portalConfigParamValue->setValue($paramValue);
        $portalConfigParamValue->updateToDatabase($this->getPDO());
        Apc::getInstance()->maitain($paramName, $paramValue);
    }
    /**
     * Sets file db parameter
     * @param StringType $sectionName section of parameter
     * @param StringType $paramName name of parameter
     * @param StringType $paramValue value of parameter
     */
    private function setCfParameter($sectionName, $paramName, $paramValue) {
        $this->getCfParameterValue($sectionName, $paramName);
        $this->getCF()->setConfigParams($sectionName, $paramName, $paramValue);
    }
}

<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Description of CacheWsAuthorized
 *
 * @author h3x0r
 */
final class CacheWsAuthorized extends WsAbstr {
    use AuthTrait;
    public function info() {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        return apcu_cache_info();
    }
    public function delete(StringType $key) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        Apc::getInstance()->delete($key->getRequest());
    }
    public function clear() {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        Apc::getInstance()->clear();
    }
}

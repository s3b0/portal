<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Portal class - public
 *
 * @author h3x0r
 */
class ConfigWs extends WsAbstr {   
    /**
     * Constructor
     */
    function __construct() {
	parent::__construct();
    }
    /**
     * Pobiera województwa
     * @return StringType[] województwa
     */
    public function getVoivodeships(StringType $country) {
        $countryType = $country->getRequest();
        if(is_array(VOIVODESHIPS[$countryType]))
            return VOIVODESHIPS[$countryType];
        throw new WrongRequestException('Niepoprawne żądanie!');
    }
    /**
     * Sprawdza czy podane województwo jest obecne w konfiguracji portalu
     * @param StringType $voivodeship numer wojewodztwa
     * @return BooleanType true jeśli podane województwo istnieje
     */
    public function checkVoivodeships(StringType $voivodeship, StringType $coutry) {
	return array_key_exists($voivodeship->getRequest(), $this->getVoivodeships($coutry));
    }
}

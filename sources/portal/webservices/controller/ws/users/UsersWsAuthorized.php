<?php
require_once 'config/PortalConfig.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserWs
 *
 * @author sebo
 */
final class UsersWsAuthorized extends UsersWs implements DeleteAuthInter {
    use AuthTrait;

    const ADMIN_TECH = 'AT';
    const ADMIN_PORT = 'AP';
    const ADMIN_EDIT = 'EP';
    const ADMIN_GROUP = 'AGP/EGP';
    
    /**
     * Resetuje hasło użytkownika
     * @param StringType $token token autoryzacyjny
     * @param IntegerType $id id użytkownika do uaktualnienia hasła
     * @return StringType status operacji
     */
    public function resetPassword(IntegerType $id) {        
	$user = UsersEntity::findById($this->getPDO(), $id->getRequest());
	if(!Utils::IsNullOrEmptyVaraible($user))//id było poprawne
	{
            $login =  $user->getLogIn();
            $email = $user->getEmail();
	    $password = self::RandomPassword();
	    $user->setPassword($password);
            $user->updateToDatabase($this->getPDO());//hasło w formacie MD5
            Mailer::GetInstance()->sendMailWithTemplate(array('login' => $user->getLogIn(), 'password' => $password), Mailer::MAIL_TEMPLATE_USER_PASSWORD_RESET, 
                    'Hasło zostało zresetowane.', NULL, $email);
	}
        else
            throw new WrongRequestException("Niepoprawne wywołanie!");
        
    }
    /**
     * Generator losowy hasła
     * @return StringType hasło
     */
    private static function RandomPassword() {
	$pass = array(); //remember to declare $pass as an array
	$alphaLength = strlen(PASSWORD_SIGNS) - 1; //put the length -1 in cache
	for ($i = 0; $i < PASSWORD_LENGHT; $i++) 
	{
	    $n = rand(0, $alphaLength);
	    $pass[] = PASSWORD_SIGNS[$n];
	}
	return implode($pass); //turn the array into a StringType
    }
     /**
     * Dodaje użytkownika (walidacja maila, duplikacji i województwa)
     * @param StringType $token token autoryzacyjny
     * @param StringType $nameAndSurname imię i nazwisko
     * @param StringType $email mail
     * @param StringType $login login w portalu
     * @param StringType $systemRole rola systemowa
     * @param BooleanType $dane_osobowe_przetwarzanie_potwierdzone jeśli TRUE to zanczy, że admin zaakceptował przetwarzanie danych osobowych podczas dodawania
     * @param BooleanType $dane_osobowe_przetwarzanie jeśli TRUE trzeba wysłać maila z prośbą o potwierdzenie przetwqarzania danych osobowych
     * @param BooleanType $active czy aktywny
     * @param StringType $addressStreet ulica zamieszkania
     * @param StringType $addressHouseFlatNumber numer domu
     * @param StringType $addressVoivodeship województwo w postaci kodu
     * @param StringType $addressCity miasto
     * @param IntegerType $avatar awatar użytkownika
     * @return StringType wiadomoś o statusie dodawania użytkownika
     */
    public function create(StringType $name, StringType $login, EmailType $email, StringType $role, StringType $address, BooleanType $active, 
            StringType $avatar = null, BooleanType $accepted = null, StringType $notes = null) {
        $nameType = $name->getRequest();
        $loginType = $login->getRequest();
        $emailType = $email->getRequest();
        $roleType = $role->getRequest();
        $addressType = $address->getRequest();
        $activeType = $active->getRequest();
        $avatarType = $avatar->getRequest();
        $acceptedType = $accepted->getRequest();
        $notesType = $notes->getRequest();
        
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        
	$user = new UsersEntity();
        $user->setAvatar($avatarType);
	$user->setName($nameType);
	$user->setEmail($emailType);
	$user->setLogIn($loginType);
	$user->setRole($roleType);
	$user->setAddress($addressType);
        $user->setActive($activeType);
        $user->setPersonalDataAccepted($acceptedType);
	$user->setAdder($this->getLoggedUserId());
        $user->setNotes($notesType);
        $password = self::RandomPassword();
	$user->setPassword(md5($password));
	
        $user->insertIntoDatabase($this->getPDO());
                     
        $link = $GLOBALS[URI].'/'.sprintf(PortalConFigEntity::findById($this->getPDO(), ConfigWsAuthorized::DB_PARAM_NAME_RULES_URL)->getValue(), 
        Crypto::getInstance()->encrypt($emailType));
        
	Mailer::GetInstance()->sendMailWithTemplate(array('role' => $roleType, 'login' => $loginType, 'password' => $password, 'link' => $link), 
                Mailer::MAIL_TEMPLATE_USER_ADDED, 'Zostałeś dodany w poczet użytkowników portalu.', NULL, $emailType);
    }
    
        /**
     * Pobiera użytkownikow portalu do listy według ich uprawnień i grup
     * @param StringType $token token autoryzacyjny
     * @param StringType $nameAndSurname imię i nazwisko
     * @param StringType $login login
     * @param StringType $email
     * @param IntegerType $addedBefore dodany przed
     * @param IntegerType $addedAfter dodany po 
     * @param BooleanType $isActive czy aktywny
     * @param IntegerType $offset przesunięcie tabeli
     * @return JSON json
     */
    public function getUsers(RequestCommonFiltersType $rcfilter, BooleanType $active, StringType $name = NULL, StringType $login = NULL, EmailType $email = NULL, 
            DateType $from = NULL, DateType $to = NULL) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        
        $activeType = $active->getRequest();
        $nameType = $name->getRequest();
        $loginType = $login->getRequest();
        $emailType = $email->getRequest();
        $addedBeforeType = $to->getRequest();
        $addedAfterType = $from->getRequest();
        
	$filter = array(new DFC(UsersEntity::FIELD_ACTIVE, $activeType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($nameType))
            array_push($filter, new DFC(UsersEntity::FIELD_NAME, $nameType, DFC::CONTAINS));
        if(!Utils::IsNullOrEmptyVaraible($loginType))
            array_push($filter, new DFC(UsersEntity::FIELD_LOGIN, $loginType, DFC::CONTAINS));
        if(!Utils::IsNullOrEmptyVaraible($emailType))
            array_push($filter, new DFC(UsersEntity::FIELD_EMAIL, $emailType, DFC::CONTAINS));
        
        if( (!Utils::IsNullOrEmptyVaraible($addedBeforeType) && Utils::IsNullOrEmptyVaraible($addedAfterType)) 
                || 
            (Utils::IsNullOrEmptyVaraible($addedBeforeType) && !Utils::IsNullOrEmptyVaraible($addedAfterType)) )
            throw new WrongRequestException('Podaj pełny przedział dat!');
        elseif(!$this->CheckIfFromTimeGraterThanToTime($addedBeforeType, $addedAfterType))
                throw new WrongRequestException('Podaj poprawny przedział dat!');
        else
            array_push($filter, new DFC(UsersEntity::FIELD_ADD_DATE, date(PDO_DATE_FORMAT, strtotime($addedAfterType)), DFC::BETWEEN, 
                    date(PDO_DATE_FORMAT, strtotime($addedBeforeType))));
        
        return UsersEntity::findByFilter($this->getPdo(), $filter, true, $rcfilter->getSortOrder(new UsersEntity()), $rcfilter->getPaginator());
    }
    
    private static function CheckIfFromTimeGraterThanToTime($from, $to) {
        return strtotime($from) > strtotime($to);
    }
    
    
    public function update(IntegerType $id, StringType $name, StringType $login, EmailType $email, StringType $role, BooleanType $active, 
            BooleanType $accepted = NULL, StringType $address = NULL, StringType $notes = NULL, StringType $avatar = NULL) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $user = UsersEntity::findById($this->getPDO(), $id->getRequest());
        if(!Utils::IsNullOrEmptyVaraible($user))
        {
            $user->setAvatar($avatar->getRequest());
            $user->setName($name->getRequest());
            $user->setEmail($email->getRequest());
            $user->setLogIn($login->getRequest());
            $user->setRole($role->getRequest());
            $user->setAddress($address->getRequest());
            $user->setActive($active->getRequest());
            $user->setPersonalDataAccepted($accepted->getRequest());
            $user->setNotes($notes->getRequest());
	    $user->updateToDatabase($this->getPDO());
        }
        else
            throw new WrongRequestException('Podany użytkownik nie istnieje!');
    }
    /**
     * Removes user from db
     * @param IntegerType $id of user
     */
    public function delete(IntegerType $id) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        if($id->getRequest() === 1)
            throw new SecurityException('Nie można usunąć głównego administratora aplikacji!');
        UsersEntity::deleteByFilter($this->getPDO(), new DFC(UsersEntity::FIELD_ID, $id->getRequest(), DFC::EXACT));
    }
    /**
     * Pobiera wyspecyfikowanego użytkownika
     * @param StringType $token token autoryzacyjny
     * @param IntegerType $id id użytkownika
     * @return UserFullView użytkownik (bez hasła)
     */
    public function getUser(IntegerType $id) {
	$this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
	return UsersEntity::findById($this->getPDO(), $id->getRequest());
    }
    /**
     * Returns roles for portal
     */
    public function getRoles(RequestCommonFiltersType $rcfilter) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        UsersRolesDictEntity::getAll($this->getPDO(),$rcfilter->getSortOrder(new UsersRolesDictEntity()), $rcfilter->getPaginator());
    }
}

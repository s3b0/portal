<?php
require_once 'config/PortalConfig.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserWs
 *
 * @author sebo
 */
class UsersWs extends WsAbstr {
    /**
     * Constructor
     */
    function __construct() {
	parent::__construct();
    }  
    /**
     * Obsługuje kliknięcie użytkownika w link w maliu z prośbą o zaakceptowanie przetwarzania danych osobowych
     * @param StringType $login login użytkownika jaki został mu nadany podczas jego tworzenia
     * @param StringType $email email uzytkownika
     * @return BooleanType True jeśli wszystko ok
     */
    public function acceptRules(Base64Type $parameter) {
        $email = Crypto::getInstance()->decrypt($parameter->getRequest());
        $filter = array(new DFC(UsersEntity::FIELD_EMAIL, $email, DFC::EXACT), new DFC(UsersEntity::FIELD_PERSONAL_DATA_ACCEPTED, DFC::FALSE, DFC::EXACT));
        $users = UsersEntity::findByFilter($this->getPDO(), $filter, true, NULL, new DPC(0, 1));
	if(!Utils::IsNullOrEmptyVaraible($users))//mamy uzytkownika
	{
            /* @var $user UsersEntity */
            $user = $users[0];
            $user->setPersonalDataAccepted(true);
            $user->updateToDatabase($this->getPDO());
        }
        else    
            throw new SecurityException('Kliknięto nieaktualny link! Zapisuje wydarzenie do dziennika!' );
    }
}

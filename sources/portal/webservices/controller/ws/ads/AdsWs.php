<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Ads class - public
 *
 * @author h3x0r
 */
class AdsWs extends WsAbstr implements DictInter {
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }
    /**
     * Zwraca kategorię ogłoszeń
     *
     * @return AdsTypesDictEntity[] z kategoriami ogłoszeń
     * @access public
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
        return AdsTypesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new AdsTypesDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Pobiera ogłoszenia na podstawie podanych parametrów (tylko zaakceptowane)
     *
     * @param RequestCommonFiltersType standardowy filtr zapytań
     * @param StringType category Kategoria ogłoszenia z formularza
     * @param StringType content Treść tekstu ogłoszenia z formularza
     * @return AdsEntity[] ogłoszenia
     * @access public
     */
    public function getAdsByCategoryOrAndContent(RequestCommonFiltersType $rcfilter, StringType $type = NULL, StringType $content = NULL) {
        $typeType = $type->getRequest();
        $contentType = $content->getRequest();
        
	$filter = array(new DFC(AdsEntity::FIELD_ACCEPTED, true, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($typeType))
            array_push($filter, new DFC(AdsEntity::FIELD_TYPE, $typeType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($contentType))
            array_push($filter, new DFC(AdsEntity::FIELD_AD, $contentType, DFC::CONTAINS));
        return AdsEntity::findByFilter($this->getPdo(), $filter, true, $rcfilter->getSortOrder(new AdsEntity()), $rcfilter->getPaginator());
    }
    /**
     * Dodaje ogłoszenie do systemu
     *
     * @param StringType category Kategoria ogłoszenia
     * @param StringType contents Treść ogłoszenia
     * @param StringType telephoneNumber Numer telefonu
     * @param StringType email EmailType dodającego
     * @param StringType captcha CaptchaType do walidacji
     * @return StringType token ogłoszenia (na jego podstawie klient będzie mógł usunąć ogłoszenie)
     * @access public
     */
    public function create(StringType $type, StringType $content, PhoneNumberType $phone, EmailType $email, CaptchaType $captcha) {
        $aid = Utils::GetSha(); //generuj token 40 znakowy;
	$ad = new AdsEntity();
	$ad->setType($type->getRequest());
	$ad->setAd($content->getRequest());
	$ad->setAdderPhone($phone->getRequest());
	$ad->setAdderEmail($email->getRequest());
	$ad->setAdderIp($_SERVER['REMOTE_ADDR']);
	$ad->setToken($aid);
        $ad->setAccepted(false);

        $ad->insertIntoDatabase($this->getPDO());
            
        return $aid;
    }
    /**
     * Updates selecten (by token) add - works only for unaccepted ads!
     * @param StringType $token
     * @param StringType $category
     * @param StringType $contents
     * @param StringType $telephoneNumber
     * @param StringType $email
     * @param StringType $captcha
     * @return StringType
     * @throws WrongRequestException
     */
    public function update(StringType $aid, StringType $type, StringType $content, PhoneNumberType $phone, EmailType $email, CaptchaType $captcha) {
        $ads = AdsEntity::findByFilter($this->getPDO(), new DFC(AdsEntity::FIELD_TOKEN, $aid->getRequest(), DFC::EXACT), NULL, new DPC(0, 1));
        if(Utils::IsNullOrEmptyVaraible($ads))
            throw new WrongRequestException("Podany token jest niepoprawny!");
        /* @var $ad AdsEntity */
        $ad = $ads[0];               
        if($ad->getAccepted() == TRUE)
            throw new WrongRequestException("Ogłoszenie jest już zaakceptowane - usuń je i dodaj ponownie, pamiętaj, że po tej operacji ogłoszenie będzie
                czekać na akceptację zanim pojawi się znowu.");
        $ad->setType($type->getRequest());
        $ad->setAd($content->getRequest());
        $ad->setAdderPhone($phone->getRequest());
        $ad->setAdderEmail($email->getRequest());
        $ad->updateToDatabase($this->getPDO());    
    }
    /**
     * Usuwa ogłoszenie na podstawie tokena przesłanego od klienta
     *
     * @param StringType token TokenType ogłoeszenia do usunięcia (powienien pochodzić z webstorage przeglądarki klienta lub z cooki klienta)
     * @param StringType captcha CaptchaType do walidacji
     * @return JSON z informacją czy udało się usunąć ogłoszenie (TRUE = tak)
     * @access public
     */
    public function remove(StringType $aid, CaptchaType $captcha) {
        AdsEntity::deleteByFilter($this->getPDO(), new DFC(AdsEntity::FIELD_TOKEN, $aid->getRequest(), DFC::EXACT)); 
    }
}
?>

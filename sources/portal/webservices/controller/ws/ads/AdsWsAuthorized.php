<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Ads class - only for authorized users
 *
 * @author h3x0r
 */
final class AdsWsAuthorized extends AdsWs implements DictAuthInter, DeleteAuthInter {
    use AuthTrait;
    use DictAuthTrait;
    /**
     * Pobiera wszystkie ogłoszenia z ograniczeniami limit i offset
     *
     * @param RequestCommonFiltersType standardowy filtr zapytań
     * @return AdsEntity[] ogłoszenia
     * @access public
     */
    public function getAds(RequestCommonFiltersType $rcfilter, BooleanType $accepted = NULL) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
	return AdsEntity::findByFilter($this->getPDO(), new DFC(AdsEntity::FIELD_ACCEPTED, $accepted->getRequest(), DFC::EXACT), true
                ,$rcfilter->getSortOrder(new AdsEntity()), $rcfilter->getPaginator());
    }
    /**
     * @see Delete::delete($id)
     * @param IntegerType $id add id
     */
    public function delete(IntegerType $id) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        AdsEntity::deleteByFilter($this->getPDO(), new DFC(AdsEntity::FIELD_ID, $id->getRequest(), DFC::EXACT));
    }
    
    public function maintain(IntegerType $id, BooleanType $accepted, StringType $content, StringType $type) {
        $idType = $id->getRequest();
        $acceptedType = $accepted->getRequest();
        $contentType = $content->getRequest();
        $typeType = $type->getRequest();
        
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $add = AdsEntity::findById($this->getPDO(), $idType);
        if(Utils::IsNullOrEmptyVaraible($add))
            throw new WrongRequestException("Podane id ogłoszenia($idType) nie jest poprawne!");
        
        $add->setAcceptDate(date('c'));
        $add->setAd($contentType);
        $add->setAcceptEr($this->getLoggedUserId());
        $add->setAccepted($acceptedType);
        $add->setType($typeType);
        $add->updateToDatabase($this->getPDO());
    }

    public function createType(StringType $name, StringType $description) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->createCommonDictType(new AdsTypesDictEntity(), $name->getRequest(), $description->getRequest());
    }

    public function deleteType(StringType $name) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->deleteCommonDictType(new AdsTypesDictEntity(), $name->getRequest());
    }

    public function updateType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->updateCommonDictType(new AdsTypesDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }

}

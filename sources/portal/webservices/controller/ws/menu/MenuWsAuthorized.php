<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Menu class - only for authorized users
 *
 * @author h3x0r
 */
final class MenuWsAuthorized extends MenuWs {
    use AuthTrait;
    use DictAuthTrait;
    
    private static $UPDATE_ORDER = 'UPDATE '.MenuEntity::SQL_TABLE_NAME.' SET %s WHERE %s';
    /**
     * @see Leaf::delete($id)
     * @param IntegerType $id of menu item
     * @return BooleanType true on success
     */
    public function delete(IntegerType $id) {
        $idType = $id->getRequest();
        $entity = MenuEntity::findById($this->getPDO(), $idType);
        if(!Utils::IsNullOrEmptyVaraible($entity))
        {
            $this->authorize($this->getAllowedGroupRoles($entity->getGroup(), 
                GroupsWsAuthorized::ADMIN), UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
            if($entity->getType() == MenuType::LEAF)
                throw new WrongRequestException('Próbujesz usunąć liść - użyj dedykowanej dla niego metody!');
        
            $childs = MenuEntity::findByFilter($this->getPDO(), new DFC(MenuEntity::FIELD_PARENT, $idType, DFC::EXACT));
            if(!Utils::IsNullOrEmptyVaraible($childs))//if we have childs - we cannot delte item - becouse child can leave changes on filesystem!
                throw new WrongRequestException('Element posiada inne zagnieżdżone wpisy menu - usuń je jako pierwsze!');
        
            $this->getPDO()->beginTransaction();
            try
            {
                $dml = 'UPDATE '.MenuEntity::SQL_TABLE_NAME." SET `order`=`order`-1 WHERE `type`='".$entity->getType().
                    "' AND `order` > ".$entity->getOrder()
                    .(!is_null($entity->getParent()) ? " AND `parent`='".$entity->getParent()."'" : '');
                $entity->deleteFromDatabase($this->getPDO());
                $this->getSQL()->dml($dml);
                $this->getPDO()->commit();
            }
            catch(Exception $e)
            {
                $this->getPDO()->rollBack();
                throw new SqlException('Usunięcie menu nieudane!', $e);
            }
        }
        else
            throw new WrongRequestException("Podany element($idType) nie istnieje!");
    }
    /**
     * Creates new menu item
     * @param StringType $name name of menu item
     * @param StringType $type on menu item @see MenuTypesDictEntity
     * @param StringType $variant of menu (it can be discriminant of particular menu type)
     * @param IntegerType $parent of item
     * @param IntegerType $group of item
     * @param StringType $leaf type of leaf - it contais pointer to table where leaf has its content (eg. article leaf points to LEAFS_ARTICLES)
     * @return BooleanType true on success
     * @throws WrongRequestException in multiple condition (see below code)
     */
    public function create(StringType $name, MenuType $type, StringType $variant = NULL, IntegerType $parent = NULL, IntegerType $group = NULL, 
            StringType $leaf = NULL) {
        
        $nameType = $name->getRequest();
        $menuType = $type->getRequest();
        $variantType = $variant->getRequest();
        $parentType = $parent->getRequest();
        $groupType = $group->getRequest();
        $leafType = $leaf->getRequest();
        
        $this->authorize($this->getAllowedGroupRoles($group->getRequest(), //authorize this group for roles
            GroupsWsAuthorized::ADMIN), UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        //check - if group user tries to add item - if item is dedicated for this group (groups doesn't have access to other menu branches)
        //they have access only to self branches
        if($this->getLoggedUserRole() == UsersWsAuthorized::ADMIN_GROUP)//role based check
        {
            if($menuType == MenuType::ROOTMENU)
                throw new WrongRequestException('Tylko administrator może dodawać '.MenuType::ROOTMENU.'!');
            elseif(Utils::IsNullOrEmptyVaraible(MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_ID, $parentType, DFC::EXACT),
                new DFC(MenuEntity::FIELD_GROUP, $groupType, DFC::EXACT), new DFC(MenuEntity::FIELD_TYPE, MenuType::SUBMENU, DFC::EXACT)), true)))
                    throw new SecurityException("Grupa o id $groupType nie posiada swojej pozycji w menu - poproś administratora portalu o jej utworzenie.");       
        }
        
        if(!Utils::IsNullOrEmptyVaraible($groupType) && (Utils::IsNullOrEmptyVaraible($parentType) || $menuType == MenuType::ROOTMENU))
            throw new WrongRequestException('Wpis w menu dla grupy musi się znajdować w '.MenuType::SUBMENU.'!');

        if($menuType != MenuType::ROOTMENU && Utils::IsNullOrEmptyVaraible($parentType))//jeśli to nie root menu - to musi mieć parenta!
            throw new WrongRequestException("Podany typ menu($menuType) wymaga rodzica!");
        elseif($menuType == MenuType::ROOTMENU && !Utils::IsNullOrEmptyVaraible($parentType))
            throw new WrongRequestException("Podany typ menu($menuType) nie wymaga rodzica!");
        
        //check existance of new menu item - if exist throw exception
        $dfcs = array(new DFC(MenuEntity::FIELD_TYPE, $menuType, DFC::EXACT), 
            new DFC(MenuEntity::FIELD_NAME, $nameType, DFC::EXACT));
        if(Utils::IsNullOrEmptyVaraible($parentType))
            array_push($dfcs, new DFC(MenuEntity::FIELD_PARENT, $parentType, DFC::IS_NULL));
        else
            array_push($dfcs, new DFC(MenuEntity::FIELD_PARENT, $parentType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible(MenuEntity::findByFilter($this->getPDO(), $dfcs)))
            throw new WrongRequestException('Podane menu istnieje!');
        
        $menuItem = new MenuEntity();
        $menuItem->setParent($parentType);          
        $menuItem->setType($menuType);
        $menuItem->setName($nameType);
        $menuItem->setVariant($variantType);
        $menuItem->setGroup($groupType);
        $maxOrderSql = 'SELECT COALESCE(MAX(`order`),0)+1 as `order` from '.MenuEntity::SQL_TABLE_NAME." WHERE `type`='$menuType' "
            .(!is_null($parent) ? "AND `parent`='$parentType'" : '');
        $menuItem->setOrder($this->getSQL()->dql($maxOrderSql)[0]['order']);
        $menuItem->setAdder($this->getLoggedUserId());
        $menuItem->setLeafType($leafType);
        $menuItem->insertIntoDatabase($this->getPDO());
        return (int)$menuItem->getId();//cast to int - getLastInsertId form PDO allways returns string!
    }
    
    /**
     * Updates menu it can handle:
     * - common changes (like name or variant)
     * - order changes
     * - parent changes
     * Conditions for this method:
     *   // MENU same menu cannot be parent for it self
     *   // MENU cannot be changed to leaf
     *   // SUBMENU and LEAF must have parent
     *   // ROOTMENU cannot have parent
     *   // order is very importand (menu items can be draged and droped enywhere - including its above menu!)
     *   // group admin can oparete in his groups menu only!
     * @param IntegerType $id of element
     * @param StringType $name name of element
     * @param IntegerType $order of element
     * @param StringType $variant of element
     * @param IntegerType $parent for element
     * @param IntegerType $group for element
     * @param StringType $leaf type of leaf - it contais pointer to table where leaf has its content (eg. article leaf points to LEAFS_ARTICLES)
     * @return BooleanType true on success
     * @throws WrongRequestException if item dosen't exist in db
     * @throws SqlException on sql error
     */
    public function update(IntegerType $id, StringType $name, IntegerType $order, StringType $variant = NULL, IntegerType $parent = NULL, 
            IntegerType $group = NULL, StringType $leaf = NULL) {
        //FIXME: this method is crap - i need to change it
        
        $idType = $id->getRequest();
        $nameType = $name->getRequest();
        $orderType = $order->getRequest();
        $variantType = $variant->getRequest();
        $parentType = $parent->getRequest();
        $groupType = $group->getRequest();
        $leafType = $leaf->getRequest();
        $menuItem = MenuEntity::findById($this->getPDO(), $idType);
        
        $this->authorize($this->getAllowedGroupRoles((Utils::IsNullOrEmptyVaraible($groupType) ? $menuItem->getGroup() : $groupType), 
            GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);

        //$order = intval($order);
        /*$this->logger->logDebug('--------------------'.($menuItem->getOrder() === $order));
        $this->logger->logDebug('--------------------'.is_int($order));
        $this->logger->logDebug('--------------------'.is_int($menuItem->getOrder()));
        $this->logger->logDebug('--------------------'.(!is_null($menuItem) && 
                (
                    $menuItem->getName() != $name || //jeśli zmieniono nazwę
                    $menuItem->getOrder() !== $order ||  //lub order
                    $menuItem->getParent() != $parent || //lub rodzica (rodzic może być null-em)
                    $menuItem->getGroup() != $group //lub grupę (grupę także można usunąć)
                )
           ));*/
        
        $sqlUpdates = array();
        $newType = NULL;
        if(!is_null($menuItem) && 
                (
                    $menuItem->getName() != $nameType || //jeśli zmieniono nazwę
                    $menuItem->getOrder() != $orderType ||  //lub order
                    $menuItem->getParent() != $parentType || //lub rodzica (rodzic może być null-em)
                    $menuItem->getGroup() != $groupType //lub grupę (grupę także można usunąć)
                )
           )//jeśli coś się zmieniło
        {
            //trzeba określić porządek order w nowym miejscu
            switch($menuItem->getType()) 
            {
                case ((string)MenuType::ROOTMENU):
                    if(Utils::IsNullOrEmptyVaraible($parentType) && $menuItem->getOrder() != $orderType)//jeśli nie zmieniono poziomu ale zmieniono kolejność
                    {
                        $newType = MenuType::ROOTMENU;
                        $siblings = MenuEntity::findByFilter($this->getPDO(), new DFC(MenuEntity::FIELD_TYPE, MenuType::ROOTMENU, DFC::EXACT), true, 
                        new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;    
                        //sprawdź zakres pozycji
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        array_push($sqlUpdates, $this->generateUpdateForSiblingsOnSameLevel($menuItem, $orderType));
                    }
                    elseif(!Utils::IsNullOrEmptyVaraible($parentType))//root przeniesiony do sub lub innego root
                    {
                        $newType = MenuType::SUBMENU;
                        //sprawdź czy rodzic jest OK
                        $this->checkNewParent($parentType, $menuItem->getId());//nie można przeniesiść menu do smago siebie - spradź
                        //pobierz nowe rodzeństwo
                        $siblings = MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_PARENT, $parentType, DFC::EXACT), 
                        new DFC(MenuEntity::FIELD_TYPE, MenuType::SUBMENU, DFC::EXACT)), true, new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;     
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        $sqlUpdates = $this->generateUpdatesForSiblingsOnParentChange($menuItem, $parentType);
                    }
                break;
                case ((string)MenuType::SUBMENU): // tylko sub może byś ROOT-em bądź SUBEM i odwrotnie
                    if(Utils::IsNullOrEmptyVaraible($parentType))//sub przeniesiony na poziom root
                    {
                        $newType = MenuType::ROOTMENU;
                        //pobierz nowe rodzeństwo
                        $siblings = MenuEntity::findByFilter($this->getPDO(), new DFC(MenuEntity::FIELD_TYPE, MenuType::ROOTMENU, DFC::EXACT), true, 
                        new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;    
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        //zmieniamy poziom także zwalniamy jedno miejsce w starmy menu - przesuń o -1 wszystkie wyżej
                        array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`-1', '`type`=\''.MenuType::SUBMENU.'\' AND `parent`=\''.
                            $menuItem->getParent().'\' AND `order` >= \''.$menuItem->getOrder().'\' AND `order` <> 0'));
                        //zrób miejsce w nowym menu root - dodajemy nowy element także wszystkie większę od niego przesuń o 1
                        array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`+1', '`type`=\''.MenuType::ROOTMENU.
                            '\' AND `order` >= \''.$orderType.'\''));
                    }
                    elseif($menuItem->getParent() != $parentType)//jeśli zmieniamy rodzica
                    {
                        $newType = MenuType::SUBMENU;
                        //sprawdź czy rodzic jest OK
                        $this->checkNewParent($parentType, $menuItem->getId());//nie można przeniesiść menu do smago siebie - spradź
                        $siblings = MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_PARENT, $parentType, DFC::EXACT), 
                            new DFC(MenuEntity::FIELD_TYPE, MenuType::SUBMENU, DFC::EXACT)), true, new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));  
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        //zmieniamy poziom także zwalniamy jedno miejsce w starmy menu - przesuń o -1 wszystkie wyżej
                        $sqlUpdates = $this->generateUpdatesForSiblingsOnParentChange($menuItem, $parentType);
                    }
                    elseif($menuItem->getOrder() != $orderType) //nie zmieniamy rodzica ani poziomu ale zmieniamy order
                    {
                        $siblings = MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_PARENT, $menuItem->getParent(), DFC::EXACT),
                        new DFC(MenuEntity::FIELD_TYPE, MenuType::SUBMENU, DFC::EXACT)) , true, new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));  
                        if(!self::ItemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        array_push($sqlUpdates, self::GenerateUpdateForSiblingsOnSameLevel($menuItem, $orderType));
                    }
                    //else
                    //    throw new WrongRequestException('Popraw dane!');
                break;
                case ((string)MenuType::LEAF):
                    if(Utils::IsNullOrEmptyVaraible($leafType))
                        throw new WrongRequestException('Typ liścia musi być podany!');
                    //if(is_null($parent) && $menuItem->getOrder() == $order)
                    //    throw new WrongRequestException('Próbujesz przenieść liść bez podania rodzica!');
                    if(!Utils::IsNullOrEmptyVaraible($parentType) && $menuItem->getParent() != $parentType)//zmieniamy rodzica
                    {
                        //sprawdź czy rodzic jest OK
                        $this->checkNewParent($parentType);
                        $siblings = MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_PARENT, $parentType, DFC::EXACT),
                            new DFC(MenuEntity::FIELD_TYPE, MenuType::LEAF, DFC::EXACT)), true, new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;  
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        $sqlUpdates = $this->generateUpdatesForSiblingsOnParentChange($menuItem, $parentType);
                    }
                    elseif($menuItem->getOrder() != $orderType) //nie zmieniamy rodzica ani poziomu ale zmieniamy order
                    {
                        $siblings = MenuEntity::findByFilter($this->getPDO(), array(new DFC(MenuEntity::FIELD_PARENT, $menuItem->getParent(), DFC::EXACT),
                            new DFC(MenuEntity::FIELD_TYPE, MenuType::LEAF, DFC::EXACT)), true, new DSC(MenuEntity::FIELD_ORDER, DSC::ASC));
                        if(!$this->itemHasSiblings($menuItem, $siblings))//sprawdź czy mamy rodzeństwo jeśli nie to ustaw order na 0 i wyjdź
                            break;  
                        //sprawdź zakres pozycji w nowym miejscu
                        $this->checkSiblingsOrderRange($orderType, $siblings);
                        array_push($sqlUpdates, self::GenerateUpdateForSiblingsOnSameLevel($menuItem, $orderType));
                    }
                    //else
                    //    throw new WrongRequestException('Popraw dane!');
                break;
                default:
                    throw new WrongRequestException('Nieznany typ MENU!');
            }
                        
            $menuItem->setName($nameType);
            $menuItem->setVariant($variantType);
            $menuItem->setGroup($groupType);
            $menuItem->setLeafType($leafType);
            
            //moves in menu tree can change menu type - so if we have this kind of situation react
            if(!Utils::IsNullOrEmptyVaraible($newType))
                $menuItem->setType($newType);

            $this->getPDO()->beginTransaction();
            try
            {
                if($menuItem->getOrder() != $orderType || $menuItem->getParent() != $parentType)// updatety tylko w przypadku zmiany porządku lub rodzica
                {
                    if($menuItem->getOrder() != $orderType)
                        $menuItem->setOrder($orderType);
                    if(!Utils::IsNullOrEmptyVaraible($parentType) && $menuItem->getParent() != $parentType)
                        $menuItem->setParent($parentType);
                    
                    foreach($sqlUpdates as $sqlUpdate)//tutaj podobnie - jakby co to rollback!
                        $this->getSQL()->dml($sqlUpdate);
                }
                $menuItem->updateToDatabase($this->getPDO());//najpierw spróbuj dodać - jeśli DB wykryje jakiś błąd - np constrainty to rollback!
                $this->getPDO()->commit();
            } 
            catch (Exception $e) 
            {
                $this->getPDO()->rollBack();
                throw new SqlException('Edycja menu nie powiodła się!', $e);
            }
        }
        else
            throw new WrongRequestException('Podałeś błędne dane!');
    }
    /**
     * Checks order range for item
     * @param IntegerType $order actual order
     * @param MenuEntity[] $siblings of item
     * @throws WrongRequestException if position is incorrect
     */
    private function checkSiblingsOrderRange($order, $siblings) {
        $firstSibling = $siblings[0]; 
        $min = $firstSibling->getOrder();
        $lastSibling = $siblings[count($siblings)-1];
        $max = $lastSibling->getOrder();
        if($order <  $min || $order > $max)
            throw new WrongRequestException("Podana $order pozycja jest nieprawidłowa(przedział $min do $max)!");
    }
    /**
     * We must check if parent (if will be changed) is correct
     * We cannot move item to it self and to leaf
     * @param IntegerType $parentId id of new parent
     * @param IntegerType $menuItemId id of item that wants to chagne it parent
     * @throws WrongRequestException if parrent is not correct
     */
    private function checkNewParent($parentId, $menuItemId = NULL) {
        $this->getSQL()->dbsm('max_sp_recursion_depth', 255);//procedura rekurencyjna - zdejmij ograniczenia dla sesji
        $parentItem = $this->getSQL()->callDbProcedure("checkParentForMovedRoot($parentId);");
        //$this->logger->logDebug('--------------------------------'.Utils::VarDumptoString($parentItem));
        //$this->dbsm('max_sp_recursion_depth', 0);
        if(is_null($parentItem) || 
                (count($parentItem) > 0 && ($parentItem[0]['type'] === MenuType::LEAF || (!is_null($menuItemId) && $parentItem[0]['id'] == $menuItemId))))
            throw new WrongRequestException('Podany rodzic jest nieprawidłowy!');
    }
    /**
     * Check if item has siblings - if not it sets order to 0
     * @param MenuEntity $menuItem refference to item that will be updated
     * @param MenuEntity[] $siblings of item
     * @return BooleanType true if item has siblings
     */
    private function itemHasSiblings(MenuEntity &$menuItem, $siblings) {
        $this->logger->logDebug('Ilość potomków '.count($siblings));
        if(count($siblings) == 0) // jeśli nie mamy rodzeństwa nie ma co sprawdzać - ustaw 0 i wyjdź
        {
            $this->logger->logDebug('Nowe menu dla '.$menuItem->getName().' nie ma ptomków - ustawiam order na 0');
            $menuItem->setOrder(0);
            return false;
        }
        return true;
    }
    /**
     * When we ant to change order for item we need to change order for item siblings too. If we change order upwards (+ from actual order)
     * all higher items has to change order too, the same thing will be done for lower (-) items.
     * It wokrs without level change
     * @param MenuEntity $menuItem item for update
     * @param IntegerType $order new order
     * @return StringType Sql with order change to invoke
     */
    private function generateUpdateForSiblingsOnSameLevel(MenuEntity $menuItem, $order) {
        $parentSql = '';
        if($menuItem->getType() !== MenuType::ROOTMENU)//jeśli nie root dodaj info o rodzicu
                $parentSql = 'AND `parent`=\''.$menuItem->getParent().'\'';
        if($order > $menuItem->getOrder())//przesuń w lewo
            return sprintf(self::$UPDATE_ORDER, '`order`=`order`-1', '`type`=\''.$menuItem->getType().
            '\' AND (`order` BETWEEN \''.$menuItem->getOrder().'\' AND \''.$order.'\') AND `order` <> 0 '.$parentSql);
        else//przesuń w prawo
            return sprintf(self::$UPDATE_ORDER, '`order`=`order`+1', '`type`=\''.$menuItem->getType().
            '\' AND (`order` BETWEEN \''.$order.'\' AND \''.$menuItem->getOrder().'\') '.$parentSql);    
    }
    /**
     * When we ant to change order for item we need to change order for item siblings too. If we change order upwards (+ from actual order)
     * all higher items has to change order too, the same thing will be done for lower (-) items - thiss will be done in oold parent and in new parent
     * It wokrs when level changes
     * @param MenuEntity $menuItem item for update
     * @param IntegerType $parent new parent
     * @param IntegerType $order new order
     * @return strig[] updates with level changes
     */
    private function generateUpdatesForSiblingsOnParentChange(MenuEntity $menuItem, $parent, $order) {
        $sqlUpdates = array();
        $this->logger->logDebug('Generuje updaty dla menu o typie: '.$menuItem->getType().' i nazwie: '.$menuItem->getName());
        if($menuItem->getType() === MenuType::ROOTMENU)
        {
            //zmieniamy poziom także zwalniamy jedno miejsce w starmy menu - przesuń o -1 wszystkie wyżej
            array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`-1', ' `type`=\''.MenuType::ROOTMENU.'\' AND `order` >= \''. 
                   $menuItem->getOrder().'\' AND `order` <> 0'));
            //zrób miejsce w nowym menu - dodajemy nowy element także wszystkie większę od niego przesuń o 1
            array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`+1', ' `type`=\''.MenuType::SUBMENU.'\' AND `parent`=\''.
                   $parent.'\' AND `order` >= \''.$order.'\''));
            $this->logger->logDebug('Wygenerowano dla '.$menuItem->getType());
        }
        else
        {
            //zmieniamy poziom także zwalniamy jedno miejsce w starmy menu - przesuń o -1 wszystkie wyżej
            array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`-1', '`type`=\''.$menuItem->getType().'\' AND `parent`=\''.
                   $menuItem->getParent().'\' AND `order` >= \''.$menuItem->getOrder().'\' AND `order` <> 0'));
            //zrób miejsce w nowym menu - dodajemy nowy element także wszystkie większę od niego przesuń o 1
            array_push($sqlUpdates, sprintf(self::$UPDATE_ORDER, '`order`=`order`+1', ' `type`=\''.$menuItem->getType().'\' AND `parent`=\''.
                   $parent.'\' AND `order` >= \''.$order.'\''));
            $this->logger->logDebug('Wygenerowano dla '.$menuItem->getType());
        }
        return $sqlUpdates;
    }
    /**
     * Deletes menu item varaint (cascade in db sets menu variant field to null)
     * @param IntegerType $name variant name
     * @return BooleanType true on success
     */
    public function deleteMenuVariant(StringType $name) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->deleteCommonDictType(new MenuVariantsDictEntity(), $name->getRequest());
    }
    /**
     * Creates
     * @param StringType $name
     * @param StringType $description
     */
    public function createMenuVariant(StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->createCommonDictType(new MenuVariantsDictEntity(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Updates variant for menu items
     * @param IntegerType $id variant id
     * @param StringType $name of variant
     * @param StringType $description of variant
     * @return BooleanType true on success
     * @throws WrongRequestException iv variant with given id dosen't exist in db
     */
    public function updateMenuVariant(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->updateCommonDictType(new MenuVariantsDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }

    public function createLeafType(StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->createCommonDictType(new MenuLeafTypeDictEntity(), $name->getRequest(), $description->getRequest());   
    }

    public function deleteLeafType(StringType $name) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->deleteCommonDictType(new MenuLeafTypeDictEntity(), $name->getRequest());
    }

    public function updateLeafType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->updateCommonDictType(new MenuLeafTypeDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());  
    }

}

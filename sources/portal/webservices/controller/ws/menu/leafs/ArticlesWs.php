<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Articles class - public
 * 
 * @author h3x0r
 */
class ArticlesWs extends WsAbstr implements DictInter {
    /**
     * Binary positions of articles
     */
    const IN_SLIDER = 1;
    const HOT = 2;
    const STICKY = 4;
    /**
     * Array of articles positions on page
     */
    const ARTICLES_POSITION = array(
        'SLIDER' => self::IN_SLIDER,
        'HOT' => self::HOT,
        'STICKY' => self::STICKY
    );
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }
    
    public function getArticles(RequestCommonFiltersType $rcfilter) {
        return LeafsArticlesEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new LeafsArticlesEntity()), $rcfilter->getPaginator());
    }
    
    /**
     * Pobiera artykuł po tytule
     *
     * @param StringType title Tytuł do zapytania
     * @return LeafsArticlesEntity[] articles by title
     * @access public
     */
    public function getArticlesByTitle(RequestCommonFiltersType $rcfilter, StringType $title) {
	return $this->getSQL()->dql("SELECT a.*,m.`name` from LEAFS_ARTICLES as a, MENU as m where m.id = a.leaf "
                . "and m.`name` like '%".$title->getRequest()."%' ".$rcfilter->getAsSql(new LeafsArticlesEntity()));
    }
    /**
     * Returns types of articles
     * @return LeafsArticlesTypesDictEntity[] types of articles
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
        return LeafsArticlesTypesDictEntity::getAll($this->getPdo(), $rcfilter->getSortOrder(new LeafsArticlesTypesDictEntity()), 
                $rcfilter->getPaginator());
    }
    /**
     * Finds articles by their type 
     * @param RequestCommonFiltersType $rcfilter
     * @param StringType $position @link ArticlesWs::ARTICLES_POSITION
     * @return type string represetation of article type @link ArticlesWs::ARTICLES_POSITION
     * @throws WrongRequestException
     */
    public function getArticlesByPagePosition(RequestCommonFiltersType $rcfilter, ByteType $position) {
        $positionType = $position->getRequest();
        $array = self::ARTICLES_POSITION;
        if(in_array($positionType, $array))
            return $this->getSQL()->dql("SELECT a.*,m.`name` from LEAFS_ARTICLES as a, MENU as m where m.id = a.leaf "
                . "and a.`position` = '$positionType' ".$rcfilter->getAsSql(new LeafsArticlesEntity()));
        throw new WrongRequestException("Podana pozycja $positionType nie istnieje!");
    }
    /**
     * Gets all allowed positions for articles
     * @return array @see ArticlesWs::ARTICLES_POSITION
     */
    public function getArticlesPagePositions() {
        return self::ARTICLES_POSITION;
    }
}
?>

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Galleries class - only for authorized users
 *
 * @author h3x0r
 */
final class GalleriesWsAuthorized extends GalleriesWs implements LeafAuthInter, DictAuthInter, DeleteAuthInter {
    use AuthTrait;
    use DictAuthTrait;
    /**
     * Creates gallery type
     * @param StringType $name new type name
     * @param StringType $description type description
     * @return BooleanType true on success
     */
    public function createType(StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->createCommonDictType(new LeafsGalleriesTypesDictEntity(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Deletes gallery type
     * @param IntegerType $id id of gallery type
     * @return BooleanType true on success
     */
    public function deleteType(StringType $name) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->deleteCommonDictType(new LeafsGalleriesTypesDictEntity(), $name->getRequest());
    }
    /**
     * Updates gallery type
     * @param IntegerType $id id of gallery type
     * @param StringType $name name for type
     * @param StringType $description type description
     * @return BooleanType true on success
     * @throws WrongRequestException if type dosen't exist in db
     */
    public function updateType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
        $this->updateCommonDictType(new LeafsGalleriesTypesDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }
    /**
     * @see Leaf::delete($leaf)
     * @param IntegerType $id id of leaf from Menu table
     * @throws FileException
     * @throws WrongRequestException
     */
    public function delete(IntegerType $id) {
        $idType = $id->getRequest();
        
        $galleryMenuItem = MenuEntity::findById($this->getPDO(), $idType);
        if(!Utils::IsNullOrEmptyVaraible($galleryMenuItem))
        {
            $this->authorize($this->getAllowedGroupRoles($galleryMenuItem->getGroup(), GroupsWsAuthorized::ADMIN), 
                UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
            
            $galleries = LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $idType, DFC::EXACT));
            if(!Utils::IsNullOrEmptyVaraible($galleries))
            {
                $gallery = $galleries[0];
                $photoDir = $gallery->getPhotosDir();
                $thumbnailsDir = $gallery->getThumbnailsDir();
            
                $this->removeAllPhotosInDir($thumbnailsDir);
                $dirRemoved = rmdir($thumbnailsDir);
                if($dirRemoved)
                {
                    $this->removeAllPhotosInDir($photoDir);
                    $dirRemoved = rmdir($photoDir);
                }
                if(!$dirRemoved)
                    throw new FileException("Nie udało się usunąć: $photoDir lub $thumbnailsDir!");
            }
            $galleryMenuItem->deleteFromDatabase($this->getPDO());
        }
        else
            throw new WrongRequestException("Galeria liściu $idType nie istnieje!");
    }
    private function removeAllPhotosInDir($path) {
        $files = glob($path.DIRECTORY_SEPARATOR.'*'); // get all file names
        foreach($files as $file) // iterate files
            if(is_file($file))
                unlink($file); // delete file
            else
                throw new FileException("Katalog $path zawiera inny katalog!");
    }
    /**
     * Creates new gallery
     * @param IntegerType $id of leaf from Menu table
     * @param StringType $type of gallery @see LeafsGalleriesTypesDictEntity
     * @param StringType $description description for gallery
     * @param IntegerType $group group that gallery belongs to
     * @return BooleanType true on success
     * @throws FileException if filesystem was inaccessible (for example: becouse permission problems)
     * @throws WrongRequestException if gallery dosen't exist
     */
    public function maintain(IntegerType $id, StringType $type, StringType $description = NULL, IntegerType $group = NULL) {
        $idType = $id->getRequest();
        $typeType = $type->getRequest();
        $descriptionType = $description->getRequest();
        $groupType = $group->getRequest();
        
        $this->authorize($this->getAllowedGroupRoles($groupType, GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
              UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
        $gallery = NULL;
        $galleries = LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $idType));
        if(!Utils::IsNullOrEmptyVaraible($galleries))
            $gallery = $galleries[0];
        else 
        {
            $gallery = new LeafsGalleriesEntity();
            $gallery->setLeaf($idType);
            $photoDir = GALLERIES_ROOT_CATALOG.Utils::GetSha(uniqid().$idType);
            $gallery->setPhotosDir($photoDir);
            $thumbnailsDir = $photoDir.DIRECTORY_SEPARATOR.self::THUMBNAILS_DIR;
            $gallery->setThumbnailsDir($thumbnailsDir);
            $gallery->setAdder($this->getLoggedUserId());
        
            if(!mkdir($photoDir, GALLERIES_CATALOG_PERMS) || !mkdir($thumbnailsDir, GALLERIES_CATALOG_PERMS))
                throw new FileException("Nieudało się stworzyć katalogu $photoDir lub $thumbnailsDir!");    
        }
        
        if(Utils::IsNullOrEmptyVaraible($gallery))
            throw new WrongRequestException("Galeria dla liścia $idType nie istnieje!");
        
        $gallery->setType($typeType);
        $gallery->setDescription($descriptionType);
        $gallery->setGroup($groupType);
        $gallery->updateInsertToDatabase($this->getPDO());
    }
    /**
     * Sets image info
     * @param StringType $path path to image
     * @param StringType $comment for image
     * @param StringType $description for image
     * @throws WrongRequestException if image dosen't have proper EXIF/TIFF structure
     */
    private function setImageInfo($path, $comment, $description) {
        $file = file_get_contents($path);
        $data = new \lsolesen\pel\PelDataWindow($file);
        if($file)
        {
            if(\lsolesen\pel\PelJpeg::isValid($data)) 
            {
                $jpeg = new \lsolesen\pel\PelJpeg();
                $jpeg->load($data);
                                
                $exif = $jpeg->getExif();
                $tiff = $exif->getTiff();
                $ifd0 = $tiff->getIfd();
            
                //data zmiany
                $ifd0->getEntry(\lsolesen\pel\PelTag::DATE_TIME)->setValue(time());
                //opis
                $ifd0->getEntry(\lsolesen\pel\PelTag::IMAGE_DESCRIPTION)->setValue($description);
                $subIfd = $ifd0->getSubIfd(\lsolesen\pel\PelIfd::EXIF);
                $subIfd->getEntry(\lsolesen\pel\PelTag::USER_COMMENT)->setValue($comment);
                $jpeg->saveFile($path);
                return;
            }
        }
        throw new WrongRequestException("Dane dla obrazu są niepoprawne - nieudana próba wykrycia struktury EXIF/TIFF!");
    }
    /**
     * Updates image information
     * @param IntegerType $id of leaf from Menu table
     * @param StringType $url of image
     * @param StringType $comment for image
     * @param StringType $description for image
     * @return BooleanType true on success
     */
    public function updateImageInfo(IntegerType $id, UrlType $url, StringType $comment, StringType $description) {
        $idType = $id->getRequest();
        $urlType = $url->getRequest();
        $commentType = $comment->getRequest();
        $descriptionType = $description->getRequest();
        
        $galleries = LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $idType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($galleries))
        {
            $gallery = $galleries[0];
            $this->authorize($this->getAllowedGroupRoles($gallery->getGroup(), GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
              UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
            $path = $this->getImagePath($urlType);
            $this->setImageInfo($path, $commentType, $descriptionType);
            if(strpos($path, self::THUMBNAILS_DIR)) //edytujemy thumbnail nie zapomnij o full photo
                $this->setImageInfo($this->prepareFullPhotoPath($path), $commentType, $descriptionType);
            else //edytujemy full photo nie zapomnij o thumbnail
                $this->setImageInfo($this->prepareThumbnailPath($path), $commentType, $descriptionType);
        }
        else
            throw new WrongRequestException("Galeria liściu $idType nie istnieje!");
    }
    /**
     * Prepares path for gallery with full size images
     * @param StringType $path for images
     * @return StringType correct path to gallery
     */
    private function prepareFullPhotoPath($path) {
        return str_replace('/'.self::THUMBNAILS_DIR, '', $path);
    }
    /**
     * Prepares path for gallery with thumbnails images
     * @param StringType $path for images
     * @return StringType correct path to thumbnails gallery
     */
    private function prepareThumbnailPath($path) {
        return substr_replace($path, DIRECTORY_SEPARATOR.self::THUMBNAILS_DIR, strrpos($path, DIRECTORY_SEPARATOR), 0);
    }
    /**
     * Deletes image from filesystem
     * @param IntegerType $id of leaf from Menu table
     * @param StringType $url of image
     * @return BooleanType true on success
     */
    public function deleteImage(IntegerType $id, UrlType $url) {
        $idType = $id->getRequest();
        $urlType = $url->getRequest();
               
        $galleries = LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $idType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($galleries))
        {
            $gallery = $galleries[0];
            $this->authorize($this->getAllowedGroupRoles($gallery->getGroup(), GroupsWsAuthorized::ADMIN), 
                UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
            $path = $this->getImagePath($urlType);
            unlink($path);
            if(strpos($path, self::THUMBNAILS_DIR)) //thumbnail nie zapomnij o full photo
                unlink($this->prepareFullPhotoPath($path));
            else //full photo nie zapomnij o thumbnail
                unlink($this->prepareThumbnailPath($path));
        }
        else
            throw new WrongRequestException("Galeria liściu $urlType nie istnieje!");
    }
    /**
     * Uploads images - images commes in array[array("filename" => StringType , "image" => base64Image)] structure
     * @param IntegerType $id of leaf from Menu table
     * @param BundleType $images bundle of images
     * @return BooleanType true on success
     * @throws WrongRequestException on multiple failure (see WrongRequestException description below)
     * @throws FileException if image dir is inaccessible
     */
    public function uploadImages(IntegerType $id, BundleType $images) {
        $idType = $id->getRequest();
        $imagesType = $images->getRequest();
        
        $galleries = LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $idType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($galleries))
        {
            $gallery = $galleries[0];
            $this->authorize($this->getAllowedGroupRoles($gallery->getGroup(), GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
                UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
                
            $photosDir = $gallery->getPhotosDir();
            $thumbnailsDir = $gallery->getThumbnailsDir();
            $imagic = new Imagick();
            foreach($imagesType as $image)
            {
                $img = $image['image'];
                $filename = $image['filename'];
                if(Utils::IsNullOrEmptyVaraible($image) || Utils::IsNullOrEmptyVaraible($filename))
                    throw new WrongRequestException("Brakuje nazwy lub danych obrazu!");
            
                $data = base64_decode($img, true);
                if(!$data)
                    throw new WrongRequestException("Dane dla obrazu są niepoprawne!");
                
                $fileFullPath = $photosDir.DIRECTORY_SEPARATOR.$filename;
                $fileThumbnailsPath = $thumbnailsDir.DIRECTORY_SEPARATOR.$filename;

                if(file_exists($fileFullPath))
                    throw new WrongRequestException("Obraz $filename już istnieje w $fileFullPath - usuń go i "
                            . "spróbuje ponownie ( a może wystarczy go wyedytować :) ? )!");              
            
                $imagic->readImageBlob($data);
                $imagic->setimageformat('JPEG');//only jpeg and tiff(but jpeg is more common) has exif abilities!
                $imagic->setfilename($filename);
            
                if(!$imagic->valid())
                    throw new WrongRequestException("Obraz o nazwie $filename jest niepoprawnym plikiem JPEG!");
            
                //$imagic->readimageblob($this->createRequiredExifTiffStructInImage($imagic->getimageblob(), true));//full size image info
                if(!$imagic->writeimage($fileFullPath))
                    throw new FileException("Nieudało się zapisać obrazu $filename do $fileFullPath!");
                $this->createRequiredExifTiffStructInImage($fileFullPath, true);
            
                $imagic->thumbnailimage(300, 0);
                //$imagic->readimageblob($this->createRequiredExifTiffStructInImage($imagic->getimageblob(), true));//one more time - it is new image!
                if(!$imagic->writeimage($fileThumbnailsPath))
                    throw new FileException("Nieudało się zapisać miniaturki obrazu $filename do $fileThumbnailsPath!");
                $this->createRequiredExifTiffStructInImage($fileThumbnailsPath, true);
            
                $imagic->clear();
            }
        }
        else
            throw new WrongRequestException("Galeria liściu $idType nie istnieje!");
    }
    /**
     * Ustawia wymagane struktury w JPEG-u, działa szybciej i pewniej przy działaniach na plikach (próby na bajtach wykazały, że nie zapisują się wszystkie
     * struktury JPEG-a)
     * @param StringType $imagePath
     * @param BooleanType $setSendDate
     * @param StringType $comment
     * @param StringType $description
     * @throws WrongRequestException
     */
    private function createRequiredExifTiffStructInImage($imagePath, $setSendDate = false, $comment = 'Brak komentarzy', $description = 'Brak opisu') {
        //$data = new \lsolesen\pel\PelDataWindow($image);
        $data = new \lsolesen\pel\PelDataWindow(file_get_contents($imagePath));
        /*
        * The static isValid methods in PelJpeg and PelTiff will tell us in
        * an efficient maner which kind of data we are dealing with.
        */
        if(\lsolesen\pel\PelJpeg::isValid($data)) 
        {
            /*
            * The data was recognized as JPEG data, so we create a new empty
            * PelJpeg object which will hold it. When we want to save the
            * image again, we need to know which object to same (using the
            * getBytes method), so we store $jpeg as $file too.
            */
            $jpeg = new \lsolesen\pel\PelJpeg();
            /*
            * We then load the data from the PelDataWindow into our PelJpeg
            * object. No copying of data will be done, the PelJpeg object will
            * simply remember that it is to ask the PelDataWindow for data when
            * required.
            */
            $jpeg->load($data);
            //echo $jpeg->getICC()->getBytes();
            $jpeg->setICC(new \lsolesen\pel\PelJpegComment(PHOTO_WATERMARK));
            /*
            * The PelJpeg object contains a number of sections, one of which
            * might be our Exif data. The getExif() method is a convenient way
            * of getting the right section with a minimum of fuzz.
            */
            $exif = $jpeg->getExif();
            if($exif == null) 
            {    
                /*
                * Ups, there is no APP1 section in the JPEG file. This is where
                * the Exif data should be.
                */
                $this->logger->logDebug('No APP1 section found, added new.');
                /*
                * In this case we simply create a new APP1 section (a PelExif
                * object) and adds it to the PelJpeg object.
                */
                $exif = new \lsolesen\pel\PelExif();
                $jpeg->setExif($exif);
                /* We then create an empty TIFF structure in the APP1 section. */
                $tiff = new \lsolesen\pel\PelTiff();
                $exif->setTiff($tiff);
            } 
            else 
            {
                /*
                * Surprice, surprice: Exif data is really just TIFF data! So we
                * extract the PelTiff object for later use.
                */
                $this->logger->logDebug('Found existing APP1 section.');
                $tiff = $exif->getTiff();
            }
            /*
            * TIFF data has a tree structure much like a file system. There is a
            * root IFD (Image File Directory) which contains a number of entries
            * and maybe a link to the next IFD. The IFDs are chained together
            * like this, but some of them can also contain what is known as
            * sub-IFDs. For our purpose we only need the first IFD, for this is
            * where the image description should be stored.
            */
            $ifd0 = $tiff->getIfd();
            /*
            $model = $ifd0->getEntry(\lsolesen\pel\PelTag::MODEL);
            if(is_null($model))
                $ifd0->addEntry(new \lsolesen\pel\PelEntryAscii(\lsolesen\pel\PelTag::MODEL, 'model'));
            else
                $model->setValue('model4');
            */
            if($ifd0 == null) 
            {
                /*
                * No IFD in the TIFF data? This probably means that the image
                * didn't have any Exif information to start with, and so an empty
                * PelTiff object was inserted by the code above. But this is no
                * problem, we just create and inserts an empty PelIfd object.
                */
                $this->logger->logDebug('No IFD found, adding new.');
                $ifd0 = new \lsolesen\pel\PelIfd(\lsolesen\pel\PelIfd::IFD0);
                $tiff->setIfd($ifd0);
            }
            //set require parameters (change date)
            //$ifd0->addEntry(new \lsolesen\pel\PelEntryAscii(\lsolesen\pel\PelTag::DATE_TIME, '14-09-2015 14:22'));
            $ifd0->addEntry(new \lsolesen\pel\PelEntryTime(\lsolesen\pel\PelTag::DATE_TIME, time()));
            
            $exif_ifd = $ifd0->getSubIfd(\lsolesen\pel\PelIfd::EXIF);
            if($exif_ifd == null)
            {
                $exif_ifd = new \lsolesen\pel\PelIfd(\lsolesen\pel\PelIfd::EXIF);
                $exif_ifd->addEntry(new \lsolesen\pel\PelEntryUserComment($comment));
                $ifd0->addSubIfd($exif_ifd);
            }
            else
            {
                $exif_ifd->getEntry(\lsolesen\pel\PelTag::USER_COMMENT)->setValue($comment);
            }
            if($setSendDate)// we use it as send date (date that indicates time of saving in portal)
            //DATE_TIME_DIGITIZED,DATE_TIME_ORIGINAL pobierane jest zawsze nullem - trzeba to ustawiać jak niżej
                //$exif_ifd->addEntry(new \lsolesen\pel\PelEntryAscii(\lsolesen\pel\PelTag::DATE_TIME_DIGITIZED, '14-09-2015 14:22'));
                $exif_ifd->addEntry(new \lsolesen\pel\PelEntryTime(\lsolesen\pel\PelTag::DATE_TIME_DIGITIZED, time()));
            /*
            * Each entry in an IFD is identified with a tag. This will load the
            * ImageDescription entry if it is present. If the IFD does not
            * contain such an entry, null will be returned.
            */
            $desc = $ifd0->getEntry(\lsolesen\pel\PelTag::IMAGE_DESCRIPTION);
            /* We need to check if the image already had a description stored. */
            if($desc == null) 
            {
                /*
                * In this case we simply create a new PelEntryAscii object to hold
                * the description. The constructor for PelEntryAscii needs to know
                * the tag and contents of the new entry.
                */
                $desc = new \lsolesen\pel\PelEntryAscii(\lsolesen\pel\PelTag::IMAGE_DESCRIPTION, $description);
                /*
                * This will insert the newly created entry with the description
                * into the IFD.
                */
                $ifd0->addEntry($desc);
            }
            else 
            {
                /* The description is simply updated with the new description. */
                $desc->setValue($description);
            }
            //finaly write all to image
            //return $jpeg->getBytes();
            $jpeg->saveFile($imagePath);
        }
        else
            throw new WrongRequestException("Dane dla obrazu są niepoprawne - nieudana próba wykrycia struktury EXIF/TIFF!");
    }
    
}
?>
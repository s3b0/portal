<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Articles class - only for authorized users
 * 
 * @author h3x0r
 */
final class ArticlesWsAuthorized extends ArticlesWs implements LeafAuthInter, DictAuthInter, DeleteAuthInter {
    use AuthTrait;
    use DictAuthTrait;
    /**
     * Creates article type
     * @param StringType $name of type
     */
    public function createType(StringType $name, StringType $description) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->createCommonDictType(new LeafsArticlesTypesDictEntity(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Updates article type
     * @param IntegerType $id of type
     * @param StringType $name of type
     * @throws WrongRequestException
     */
    public function updateType(IntegerType $id, StringType $name, StringType $description) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->updateCommonDictType(new LeafsArticlesTypesDictEntity(), $id->getRequest(), $name->getRequest(), $description->getRequest());
    }
    /**
     * Deletes article type
     * @param IntegerType $id of type
     */
    public function deleteType(StringType $name) {
        $this->authorize(NULL, array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH));
        $this->deleteCommonDictType(new LeafsArticlesTypesDictEntity(), $name->getRequest());
    }
    /**
     * Method that will handle articles creation and update
     * @param IntegerType $id of article leaf (from Menu table)
     * @param StringType $content of article to write
     * @param StringType $banner of article
     * @param StringType $type of article @see LeafsArticlesTypesDictEntity
     * @param IntegerType $group of article @see GroupsEntity
     * @param BooleanType $hot is hot article?
     * @param BooleanType $sticky is sticky article?
     * @param BooleanType $slider is in slider?
     * @throws WrongRequestException
     * @throws FileException
     * @throws GeneralException
     */
    public function maintain(IntegerType $id, HtmlType $content, StringType $banner, StringType $type, ByteType $position = NULL, IntegerType $group = NULL) {
        $idType = $id->getRequest();
        $contentType = $content->getRequest();
        $bannerType = $banner->getRequest();
        $typeType = $type->getRequest();
        $positionType = $position->getRequest();
        $groupType = $group->getRequest();
        
        $this->authorize($this->getAllowedGroupRoles($groupType, GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
              UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
        
        if(!file_exists($bannerType))
            throw new WrongRequestException("Podany baner: $bannerType, nieistnieje!");
        
        $article = NULL;
        $articles = LeafsArticlesEntity::findByFilter($this->getPDO(), new DFC(LeafsArticlesEntity::FIELD_LEAF, $idType, DFC::EXACT));
        if(!Utils::IsNullOrEmptyVaraible($articles))
        {
            $article = $articles[0];
            $filename = $article->getFile();
            $path = $article->getDir().$article->getFile();
        }
        else
        {
            $article = new LeafsArticlesEntity();
            $filename = Utils::GetSha(uniqid().$idType).ARTICLES_FILES_EXTENSION;
            //while(file_exists(ARTICLES_ROOT_CATALOG.$filename))//unique filename!
            //    $filename = sha1_file(uniqid()+$type);
            $path = ARTICLES_ROOT_CATALOG.$filename;
            $article->setFile($filename);
            $article->setDir(ARTICLES_ROOT_CATALOG);
            $article->setLeaf($idType);
            $article->setAdder($this->getLoggedUserId());
        }
        
        if(Utils::IsNullOrEmptyVaraible($article))
           throw new WrongRequestException("Artykuł dla liścia $idType nie istnieje!");
        
        $article->setType($typeType);
        $article->setGroup($groupType);
        $article->setBanner($bannerType);
        $article->setPosition($positionType);
        
        $spl = new SplFileObject($path, 'w');
        if(!$spl->isWritable())
            throw new FileException("Brak uprawnień do zapisu $path!");
        $success = $spl->fwrite($contentType);
        if(Utils::IsNullOrEmptyVaraible($success))
            throw new FileException("Zapis pliku $path nieudany!");
        $article->updateInsertToDatabase($this->getPDO());
    }
    /**
     * @see Leaf::delete($leaf)
     * @param IntegerType $id of article leaf (from Menu table)     
     * @throws FileException
     * @throws WrongRequestException
     */
    public function delete(IntegerType $id) {
        $idType = $id->getRequest();
        
        $atricleMenuItem = MenuEntity::findById($this->getPDO(), $idType);
        if(!Utils::IsNullOrEmptyVaraible($atricleMenuItem))
        {
            $this->authorize($this->getAllowedGroupRoles($atricleMenuItem->getGroup(), GroupsWsAuthorized::ADMIN, GroupsWsAuthorized::EDITOR), 
                UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
            
            $articles = LeafsArticlesEntity::findByFilter($this->getPDO(), new DFC(LeafsArticlesEntity::FIELD_LEAF, $idType, DFC::EXACT));
            if(!Utils::IsNullOrEmptyVaraible($articles))//leaf can be orphan if exeption occured during article addition
            {
                /* @var $article LeafsArticlesEntity */
                $article = $articles[0];
            
                $path = ARTICLES_ROOT_CATALOG.$article->getFile();
                if(!unlink($path))
                    throw new FileException("Brak uprawnień do usunięcia $path!");
            }//in this case we need to remove menu netry 
            //cascade id DB will delete article also
            $atricleMenuItem->deleteFromDatabase($this->getPDO());
        }
        else
            throw new WrongRequestException("Artykuł o podanym liściu($idType) nie istnieje!");
    }
}

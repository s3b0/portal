<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Galleries class - public
 *
 * @author h3x0r
 */
class GalleriesWs extends WsAbstr implements DictInter {
    const THUMBNAILS_DIR = 'thumbnails';
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }
    /**
     * Pobiera wszystkie galerie jakie są na portalu
     * @return LeafsGalleriesEntity[] galerie
     */
    public function getGalleries(RequestCommonFiltersType $rcfilter) {
	return LeafsGalleriesEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new LeafsGalleriesEntity()), $rcfilter->getPaginator());
    }
    /**
     * Returns gallery
     * @param IntegerType id of gallery
     * @return LeafsGalleriesEntity gallery
     */
    public function getGallery(IntegerType $id) {
	return LeafsGalleriesEntity::findByFilter($this->getPDO(), new DFC(LeafsGalleriesEntity::FIELD_LEAF, $id->getRequest(), DFC::EXACT));
    }
    /**
     * Returns galleries
     * @param RequestCommonFiltersType standard request filter
     * @return LeafsGalleriesTypesDictEntity[] galleries
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
        return LeafsGalleriesTypesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new LeafsGalleriesTypesDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Returns image inforation
     * @param StringType $url of image
     * @return array with informations about image 
     * @throws WrongRequestException if EXIF/TIFF structure is incorrect
     */
    public function getImageInfo(UrlType $url) {
        $file = file_get_contents($this->getImagePath($url->getRequest()));
        if($file)
        {
            $data = new \lsolesen\pel\PelDataWindow($file);
            if(\lsolesen\pel\PelJpeg::isValid($data)) 
            {
                $photoInfo = array();
                $jpeg = new \lsolesen\pel\PelJpeg();
                $jpeg->load($data);
                                
                $exif = $jpeg->getExif();
                $tiff = $exif->getTiff();
                $ifd0 = $tiff->getIfd();
            
                //data zmiany
                $photoInfo['changed'] = $ifd0->getEntry(\lsolesen\pel\PelTag::DATE_TIME)->getText();
                //opis
                $photoInfo['description'] = $ifd0->getEntry(\lsolesen\pel\PelTag::IMAGE_DESCRIPTION)->getValue();
                //copyright (left)
                $photoInfo['icc'] = $jpeg->getICC()->getBytes();
                $subIfd = $ifd0->getSubIfd(\lsolesen\pel\PelIfd::EXIF);
                $photoInfo['comments'] = $subIfd->getEntry(\lsolesen\pel\PelTag::USER_COMMENT)->getValue();
                $photoInfo['added'] = $subIfd->getEntry(\lsolesen\pel\PelTag::DATE_TIME_DIGITIZED)->getText();
                return $photoInfo;
            }
        }
        throw new WrongRequestException("Dane dla obrazu są niepoprawne - nieudana próba wykrycia struktury EXIF/TIFF!");
    }
    /**
     * Extracts image path from url - after check and recombination
     * @param StringType $url uri of image
     * @return StringType decodec(url can contais html entities) and focused on local filesystem path
     * @throws WrongRequestException if url was incorrect
     */
    protected function getImagePath($url) {
        $galleryPathToken = str_replace('..'.DIRECTORY_SEPARATOR, '', GALLERIES_ROOT_CATALOG);
        $path = urldecode('../'.substr($url, strpos($url, $galleryPathToken)));
        if(!file_exists($path))
            throw new WrongRequestException("Niepoprawny url $url!");
        return $path;
    }
}
?>

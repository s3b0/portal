<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Menu class - public
 *
 * @author h3x0r
 */
class MenuWs extends WsAbstr implements DictInter {       
    /**
     * Constructor
     */
    public function __construct() {
	parent::__construct();
    }
    /**
     * Gets whole menu
     * @return array with whole menu
     */
    public function getMenu() {
        $menu = array();
        $menuRows = $this->getSQL()->callDbProcedure('getMenu');
        foreach($menuRows as $i => $row)//obróbka rekurencyjnej tabeli
	{
            if($row['TYPE'] == MenuType::ROOTMENU)
            {
                $row[MenuType::SUBMENU] = array();
                $row[MenuType::LEAF] = array();
                $root = $row;
                $menu[] = &$root;
                unset($menuRows[$i]);
                $this->buildLeafs($menuRows, $root);//build leafs for root
                $this->buildSubmenus($menuRows, $root);
                unset($root);
            }
        }
        
	return $menu;
    }
    /**
     * Generates leafs entries for menu
     * @param type $menuRows
     * @param type $parent
     * @return type
     */
    private function buildLeafs(&$menuRows, &$parent) {
        foreach($menuRows as $i => $row)//obróbka rekurencyjnej tabeli
	{
            if(is_null($row['ID']))//loop until reach null separator from db
            {
                unset($menuRows[$i]);
                return;
            }
            if($row['TYPE'] == MenuType::LEAF && $parent['ID'] == $row['PARENT'])
            {
                $leaf = $row;
                $parent[MenuType::LEAF][] = &$leaf;
                unset($menuRows[$i]);
                unset($leaf);
                continue;
            }
        }
    }
    /**
     * Generetes all submenus
     * @param type $menuRows
     * @param type $parent
     * @return type
     */
    private function buildSubmenus(&$menuRows, &$parent) {
        foreach($menuRows as $i => $row)//obróbka rekurencyjnej tabeli
	{
            if(is_null($row['ID']))//loop until reach null separator from db
            {
                unset($menuRows[$i]);
                return;
            }
            if($row['TYPE'] == MenuType::SUBMENU && $parent['ID'] == $row['PARENT'])
            {
                $row[MenuType::SUBMENU] = array();
                $row[MenuType::LEAF] = array();
                $sub = $row;
                $parent[MenuType::SUBMENU][] = &$sub;
                $this->buildLeafs($menuRows, $sub);//build leafs for root
                $this->buildSubmenus($menuRows, $sub);
                unset($sub);
            }
        }
    }
    /**
     * Pobiera typy menu
     *
     * @return JSON typy menu
     * @access public
     */
    public function getTypes(RequestCommonFiltersType $rcfilter) {
	return MenuTypesDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new MenuTypesDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Gets all leaf types
     * @return MenuLeafTypeDictEntity[] leaf types
     */
    public function getLeafTypes(RequestCommonFiltersType $rcfilter) {
        return MenuLeafTypeDictEntity::getAll($this->getPDO(),  $rcfilter->getSortOrder(new MenuLeafTypeDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Gets all variants for items
     * @return LeafsMenuVariantsDictEntity[] menu item variants
     */
    public function getMenuVariants(RequestCommonFiltersType $rcfilter) {
        return MenuVariantsDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new MenuVariantsDictEntity()), $rcfilter->getPaginator());
    }
}
?>

<?php
require_once 'config/PortalConfig.php';
/**
 * Description of GroupsWs
 *
 * @author sebo
 */
class GroupsWs extends WsAbstr {
    /**
     * Constructor
     */
    function __construct() {
	parent::__construct();
    }
    /**
     * Gets groups from portal
     * @param RequestCommonFiltersType $rcfilter common filter 
     * @return GroupsEntity[] groups
     */
    public function getGroups(RequestCommonFiltersType $rcfilter) {
	return GroupsEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new GroupsEntity()), $rcfilter->getPaginator());
    }
}

<?php
require_once 'config/PortalConfig.php';
/**
 * Description of GroupsWs
 *
 * @author sebo
 */
final class GroupsWsAuthorized extends GroupsWs implements DeleteAuthInter {
    use AuthTrait;
    
    const ADMIN = 'AGP';
    const EDITOR = 'EGP';
    /**
     * Ads group to db
     * @param StringType $name of group
     * @param BooleanType $active activates/deactivates group
     */
    public function create(StringType $name, BooleanType $active) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $group = new GroupsEntity();
        $group->setActive($active->getRequest());
        $group->setAdder($this->getLoggedUserId());
        $group->setName($name->getRequest());
        $group->insertIntoDatabase($this->getPDO());
    }
    /**
     * Deletes group in db
     * @param IntegerType $id of group
     */
    public function delete(IntegerType $id) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        GroupsEntity::deleteByFilter($this->getPDO(), new DFC(GroupsEntity::FIELD_ID, $id->getRequest(), DFC::EXACT));
    }
    /**
     * Updates particulary group
     * @param IntegerType $id of group
     * @param StringType $name name for group
     * @param BooleanType $active activates/deactivates group
     * @throws WrongRequestException
     */
    public function update(IntegerType $id, StringType $name, BooleanType $active) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $groups = GroupsEntity::findByFilter($this->getPDO(), new DFC(GroupsEntity::FIELD_ID, $id->getRequest(), DFC::EXACT));
        if(is_array($groups) && count($groups) == 1)
        {
            $group = $groups[0];
            $group->setActive($active->getRequest());
            $group->setName($name->getRequest());
            $group->updateToDatabase($this->getPDO());
        }
        else
            throw new WrongRequestException('Podana grupa nie istnieje!');
    }
    /**
     * Returns roles for groups in portal
     * @param RequestCommonFiltersType $rcfilter comon filters
     */
    public function getRoles(RequestCommonFiltersType $rcfilter) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        UserRoleInGroupDictEntity::getAll($this->getPDO(), $rcfilter->getSortOrder(new UserRoleInGroupDictEntity()), $rcfilter->getPaginator());
    }
    /**
     * Picks users in group
     * @param RequestCommonFiltersType $rcfilter common filters
     * @param StringType $name name of group
     * @return UsersEntity[] users in group
     */
    public function getUsersInGroup(RequestCommonFiltersType $rcfilter, StringType $name) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $query = "SELECT `USERS`.`name`, `USERS`.`login`, `USERS`.`email`, `USERS`.`active`, `USERS`.`avatar`, `USERS`.`role`, `USERS`.`password`,
            `USERS`.`personal_data_accepted`, `USERS`.`add_date`, `USERS`.`adder`, `USERS`.`deletable`, `USERS`.`delated`, `USERS`.`address`, `USERS`.`notes`,
            `GROUP_ROLES_OF_USER`.`role`
            FROM 
                USERS, GROUP_ROLES_OF_USER, GROUPS 
            WHERE 
                GROUPS.`name` = GROUP_ROLES_OF_USER.`group` AND GROUP_ROLES_OF_USER.`user` = USERS.`id` AND GROUPS.`name` = '".$name->getRequest()."' ";
        $query .= $rcfilter->getAsSql(new UsersEntity());
        return $this->getSQL()->dql($query);
    }
    /**
     * Appends user to groups, or remove him from groups
     * @param IntegerType $id of user
     * @param array $groups for user array[id -> group id, role -> group role]
     * @throws WrongRequestException
     */
    public function appendUserToGroups(IntegerType $id, BundleType $groups) {
        $this->authorize(NULL, UsersWsAuthorized::ADMIN_TECH);
        $id = $id->getRequest();
        //make clean in DB
        $user = UsersEntity::findById($this->getPDO(), $id);
        if(Utils::IsNullOrEmptyVaraible($user))
            throw new WrongRequestException('Podany użytkownik nie istnieje!');
        $globalRole = $user->getRole();
        if($globalRole !== UsersWsAuthorized::ADMIN_GROUP)
            throw new WrongRequestException('Podany użytkownik nie potrzebuje bezpośredniego dostępu do grupy! Jego rola to:'.$globalRole);
        
        GroupRolesOfUserEntity::deleteByFilter($this->getPDO(), new DFC(GroupRolesOfUserEntity::FIELD_USER, $id, DFC::EXACT));
        //insert new groups for user
        $userGroupRoles = new GroupRolesOfUserEntity();
        $userGroupRoles->setUser($id);
        foreach($groups->getRequest() as $group)
        {
            $groupId = $group['name'];
            $groupRole = $group['role'];
            $userGroupRoles->setGroup($groupId);
            $userGroupRoles->setRole($groupRole);
            $userGroupRoles->insertIntoDatabase($this->getPDO());
        }
    }
    /**
     * Pobiera grupy dla danego użytkownika
     * @param StringType $token token autoryzacyjny
     * @param IntegerType $id użytkownika
     * @return array grupy użytkownika
     */
    public function getUserGroups(RequestCommonFiltersType $rcfilter, IntegerType $id) {
        $this->authorize(NULL,   UsersWsAuthorized::ADMIN_TECH);
	$query = "SELECT `GROUPS`.`id`, `GROUPS`.`name`, `GROUPS`.`add_date`, `GROUPS`.`active`, `GROUPS`.`deleted`, `GROUPS`.`adder`, `GROUP_ROLES_OF_USER`.`role`
            FROM 
                USERS, GROUP_ROLES_OF_USER, GROUPS 
            WHERE 
                GROUPS.`name` = GROUP_ROLES_OF_USER.`group` AND GROUP_ROLES_OF_USER.`user` = USERS.`id` AND USERS.`id` = '".$id->getRequest()."' ";
        $query .= $rcfilter->getAsSql(new GroupsEntity());
        return $this->getSQL()->dql($query);
    }
}

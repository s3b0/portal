<?php
require_once 'config/PortalConfig.php';
/**
 * class AuthWs
 * Klasa(singleton) uwierzytelniająca i autoryzująca
 */
final class AuthWs extends WsAbstr {
    use AuthTrait;
    /**
     * Nazwa tablicy w sesji jaka będzi zakładana dla zalogowanego użytkownika
     */
    const USER_ARRAY_IN_SESSION_NAME = 'USER_ARRAY';
    /**
     * Nazwa tablicy jaka będzie przechowywać dane zalogowanego użytkownika w sesji
     */
    const USER_OBJECT_IN_SESSION = 'USER_OBJECT';
    const USER_LOGIN_IN_SESSION = 'USER_LOGIN';
    /**
     * We need this for session state checking
     */
    const USER_ID_IN_SESSION = 'USER_ID';
    /**
     * Nazwa tablicy jaka będzie przechowywać dane grup zalogowanego użytkownika w sesji
     */
    const USER_GROUP_OBJECTS_ARRAY_IN_SESSION = 'USER_GROUP_OBJECTS';
    
    private static $JSON_WELCOMPAGE_TOKEN = 'home';
    private static $JSON_TOKEN = 'token';
    /**
     * Metoda uwierzytelnia uzytkownika - kanał komunikacyjny musi być zabezpieczony TLS-em
     *
     * @param StringType login 
     * @param StringType password 
     * @return JSON zawiera token sesji użytkownika(jeśli poprawnie zalogowano) oraz stronę na jaką należy przekierować przeglądarkę
     * @access public
     */
    public function authenticate(StringType $login, StringType $password) {
        $loginType = $login->getRequest();
        $passwordType = $password->getRequest();
        
        //we use session on server side so we need something to define state of session
        $loggedInTagFile = $this->createUserSessiontagFilename($loginType);
        if(file_exists($loggedInTagFile) && (time()-filemtime($loggedInTagFile)) < SESSION_LIFETIME)//if creation time of session tagfile is lower than session 
        //lifitime
            throw new SecurityException("Użytkownik $loginType już zalogowany!"); // we are aleredy loged in
        elseif(file_exists($loggedInTagFile))//else delete tag and allow user to login
            $this->deleteUserSessionFile($loggedInTagFile);
        //rest is normal loging
        $filter = array(new DFC(UsersEntity::FIELD_LOGIN, $loginType, DFC::EXACT), new DFC(UsersEntity::FIELD_PASSWORD, 
                md5($passwordType), DFC::EXACT));
	$users = UsersEntity::findByFilter($this->getPdo(), $filter, true, new DSC(UsersEntity::FIELD_ID), new DPC(0, 1));
	if(!Utils::IsNullOrEmptyVaraible($users))
	{   
            /* @var $user UsersEntity */
            $user = $users[0];
            if(!touch($loggedInTagFile))//create sessiontag file
                throw new FileException("Nie udało się stworzyć tagu sesji dla $loginType!");
            session_start();
	    $token = session_id();
            $role = $user->getRole();
            $userId = $user->getId();
            $sessionArray = array();
            $sessionArray[AuthWs::USER_ID_IN_SESSION] = $userId;
            $sessionArray[AuthWs::USER_LOGIN_IN_SESSION] = $loginType;
            $sessionArray[AuthWs::USER_OBJECT_IN_SESSION] = serialize($user);
            if($role === UsersWsAuthorized::ADMIN_GROUP)
                $sessionArray[AuthWs::USER_GROUP_OBJECTS_ARRAY_IN_SESSION] = serialize
    (
$this->getSQL()->dql('select g.*,grou.role from GROUPS g, USERS u, GROUP_ROLES_OF_USER grou where g.`name` = grou.`group` and u.id = grou.`user` and u.id = '.$userId)
    );
            $_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME] = $sessionArray;
	    $this->logger->logInfo("Loguje $loginType, token: $token, rola: $role");
            return array(self::$JSON_WELCOMPAGE_TOKEN => $user->fetchUsersRolesDictEntity($this->getPDO())->getWelcomePage(), 
                self::$JSON_TOKEN => $token);
        }
        throw new SecurityException('Podaj poprawne dane!');
    }
    /**
     * Kończy sesję - usuwa wszystekie parametry sesyjne
     *
     * @param StringType token 
     * @return void
     * @access public
     */
    public function logout() {
        if(!Utils::IsNullOrEmptyVaraible($this->getSessionToken()) && $this->hasSession())
        {
            $this->deleteUserSessionFile($this->createUserSessiontagFilename($_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME][AuthWs::USER_LOGIN_IN_SESSION]));
            session_unset();
            session_destroy();
            $_SESSION = array();
            //$this->deleteUserSessionFile($this->createUserSessionFilename($this->getSessionToken()));
            $this->logger->logDebug('Wylogowano token: '.$this->getSessionToken());
        }
        else
            throw new SecurityException("Zaloguj się!");
        
    }
    private function createUserSessiontagFilename($login) {
        return TMP_DIR.'/'.md5($login);//tag for sesion state - create file with tag
    }
    /*private function createUserSessionFilename($token) {
        return TMP_DIR.'/sess_'.$token;//tag for sesion state - create file with tag
    }*/
    private function deleteUserSessionFile($sessionFilename) {
        if(!unlink($sessionFilename))
            throw new FileException("Nie udało się usunąć tagu sesji: $sessionFilename!");
    }
}
?>

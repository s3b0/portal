<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Email type
 *
 * @author h3x0r
 */
final class EmailType extends StringType {
    //private static $EMAIL_ADDRESS_PATTER = "/^[a-z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)"
    //        ."+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|pl)$/";
    /**
     * @var string email pattern
     */
    const EMAIL_ADDRESS_PATTERN = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$';
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        parent::validate($val);
	if(preg_match('/'.self::EMAIL_ADDRESS_PATTERN.'/', $val) != 1)
            throw new WrongRequestException("Niepoprawny email: $val!");
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        //return '{ "type": "string", "pattern": "'.self::EMAIL_ADDRESS_PATTERN.'" }';
        return $this->getSchemaStringTypeDefinition('EmailType address type', self::EMAIL_ADDRESS_PATTERN);
    }
}

<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Type of leafs in menu
 *
 * @author h3x0r
 */
final class MenuType extends StringType  {
    const ROOTMENU = 'ROOTMENU';
    const SUBMENU = 'SUBMENU';
    const LEAF = 'LEAF';
    private $TYPES = array(self::ROOTMENU, self::SUBMENU, self::LEAF);
    /**
     * @see StringType::validate($value)
     */
    public function validate($val) {
        parent::validate($val);
	if(!in_array($val, $this->TYPES))
            throw new WrongRequestException("Niepoprawny rodzaj menu($val)!");    
    }
    /**
     * @see StringType::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        $types = '';
        foreach($this->TYPES as $type)
            $types .= $type.',';
        return $this->getSchemaStringTypeDefinition('Menu type enum', rtrim($types, ','));
    }
}

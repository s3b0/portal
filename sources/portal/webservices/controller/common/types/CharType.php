<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Char type
 *
 * @author h3x0r
 */
final class CharType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(strlen($val) != 1 || !ctype_alnum($val))
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('CharType type definition', '^[a-zA-Z]{1}$', '"minLength": 1, "maxLength": 1');
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Captcha type - use it if you want to enable captcha validation during method invokation
 *
 * @author h3x0r
 */
final class CaptchaType extends StringType {
    /**
     *
     * @var string Session name that will hold captcha string (that will be - at the same time - session id)
     */
    const CAPTCHA_SESSION_OBJECT_NAME = 'CaptchaObject';
    /**
     * @see StringType::validate($val)
     */
    public function validate($val) {
        parent::validate($val);
        $this->validateCaptcha($val);
    }
    /**
     * Validates captcha
     * @param string $captcha
     * @throws WrongRequestException
     */
    private function validateCaptcha(string $captcha) {
        session_id($captcha);
        @session_start();
        if(isset($_SESSION[self::CAPTCHA_SESSION_OBJECT_NAME]))
        {
            session_unset();
            session_destroy();
        }
        else
            throw new WrongRequestException('Podana captcha jest niepoprawna!'); 
    }
}

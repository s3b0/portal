<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * BundleType - used for arrays input
 *
 * @author h3x0r
 */
final class BundleType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!is_array($val) || (is_array($val) && count($val) == 0))//proste sprawdzenie - pełna walidacja w metodzie (trzeba użyć pętli - używane w jednej metodzie)
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{ "type": "array" }';
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Request filter - it contains values taht describes order and limits for returning results
 * <b>If you set it as method argument, method will always recive default filter params</b>
 * @see ClassProxy::invoke($requestArray)
 *
 * @author h3x0r
 */
final class RequestCommonFiltersType extends TypeAbstr {
    /**
     * @var string name of main json type for filter
     */
    const FILTER = 'filter';
    /**
     * @var string name of main json type for order
     */
    const ORDER = 'order';
    /**
     * @var string name of filter element with limit for query
     */
    const LIMIT = 'limit';
    /**
     * @var string name of filter element with offset for query
     */
    const OFFSET = 'offset';
    /**
     * @var int default offset
     */
    private $offset = 0;
    /**
     * @var int default limit
     */
    private $limit = DB_RETURN_ROWS_LIMIT;
    /**
     * @var array[string,string] array for order(column name, order for column)
     */
    private $sort = array();    
    /**
     * Returns paginator object
     * @return DPC paginator
     */
    public function getPaginator() {
        return new DPC($this->offset, $this->limit);
    }
    /**
     * Sets offset for sql
     * @param int $offset
     */
    public function setPageOffset($offset) {
        $this->offset = $offset;
    }
    /**
     * Sets limit for sql
     * @param type $limit
     */
    public function setPageLimit($limit) {
        $this->limit = $limit;
    }
    /**
     * Gets DPC for entity (query) 
     * @param int $offset @see RequestCommonFiltersType::OFFSET
     * @param int $limit @see RequestCommonFiltersType::LIMIT
     */
    public function setPage($offset, $limit = DB_RETURN_ROWS_LIMIT) {
        $this->paginator = new DPC($offset, $limit);
    }
    /**
     * Sets sort(order) for query
     * @param type $sortArray
     */
    public function setSort($sortArray) {
        $this->sort = $sortArray;
    }
    /**
     * Returns sort order for entity
     * @param Db2PhpEntity $entityObject entity
     * @return DSC sort for query
     */
    public function &getSortOrder(Db2PhpEntity &$entityObject) {
        $reflection = new ReflectionClass($entityObject);
        $dscArray = array();
        foreach($this->sort as $column => $order)
            array_push($dscArray, new DSC($reflection->getConstant(Db2PhpEntity::ENTITY_FILED_PREFIX.strtoupper($column)), $order));
        return $dscArray;
    }
    /**
     * Gets ordeer and limit part of query as SQL
     * @param Db2PhpEntity $entityObject object for which this fragment will be created
     * @return string sql order and limit fragment
     */
    public function getAsSql(Db2PhpEntity &$entityObject) {
        $query = '';
        /* @var $order DSC */
        foreach ($this->getSortOrder($entityObject) as $order)
            $query .= ' ORDER BY '.$entityObject->getFieldNameByFieldId($order->getField()).' '.$order->getModeSql().' ';
        $query .= $this->getPaginator()->getLimitsForQuery();
        return $query;
    }
    /**
     * @see StringType::validate($value)
     */
    public function validate($val) {
        if(!is_array($val))
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see StringType::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{"type": "object", '
         . '        "properties": { '
                . '     "filter": { "offset": "integer", "limit" : "integer" }, '
                . '     "order": {"description" : "Order in wich output sholud be presented, format: property_name : ASC or DESC", "additionalProperties": true}'
                . '  }, '
                . '"additionalProperties": false}';
    }
}

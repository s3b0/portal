<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Url type - use it to pass and valite urls
 *
 * @author h3x0r
 */
final class UrlType extends TypeAbstr {
    /**
     * @var string url pattern
     */
    const URL_PATTERN = "((https?|ftp)\:\/\/)?". // SCHEME
                        "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?". // User and Pass
                        "([a-z0-9-.]*)\.([a-z]{2,4})". // Host or IP
                        "(\:[0-9]{2,5})?". // Port
                        "(\/([a-z0-9+\$_-]\.?)+)*\/?". // Path
                        "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?". // GET Query
                        "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(preg_match('/'.UrlType::URL_PATTERN.'/', $val) != 1)
            throw new WrongRequestException("Niepoprawny url: $val!");
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition("StringType representation of URL", UrlType::URL_PATTERN);
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Integer type
 *
 * @author h3x0r
 */
final class IntegerType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!is_int($val))
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{ "type": "integer" }';
    }
}

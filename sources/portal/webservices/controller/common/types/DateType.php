<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Date type
 *
 * @author h3x0r
 */
final class DateType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        $date = DateTime::createFromFormat(DATE_FORMAT, $val);
        if(!$date)
            $this->createTypeHintErrorMessege(DATE_FORMAT);
        $this->value = $date->format(DATE_FORMAT);
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('DateType definition, format d-m-Y', '^[0-9]{1,2}\-[0-9]{1,2}\-[0-9]{4}$');
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Phonenumber type
 *
 * @author h3x0r
 */
final class PhoneNumberType extends StringType {
    /**
     * @var string phone pattern
     */
    const PHONE_NUMBER_PATTER = '^(\([0-9]{2}\))?[0-9 ]{1,3}[0-9\-]{1,10}[0-9]{1,3}$';
    /**
     * @see StringType::validate($value)
     */
    public function validate($val) {
        parent::validate($val);
	if(preg_match('/'.self::PHONE_NUMBER_PATTER.'/', $val) != 1)
            throw new WrongRequestException("Niepoprawny numer telefonu: $val!");    
    }
    /**
     * @see StringType::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        //return '{ "type": "string", "pattern": "'.self::PHONE_NUMBER_PATTER.'" }';
        return $this->getSchemaStringTypeDefinition('Phone number type', self::PHONE_NUMBER_PATTER);
    }
}

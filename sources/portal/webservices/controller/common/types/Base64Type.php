<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Base64Type
 *
 * @author h3x0r
 */
final class Base64Type extends TypeAbstr {
    /**
     * BASE64 pattern
     */
    const BASE64_NUMBER_PATTER = '^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$';
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('Base64Type type', self::BASE64_NUMBER_PATTER);
    }
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(preg_match('/'.self::BASE64_NUMBER_PATTER.'/', $val) != 1)
            throw new WrongRequestException("Niepoprawny ciąg base64: $val!");
        $this->value = $val;
    }
}

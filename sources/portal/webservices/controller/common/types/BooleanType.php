<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Boolean type
 *
 * @author h3x0r
 */
final class BooleanType extends TypeAbstr {
    /**
     * Constructor - we need to tell our parent taht we are boolean type
     * @param @see TypeAbstr::__construct()
     */
    public function __construct(bool $value = NULL) {
        //if($value === FALSE)
        //    $value = 0;
        parent::__construct($value);
        $this->isBool = true;
        //        $this->getLogger()->logDebug("ww = $value".'   '.  is_bool($value));
    }
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!is_bool($val))
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{ "type" : "boolean" }';
    }
}

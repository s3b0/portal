<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Session token type
 *
 * @author h3x0r
 */
final class TokenType extends StringType {
    /**
     * @var string token pattern
     */
    const SESSION_PATTERN = '^[a-zA-Z0-9]{26}$';
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        parent::validate($val);
	if(preg_match('/'.self::SESSION_PATTERN.'/', $val) != 1)
            throw new WrongRequestException("Niepoprawny token: $val!");
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        //return '{ "type": "string", "pattern": "'.self::EMAIL_ADDRESS_PATTERN.'" }';
        return $this->getSchemaStringTypeDefinition('Session token type', self::SESSION_PATTERN);
    }
}

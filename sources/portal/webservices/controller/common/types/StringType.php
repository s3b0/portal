<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * String type
 *
 * @author h3x0r
 */
class StringType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        $escapedVal = filter_var($val, FILTER_SANITIZE_STRING);
        if(!$escapedVal || !is_string($val)) 
           $this->createTypeHintErrorMessege($val);
        $this->value = $escapedVal;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('StringType will be stripped!');
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Timestamp type
 *
 * @author h3x0r
 */
final class TimestampType extends TypeAbstr {
    /**
     * @var string timestamp pattern
     */
    const TIMESTAMP_PATTERN = '^[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2} [0-2][0-9]:[0-5][0-9]:[0-5][0-9]$';
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        $date = DateTime::createFromFormat(TIMESTAMP_FORMAT, $val);
        if(!$date)
            $this->createTypeHintErrorMessege(TIMESTAMP_FORMAT);
        $this->value = $date->format(TIMESTAMP_FORMAT);
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('TimestampType type definition', self::TIMESTAMP_PATTERN);
    }
}

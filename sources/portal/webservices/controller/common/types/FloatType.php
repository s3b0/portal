<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Float type
 *
 * @author h3x0r
 */
final class FloatType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!is_float($val))
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{ "type": "number" }';
    }
}

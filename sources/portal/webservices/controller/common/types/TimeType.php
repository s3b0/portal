<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Time type
 *
 * @author h3x0r
 */
final class TimeType extends TypeAbstr {
    /**
     * @var string time pattern
     */
    const TIME_PATTERN = '^[0-2][0-9]:[0-5][0-9]$';
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!preg_match('/'.self::TIME_PATTERN.'/', $val))
            $this->createTypeHintErrorMessege(TIME_FORMAT);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('TimeType type', self::TIME_PATTERN);
    }
}

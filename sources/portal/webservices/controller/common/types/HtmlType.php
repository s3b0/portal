<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Html type - use it if you want to pass as method argument some html
 *
 * @author h3x0r
 */
final class HtmlType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        $tidy = new tidy();
        $tidy->parseString("<!DOCTYPE html><title/><body>$val</body>"); // tag p can be without closing tag - this is good behiviore
        if(!$tidy->body()->isHtml())
            $this->createTypeHintErrorMessege($val);
        $tidy->diagnose();
        if($tidy->getStatus() != 0)
            throw new WrongRequestException("Błędny html: $tidy->errorBuffer");
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */    
    public function getSchemaTypeDefinition() {
        return $this->getSchemaStringTypeDefinition('HtmlType content definition, input muust be valid HTML without html,head and body tags');
    }
}

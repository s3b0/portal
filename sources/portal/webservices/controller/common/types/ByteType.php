<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Byte type
 *
 * @author h3x0r
 */
final class ByteType extends TypeAbstr {
    /**
     * @see TypeAbstr::validate($value)
     */
    public function validate($val) {
        if(!is_Integer($val) || intval($val) < 0 || intval($val) > 127)
            $this->createTypeHintErrorMessege($val);
        $this->value = $val;
    }
    /**
     * @see TypeAbstr::getSchemaTypeDefinition()
     */
    public function getSchemaTypeDefinition() {
        return '{ "type": "number", "minimum": 1, "maximum": 128, "exclusiveMaximum": true, "multipleOf": 2}';
    }
}

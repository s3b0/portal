<?php

/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Abstract class of types used in portal
 *
 * @author h3x0r
 */
abstract class TypeAbstr {
    protected $isBool = false;
    /**
     * @var Logger class logger
     */
    protected $logger;
    /**
     * @var mixed value passed from request - it will be used in WS-es
     */
    protected $value;
    /**
     * Constructor
     * @param mixed $value @see TypeAbstr::$value
     */
    public function __construct($value = NULL) {
	$this->logger = new Logger(get_class($this));   
        if(!Utils::IsNullOrEmptyVaraible($value))
            $this->validate($value);
    }
    /**
     * This method is used in ClassProxy @see ClassProxy::invoke($requestArray)
     * It's simplifies type creation process
     * @return \static object of type
     */
    /*public static function GetInstance() {
        return new static();
    }*/
    /**
     * Validates $value for specific type @see TypeAbstr::$value
     * @param $value @see TypeAbstr::$value
     */
    abstract public function validate($value);
    /**
     * Gets schema definition for type
     */
    abstract public function getSchemaTypeDefinition();
    /**
     * Creates message for exception - used when passed - from request - value dosnt fit to declered type
     * @param mixed $value value that didn't passed validation
     * @throws WrongRequestException
     */
    protected function createTypeHintErrorMessege($value) {
        throw new WrongRequestException('Oczekiwano '.get_called_class()." w parametrze o wartości '$value'!");
    }
    /**
     * Gets request value after specific for this type validation
     * So consider this value as safe to use in application
     * @return mixed value of this type
     */
    public function getRequest() {
        if($this->isBool && Utils::IsNullOrEmptyVaraible($this->value))
            return 0;
        return $this->value;
    }
    /**
     * Gets value from optional value - if value is null than its return default value for this type
     * For boole false, and for other types NULL
     * TODO: i think that this method can be replaced white something more convenient
     * @param TypeAbstr $optionalRequestParameter child that implements secific type
     * @param bool $isBoolean if true that this value is considered as boolean
     * @return mixed value of type
     */
    /*public static function getRequestValueFromOptionalParameter(TypeAbstr $optionalRequestParameter = NULL) {
        if($optionalRequestParameter === NULL)
        {
            if($this->isBool)
            {
                $this->logger->logDebug("dsdsdsdddddddddddddddddddddddd");
                return 0;
            }
            return null;
        }
        return $optionalRequestParameter->getRequest();
    }*/
    /*public function __toString() {
        //cannot be used beacous this method requires to return StringType only!
        $this->logger->logWarning("Wywołano _toString dla wartości $this->value.");
        return (string)$this->value;
    }*/
    /**
     * Get schema decalration for StringType @link StringType
     * @param string $description of element
     * @param string $pattern for element
     * @param string $additions other importand information about this element
     * @return string declaration of type for schema
     */
    protected function getSchemaStringTypeDefinition(string $description, string $pattern = NULL, string $additions = NULL) {
        $stringTypeDefinition = '{ "description" : "'.$description.'" ,"type": "string" ';
        if(!Utils::IsNullOrEmptyVaraible($pattern))
            $stringTypeDefinition .= ', "pattern": "'.$pattern.'", ';
        if(!Utils::IsNullOrEmptyVaraible($additions))
            $stringTypeDefinition .= $additions;
        $stringTypeDefinition = trim($stringTypeDefinition, ',');
        return $stringTypeDefinition.' }';
    }
    /**
     * Gets logger
     * @return Logger
     */
    protected function getLogger() {
        return $this->logger;
    }
}

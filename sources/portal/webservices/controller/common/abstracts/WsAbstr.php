<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Abstract class of portal webservices(all)
 * 
 * @author h3x0r
 */
abstract class WsAbstr {
   /**
    * @var Logger standard logger
    */
    protected $logger;
    /**
     * Construct
     */
    protected function __construct() {
	$this->logger = new Logger(get_class($this));     
    }
    /**
     * Interface to native SQL connection (use it for standard dml,dql queries)
     * @return SqlDAO SQL Native Interace
     */
    protected function getSQL() {
        return SqlDAO::getInstance();
    }
    /**
     * Interface to PDO (connection) object
     * @return PDO pdo
     */
    protected function getPDO() {
        return $this->getSQL()->getPdo();
    }
    /**
     * Gets DAO instance for file db access
     * @return CfDAO file dao
     */
    protected function getCF() {
        return CfDAO::getInstance();
    }
    /**
     * Gets application version
     * @return string app version
     */
    public function getVersion() {
	return APP_VERSION;
    }
    /**
     * Gets captcha
     * @return string captcha
     */
    public function getCapcha() {
	$chars = CAPTCHA_SIGNS;
	$randomString = '';
	for($i = 0; $i < CAPTCHA_LENGTH; $i++)
	    $randomString .= $chars[rand(0, strlen($chars) - 1)];
	session_id($randomString); //ustaw captchę jako id sesji
	@session_start(); //i odpal sesję
	$_SESSION[CaptchaType::CAPTCHA_SESSION_OBJECT_NAME] = $randomString;
        $this->logger->logInfo($randomString);
	return $randomString;
    }
    /**
     * Gets class name (for child class name of child)
     * @return string class name
     */
    public static function GetClassName() {
        return get_called_class();
    }
    /**
     * Gets logger
     * @return Logger
     */
    protected function getLogger() {
        return $this->logger;
    }
}
?>

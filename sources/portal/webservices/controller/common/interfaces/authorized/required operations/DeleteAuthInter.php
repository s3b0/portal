<?php
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Interface for webservices deletes operation that is required
 * (common for webservices)
 * <p>If you find method that is common for Webservices put it here</p>
 * <b>This is used in webservices after authentication/authorization</b>
 * @author h3x0r
 */
interface DeleteAuthInter {
    /**
     * Deletes leaf object from DB (its representation in domain table and in menu table)
     * @param IntegerType $id of row (this is from Menu table!)
     */
    public function delete(IntegerType $id);
}

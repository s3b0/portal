<?php
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Interface for domain dictionary function (for example: Events has types dict that belogns to Event domain)
 * (common for webservices)
 * <b>This is used in webservices after authentication/authorization</b>
 * @author h3x0r
 */
interface DictAuthInter {
    /**
     * Creates type for domain
     * @param StringType $name of type
     * @param StringType $description of type
     */
    public function createType(StringType $name, StringType $description);
    /**
     * Deletess type for domain
     * @param StringType $name of type
     */
    public function deleteType(StringType $name);
    /**
     * Updates type for domain
     * @param IntegerType $id of type
     * @param StringType $name of type
     * @param StringType $description of type
     */
    public function updateType(IntegerType $id, StringType $name, StringType $description);
}

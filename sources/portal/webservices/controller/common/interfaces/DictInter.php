<?php
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Publicly available methods for dictionary operations 
 * (common for webservices)
 * @author h3x0r
 */
interface DictInter {
    /**
     * Gets types for domain (for example types of events for Event domain)
     * @param RequestCommonFiltersType $rcfilter common filter for get methods @see RequestCommonFiltersType::__construct()
     */
    public function getTypes(RequestCommonFiltersType $rcfilter);
}

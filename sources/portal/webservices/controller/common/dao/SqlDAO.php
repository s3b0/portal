<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * DAO for config params stored in db
 * Singleton
 * @author h3x0r
 */
final class SqlDAO extends SingletonAbstr {
    /**
     * @var SqlDAO instance of this class
     */
    protected static $INSTANCE;
    /**
     * @var PDO connection
     */
    private $pdo;
    /**
     * Construct
     */
    protected function __construct() {
        parent::__construct();
        try
	{
            $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_SCHEMA . ';charset=utf8';
            $this->pdo = new PDO($dsn, DB_USER, DB_PASSWORD);
            //self::$PDO->setAttribute(PDO::ATTR_CASE, PDO::CASE_UPPER);//nazwy kolumn jak w bazie!!! nie ustawiać tego!!!
            //$this->pdo->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_PERSISTENT, true);
            //self::$PDO->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            //echo $this->pdo->getAttribute(PDO::ATTR_PREFETCH);
	}
	catch(PDOException $e)
	{
            $this->getLogger()->logError('Wyjatek: ' . $e->getMessage() . ' w pliku ' . $e->getFile() . ' w wierszu ' . $e->getLine(), $e);
            throw new SqlException('Błąd połaczenia z bazą danych!');
	} 
    }
    /**
     * Calls stored prcedure(routine) and returns its rows
     * @param string $name procedure name
     * @return mixed rows returned by procedure
     * @throws SqlException
     */
    public function callDbProcedure(string $name) {
	try
	{
	    $query = "call $name";
            $this->getLogger()->logDebug("Wywołuję procedurę: $query");
	    $this->getPDO()->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
	    $rows = $this->getPDO()->query($query, PDO::FETCH_ASSOC)->fetchAll();
	    $this->getPDO()->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	    return $rows;
	}
	catch(PDOException $e)
	{
            throw new SqlException("Call to procedure $procedureName failed!", $e);
	}
    }
    
    /**
     * Infokes DML query and returns rows returned (INSERT, DELETE, UPDATE)
     * @param string $query
     * @return int infected rows count(only Update & Delete)
     */
    public function dml(string $query) {
	try
	{
            $this->getLogger()->logDebug("DML: $query");
	    return $this->getPDO()->exec($query);
	}
	catch(Exception $e)
	{
            throw new SqlException("Wywołanie dml: $query nieudane!", $e);
	}
    }
    /**
     * Use it for seting specified session settings
     * @param string $parameter
     * @param string $value
     * @return ??
     * @throws SqlException
     */
    public function dbsm(string $parameter, string $value) {
        $dbms = "SET @@session.$parameter=$value;";
        try
	{
            $this->getLogger()->logDebug("Session modification: $dbms");
	    return $this->getPDO()->exec($dbms);
	}
	catch(Exception $e)
	{
            throw new SqlException("Wywołanie dbms: $dbms nieudane!", $e);
	}
    }
    /**
     * Invokes select and return values for query as assoc array
     * @param string $query 
     * @return array[mixed] array or forws from db
     */
    public function dql(string $query) {
	try
	{
	    $this->getLogger()->logDebug("Wywołuje: $query");
	    return $this->getPDO()->query($query, PDO::FETCH_ASSOC)->fetchAll();
	}
	catch(PDOException $e)
	{
            throw new SqlException("Wywołanie dql: $query nieudane!", $e);
	}
    }
    /**
     * Gets PDO object
     * @return PDO pdo
     */
    public function getPdo() {
        return $this->pdo;
    }
    /**
     * Gets function for string-to-date conversion (for MariaDB)
     * @param string $date date string
     * @return string prepered SQL function with date in specified format
     */
    public function createSqlDateString(string $date) {
        return "STR_TO_DATE('$date','%d-%m-%Y')";
    }
}

?>

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * DAO for config file(s) - files are ini styled
 * Singleton
 * @author h3x0r
 */
final class CfDAO extends SingletonAbstr {
    /**
     * @var CfDAO instance of this class
     */
    protected static $INSTANCE;
    /**
     * @var ConfigLite instance of interface for ini files (look into lib dir)
     */
    private $configLite;
    /**
     * @var string path to dir with config files
     */
    private static $CONFIG_FILE_PATH = '/webservices/config/';
    /**
     * @var string name of out ini db (config file)
     */
    private static $CONFIG_FILE_NAME = 'db.ini'; 
    /**
     * Constructor
     * @throws CfException
     */
    protected function __construct() {
        parent::__construct();
        $configFileName = '..'.self::$CONFIG_FILE_PATH.self::$CONFIG_FILE_NAME;
	if(file_exists($configFileName))
	    $this->configLite = new ConfigLite($configFileName);
	else
	    throw new CfException(sprintf('Podany plik %s nie istnieje!', $configFileName));
    }    
    /**
     * Gets config param value by section and key
     * @param string $section of param value
     * @param string $key of param value
     * @return mixed depends what has been written in config file
     * @throws CfException
     */
    public function getConfigParams(string $section, string $key) {
	if($this->configLite->has($section, $key))
            return $this->configLite->getString($section, $key);
	throw new CfException(sprintf('Klucz %s w sekcji %s nie istnieje!', $key, $section));
    }
    /**
     * Writes value to config file under section and key
     * @param string $section param section
     * @param string $key param key
     * @param mixed $value int or string value of param
     * @throws CfException
     */
    public function setConfigParams(string $section, string $key, $value) {
	try
	{
	    $this->configLite->set($section, $key, $value);
	    $this->configLite->save();
	}
	catch(Config_Lite_Exception_InvalidArgument $e)
	{
	    throw new CfException("Nie udało się zapisać parametrów: sekcja: $section, klucz: $key, wartość: $value.", $e);
	}
    }
    /**
     * Clears param value
     * @param string $section section of param
     * @param string $key key of param
     * @throws CfException
     */
    public function clearsetConfigParams(string $section, string $key) {
	if(!$this->setConfigParams($section, $key, ''))
	    throw new CfException("Nie udało się wyczyśćić parametrów: sekcja: $section, klucz: $key.");
    }
    /**
     * @see ConfigLite::getSectionCount()
     * @return int
     */
    public function getSectionCount() {
	return $this->configLite->getSectionCount();
    }
    /**
     * Gets all keys for section
     * @param string $section name
     * @return array[string] array of key names
     */
    public function getSectionKeys(string $section) {
	return $this->configLite->getSection($section);
    }
}
?>

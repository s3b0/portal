<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Trait that is require for authorization
 *
 * @author h3x0r
 */
trait AuthTrait {
    /**
     * @var string token from user - it is his session id, this token is passed to user after login
     */
    private $token;
    /**
     * @var string global (<b>not group!</b>) role of user  
     */
    private $role;
    /**
     * Constructor
     * @param TokenType $token token(sesson id) from user, this token is passed to user after login
     */
    public function __construct(TokenType $token) {
        parent::__construct();
        $this->token = $token->getRequest();
    }
    /**
     * Autorization method
     * @param array[string=>array[string]] $groupWithRoles roles for user within group that his belongs to
     * @param array[string] $roles array of roles
     */
    protected function authorize($groupWithRoles = NULL, ...$roles) {
	if($this->hasSession())
	{                     
            /* @var $user UsersEntity */
	    $user = unserialize($_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME][AuthWS::USER_OBJECT_IN_SESSION]);
	    if(!Utils::IsNullOrEmptyVaraible($user))//token poprawny i mamy usera
	    {
                $this->role = $user->getRole();
                if($this->role  === UsersWsAuthorized::ADMIN_PORT)//admin can everything
                    return;
		elseif(in_array($this->role, $roles))//allowed roles
                {
                    if($this->role === UsersWsAuthorized::ADMIN_EDIT || $this->role === UsersWsAuthorized::ADMIN_TECH)//those roles are allowed
                        return;//allow :)
                    elseif($this->role === UsersWsAuthorized::ADMIN_GROUP && !Utils::IsNullOrEmptyVaraible($groupWithRoles))//if we have group user
                    {
                        $groups = unserialize($_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME][AuthWS::USER_GROUP_OBJECTS_ARRAY_IN_SESSION]);
                        $this->logger->logDebug(Utils::VarDumptoString($groupWithRoles));
                        if(!Utils::IsNullOrEmptyVaraible($groups))
                        {
                            /* @var $group array('id', 'name', 'role') */
                            foreach($groups as $group)
                            {
                                $authorizeForGroupId = $group['id'];
                                //check if we user has passed group in his groups and his role is adequate to authorized roles
                                if(array_key_exists($authorizeForGroupId, $groupWithRoles) && in_array($group['role'], $groupWithRoles[$authorizeForGroupId]))
                                   return;
                            }
                            $this->logger->logError('Użytkownik o id '.$user->getId()." w roli $this->role nie jest autoryzowany do wykonania operacji!");
                        }
                        else
                            $this->logger->logError('Użytkownik o id '.$user->getId()." w roli $this->role nie posiada grup!");
                    }
                    else
                        throw new BadFunctionCallException('Grupa '.UsersWsAuthorized::ADMIN_GROUP.' wymaga podania grupy z conajmniej jedną uprawnioną rolą!');
                }
	    }
            throw new SecurityException('Nie jesteś uprawniony do wykonania operacji!');
	}
	throw new SecurityException("Zaloguj się!");
    }
    /**
     * Gets logged user id (like in db - soterd in session after succesful login)
     * @return int logged user id
     * @throws SecurityException
     */
    protected function getLoggedUserId() {
        if(session_status() == PHP_SESSION_ACTIVE)
            return $_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME][AuthWs::USER_ID_IN_SESSION];
        throw new SecurityException('Należy rozpocząć sesję aby pobrać id użytkownika!');
    }
    /**
     * Gets user login (like in db - soterd in session after succesful login)
     * @return string logged user login
     * @throws SecurityException
     */
    protected function getLoggedUserLogin() {
        if(session_status() == PHP_SESSION_ACTIVE)
            return $_SESSION[AuthWs::USER_ARRAY_IN_SESSION_NAME][AuthWs::USER_LOGIN_IN_SESSION];
        throw new SecurityException('Należy rozpocząć sesję aby pobrać id użytkownika!');
    }
    /**
     * Check is user has correct session
     * @return boolean true if user has correct session fale otherwise
     */
    protected function hasSession() {
        session_id($this->getSessionToken());        
        session_start();
	if(session_status() == PHP_SESSION_ACTIVE)
        {
            $sessionId = session_id();//sprawdź czy odtworzono sesję
            if($this->getSessionToken() === $sessionId && count($_SESSION) > 0)//jeśli odtworzona (znaleziono istniejącą) i mamy obiekty sesyjne
            {
		$this->logger->logDebug('Przywracam sesję: TOKEN='.$this->getSessionToken().", SESSION_ID=$sessionId, dla użytownika ".$this->getLoggedUserLogin().
                        '(id '.$this->getLoggedUserId().')');
                return true;
            }
        }
        return false;
    }
    /**
     * Gets logged user session token @see AuthTrait::$token
     * @return type
     */
    protected function getSessionToken() {
        return $this->token;
    }
    /**
     * Helper methos - gets full (without portal admin) array of roles
     * @return array[string] roles
     */
    protected function getFullRolesArray() {
        return array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH, UsersWsAuthorized::ADMIN_GROUP);
    }
    /**
     * Gets only admin roles
     * @return array[string] admin roles
     */
    protected function getAdminRolesArray() {
        return array(UsersWsAuthorized::ADMIN_EDIT, UsersWsAuthorized::ADMIN_TECH);
    }
    /**
     * Creates holder with group and roles for group
     * @param integer $group holds id of group against we must be authoized
     * @param array[string] $groupRoles set of roles for group
     * @return array[groupId => array[authorizedGroups]] array with group id as key and set of roles that are authorized
     */
    protected function getAllowedGroupRoles(string $group = NULL, ...$groupRoles) {
        if(!Utils::IsNullOrEmptyVaraible($group))
            return array($group => $groupRoles);
        return array();
    }
    /**
     * Gets logged user global role
     * @return string global role of logged user
     */
    protected function getLoggedUserRole() {
        return $this->role;
    }
}

<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Helpers for dicttionary methods @link DictInter
 * @author h3x0r
 */
trait DictAuthTrait {
    /**
     * Creates(stores in db) new type for domain 
     * It uses reflection to be generic
     * @param Db2PhpEntityBase $entityObject 
     * @param string  $name of type
     * @param string  $description of type
     */ 
    protected function createCommonDictType(Db2PhpEntityBase $entityObject, string $name, string $description) {
        $reflection = new ReflectionClass($entityObject);
        $reflection->getMethod('setName')->invoke($entityObject, $name);
        $reflection->getMethod('setDescription')->invoke($entityObject, $description);
        $reflection->getMethod('insertIntoDatabase')->invoke($entityObject, $this->getPDO());
    }
    /**
     * Deletes type for domain 
     * It uses reflection to be generic
     * @param Db2PhpEntityBase $entityObject
     * @param string  $name of type
     */
    protected function deleteCommonDictType(Db2PhpEntityBase $entityObject, string $name) {
        $reflection = new ReflectionClass($entityObject);
        $reflection->getMethod('deleteByFilter')->invokeArgs($entityObject, array($this->getPDO(), 
            new DFC($reflection->getConstant('FIELD_NAME'), $name, DFC::EXACT)));
    }
    /**
     * Updates type for domain 
     * It uses reflection to be generic
     * @param Db2PhpEntityBase $entityObject
     * @param int $id of type
     * @param string  $name of type
     * @param string  $description of type
     * @throws WrongRequestException
     */
    protected function updateCommonDictType(Db2PhpEntityBase $entityObject, int $id, string $name, string $description) {
        $reflection = new ReflectionClass($entityObject);
        $entities = $reflection->getMethod('findByFilter')->invokeArgs($entityObject, array($this->getPDO(), 
            new DFC($reflection->getConstant('FIELD_ID'), $id, DFC::EXACT)));
        
        if(Utils::IsNullOrEmptyVaraible($entities))
            throw new WrongRequestException('Podałeś błędne dane!');
        
        $entity = $entities[0];
        $entity->setName($name);
        $entity->setDescription($description);
        $entity->updateToDatabase($this->getPDO());                    
    }
}

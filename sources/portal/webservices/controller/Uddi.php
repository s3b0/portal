<?php
require_once 'config/PortalConfig.php';
/*
 * Made by s3b0(s3b0@mail.com) - look at s3b0.pl
 * Licenced under GPLv3
 */

/**
 * Uddi is responsible for schema creation
 *
 * @author h3x0r
 */
class Uddi extends WsAbstr {
    private $schema;
    private $schemaUrl;
    private static $TYPES_DIR_NAME = 'types';

    public function __construct() {
	parent::__construct();
        global $GLOBALS;
        $this->schemaUrl = $GLOBALS[URI].'?area=Uddi&operation=getSchema';
        $this->schema = '{ "$schema": "http://json-schema.org/schema#", "id": "'.$this->schemaUrl.'", '
            . '"definitions": {%s} }';
    }
    /**
     * Creates schema for portal webservices (it using types as input for schema creation)
     * @return string schema
     */
    public function getSchema() {
        $directory = new DirectoryIterator(dirname(__FILE__).'/common/'.self::$TYPES_DIR_NAME);
        $schema = '';
        while(!Utils::IsNullOrEmptyVaraible(($filename = $directory->current()->getFilename())))
        {
            $class = basename($filename, ".php");
            if($class !== '.' && $class !== '..')
            {
                $rc = new ReflectionClass($class);
                if(!$rc->isAbstract())
                {
                    $object = new $class();
                    $schema .= $class.' : '.$object->getSchemaTypeDefinition().',';
                }
            }
            $directory->next();
        }
        return sprintf($this->schema, trim($schema, ','));
    }
    /**
     * Creates listing of webservices aviaiable in portal
     * @return string[] list of services 
     */
    public function getServices() {
        $directory = new RecursiveDirectoryIterator(dirname(__FILE__).'/ws', FilesystemIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($directory);
        $services = array('services' => array());
        foreach($iterator as $filename => $fileCursor)
        {
            if(!$fileCursor->isDir())
            {
                $class = basename($filename, ".php");
                $rc = new ReflectionClass($class);
                if(!$rc->isAbstract())
                    array_push($services['services'], array('service' => $class));
            }
        }
        //$schema = trim($schema, ',');
        return $services;
    }
    /**
     * Creats full schema for one webservice
     * @param StringType $service portal webservice name @see Uddi::getServices()
     * @return array full schema for webservice
     */
    public function getOperations(StringType $service) {
        $rc = new ReflectionClass($service->getRequest());
        $operations = array('$schema' => $this->schemaUrl);
        $operations[$service->getRequest()] = array();
        //All methods for authorized wses need to have token as input
        $hasAuthTrait = array_key_exists('AuthTrait', $rc->getTraits());
        
        /* @var $method ReflectionMethod */
        foreach($rc->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            $name = $method->getName();
            //echo $name.'<br/>';
            if(!$method->isConstructor() && !$method->isStatic() && strpos($name, '__') !== 0)
            {
                $properties = array();
                $required = array();
                if($hasAuthTrait)
                {
                    $properties['token'] = array('$ref' => "#/definitions/token");
                    array_push($required, 'token');
                }
                /* @var $parameter ReflectionParameter */
                foreach($method->getParameters() as $parameter)
                {
                    if(!Utils::IsNullOrEmptyVaraible($parameter->getClass()))
                    {
                        $parameterType = strtolower($parameter->getClass()->getName());
                        $parameterName = $parameter->getName();
                        $properties[$parameter->getName()]=  array('$ref' => "#/definitions/$parameterType");
                        if(!$parameter->isOptional() && $parameterType !== strtolower('RequestCommonFiltersType'))//RequestCommonFiltersType is optional - always!
                            array_push($required, $parameterName);
                    }
                }
                array_push($operations[$service->getRequest()], array('operation' => $name, 'type' => 'object', 'properties' => $properties, 'required' => $required));
            }
        }
        return $operations;
    }
}

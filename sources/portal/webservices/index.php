<?php
require_once 'config/PortalConfig.php';
$area = NULL;
$response = NULL;
$operation = NULL;
/**
 *
 * Główny kontroler aplikacji - router webserwisów
 *
 */
try
{
    if(isset($_GET[AREA_PARAMETER_NAME]))
    {
        /**
        * @var string Określa zasięg żądania (np. pytamy o obszar menu)
        */
        $area = filter_var($_GET[AREA_PARAMETER_NAME], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }
    if(isset($_GET[OPERATION_PARAMETER_NAME]))
    {
        /**
        * @var string Określa dokładną akcję dla zasięgu (np. pobierz menu)
        */
        $operation = filter_var($_GET[OPERATION_PARAMETER_NAME], FILTER_SANITIZE_FULL_SPECIAL_CHARS);   
    }
    /**
    * @var string Szczegóły zapytania w postaci JSON (payload)
    */
    $raw = '';
    if(isset($_GET[OPTIONAL_PARAMETER_NAME]))
    {
        /**
        * @var Base64Type encoded with @see Crypto::encrypt($input) string
        */
        $parameter = filter_var($_GET[OPTIONAL_PARAMETER_NAME], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $raw = json_encode(array(OPTIONAL_PARAMETER_NAME => $parameter));
    }
    else
        $raw = file_get_contents('php://input');
    
    unset($_REQUEST);
    unset($_POST);
    unset($_GET);
    
    //required varaibles
    /**
     * @var mixed error place holder - will contains error message to output
     */
    $error = NULL;
    /**
     * @var array request array - will be prepered with @see json_decode
     */
    $request = array();
    /**
     * @var string defin if respone json is wrapped with portal defined fields
     */
    $wrapResponse = true;
    
    if(!Utils::IsNullOrEmptyVaraible($raw))
    {
        /**
        * @var array Treść żądania - szczegóły przesłane w zmiennej $query (jeśli ustawiona) i przekonwertowane do tablicy z JSON
        */
        $request = json_decode($raw, true);
        switch(json_last_error()) 
        {
            /**
            * don't throw error - newly created user accounts need to be accepted by user(with GET params) @see UsersWs::acceptRules($parameter)
            */
            case JSON_ERROR_NONE:
                $error = NULL; 
            break;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded';
            break;
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded ';
            break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given';
            break;
            default:
                $error = 'Unknown error';
            break;
        }
        if(!Utils::IsNullOrEmptyVaraible($error))
            throw new WrongRequestException('Błąd w żądaniu - sprawdź treść JSON-a: '.$error);
    }
    
    //We need token to login and authentication/authorization in *WsAuthorized zones/webservices
    /**
     * @var string token if request if for authorized stuff
     */
    $token = new TokenType();//create object(for now it is only placeholder)
    if(isset($request['token']))//FIXME: chage to Utils::IsNullOrEmptyVaraible to do this we need isset in our condition
    {
        $token->validate($request['token']);
        unset($request['token']);//remove it, token is not used as a input for methods!
    }       
    
    switch($area)
    {
        case AuthWs::GetClassName():
            $instance = new AuthWs($token);
        break;
        case AdsWs::GetClassName():
            $instance = new AdsWs();
        break;
        case AdsWsAuthorized::GetClassName():
            $instance = new AdsWsAuthorized($token);
        break;
        case ArticlesWs::GetClassName():
            $instance = new ArticlesWs();
        break;
        case ArticlesWsAuthorized::GetClassName():
            $instance = new ArticlesWsAuthorized($token);
        break;
        case UsersWs::GetClassName():
            $instance = new UsersWs();
        break;
        case UsersWsAuthorized::GetClassName():
            $instance = new UsersWsAuthorized($token);
        break;
        case ContactsWs::GetClassName():
            $instance = new ContactsWs();
        break;
        case ContactsWsAuthorized::GetClassName():
            $instance = new ContactsWsAuthorized($token);
        break;
        case MenuWs::GetClassName():
            $instance = new MenuWs();
        break;
        case MenuWsAuthorized::GetClassName():
            $instance = new MenuWsAuthorized($token);
        break;
        case ConfigWs::GetClassName():
            $instance = new ConfigWs();
        break;
        case ConfigWsAuthorized::GetClassName():
            $instance = new ConfigWsAuthorized($token);
        break;
        case EventsWs::GetClassName():
            $instance = new EventsWs();
        break;
        case EventsWsAuthorized::GetClassName():
            $instance = new EventsWsAuthorized($token);
        break;
        case GalleriesWs::GetClassName():
            $instance = new GalleriesWs();
        break;
        case GalleriesWsAuthorized::GetClassName():
            $instance = new GalleriesWsAuthorized($token);
        break;
        case GroupsWs::GetClassName():
            $instance = new GroupsWs();
        break;
        case GroupsWsAuthorized::GetClassName():
            $instance = new GroupsWsAuthorized($token);
        break;
        case Uddi::GetClassName():
            $instance = new Uddi();
            $wrapResponse = false;
        break;
        case CacheWsAuthorized::GetClassName():
            $instance = new CacheWsAuthorized($token);
        break;
        default:
            throw new WrongRequestException('Błąd w żądaniu - parametry GET żądania!');
    }
    $classProxy = new ClassProxy($area, $instance, $operation);
    $response = $classProxy->invoke($request);
}
catch(Exception $e)
{   
    $wrapResponse = true;
    $logger = new Logger('index.php');
    $message = $e->getMessage();
    if(!Utils::IsNullOrEmptyVaraible($e->getPrevious()))
        $logger->logError('Poprzedni wyjątek(jeśli istnieje): '.$e->getPrevious()->getMessage());
    $file = $e->getFile();
    $line = $e->getLine();
    $code = $e->getCode();
    $trace = $e->getTraceAsString();
    $logger->logError("Błąd w routerze! Wyjątek plik $file, linia $line, code: $code,trace: $trace", $e);
    $response = "Reason: $message, Error: $code";
}

if(Utils::IsNullOrEmptyVaraible($response) && !is_array($response))//always return true, or response - otherwise exeception will be thrown
    $response = true;

if($wrapResponse)
{
    $json = array();
    $json['hostname'] = gethostname();
    $json['area'] = $area;
    $json['operation'] = $operation;
    $json['response'] = $response;
}
else
    $json = $response;

header('Content-type: application/json; charset=UTF-8');//ustaw treść zwracaną jako JSON
if(is_array($json))
    echo json_encode($json, JSON_PRETTY_PRINT);
else
    echo $json;
?>

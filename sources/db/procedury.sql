-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE DEFINER=`smbfat`@`elitebook.dom` PROCEDURE `getMenu`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	declare rowId int;
	DECLARE cur CURSOR FOR SELECT id FROM smbfat.MENU where typ='ROOTMENU' order by kolejnosc; -- wybierz id rodziców
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	-- CREATE TEMPORARY TABLE IF NOT EXISTS tmpMenuTable engine = memory AS (SELECT * FROM smbfat.MENU);
	CREATE TEMPORARY TABLE IF NOT EXISTS tmpMenuTable (
								`ID` int(10) unsigned, `ORDER` tinyint(3) unsigned, `NAME` varchar(45) COLLATE utf8_polish_ci,
								`ADDER` int(10) unsigned, `ADD_DATE` timestamp null, `TYPE` varchar(8) COLLATE utf8_polish_ci,
								`PARENT` int(10) unsigned, `GALLERY` int(10) unsigned, `REMOVEABLE` tinyint(1), `REMOVED` tinyint(1),
								`LEAF` int(10) unsigned
								) engine=memory DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
	-- TRUNCATE tmpMenuTable;
	OPEN cur; -- trzeba otworzyć kursor(wyniki)

	set @@max_sp_recursion_depth=50;

	mainloop: loop
		FETCH cur INTO rowId;
		IF done THEN
			LEAVE mainloop;
		END IF;
		insert into tmpMenuTable SELECT * FROM smbfat.MENU where id=rowId;
		-- insert into tmpMenuTable SELECT * FROM smbfat.MENU where rodzic=rowId;
		call getMenuHierarchy(rowId);
		INSERT INTO tmpMenuTable (`ID`, `ADD_DATE`) VALUES (NULL, NULL);
	END loop;

	CLOSE cur;
	select * from tmpMenuTable;

	DROP TEMPORARY TABLE IF EXISTS tmpMenuTable;
END










-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
DELIMITER $$

CREATE PROCEDURE getMenuHierarchy(in parentId int) 
BEGIN
	DECLARE done INT DEFAULT FALSE;
	declare rowId int;
	DECLARE cur CURSOR FOR SELECT id FROM smbfat.MENU where rodzic=parentId order by kolejnosc; -- wybierz id rodziców
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur; -- trzeba otworzyć kursor(wyniki)

	mainloop: loop
		FETCH cur INTO rowId;
		IF done THEN
			LEAVE mainloop;
		END IF;
		insert into tmpMenuTable SELECT * FROM smbfat.MENU where id=rowId order by kolejnosc;
		call getMenuHierarchy(rowId);
	END loop;
	
	Close cur;
END 


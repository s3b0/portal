-- ---------------------------------
-- Obsługa smbfat.pl
-- ---------------------------------

-- ---------------------------------
-- Użytkownicy
-- ---------------------------------
INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('ks. Tomasz Zaperty', 'kstzaperty', 'xtomaszzaperty@gmail.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AP'), 1);

INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Marcin Kiernozek', 'mkiernozek', 'lomzyniaczek@gmail.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='EP'), 1);

INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Wojciech Ciebiera', 'wciebiera', 'wojciech.ciebiera@gmail.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AGP/EGP'), 1);

INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Teściarz Jeden', 'ttesciaz1', 'testowy1@test.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AGP/EGP'), 1);
INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Teściarz Dwa', 'ttesciaz2', 'testowy2@test.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AGP/EGP'), 1);
INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Teściarz Trzy', 'ttesciaz3', 'testowy3@test.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AGP/EGP'), 1);
-- ---------------------------------
-- Grupy
-- ---------------------------------
INSERT INTO `smbfat`.`GRUPY` 
(`nazwa`, `dodal`) 
VALUES 
('Grupa testowa nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1));
INSERT INTO `smbfat`.`GRUPY` 
(`nazwa`, `posiada_galerie`, `dodal`) 
VALUES 
('Grupa testowa nr.2', 1, (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1));
INSERT INTO `smbfat`.`GRUPY` 
(`nazwa`, `dodal`) 
VALUES 
('Grupa testowa nr.3', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1));

-- AGP
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.1'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz2'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz3'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
-- EGP
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz2'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.1'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz3'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.1'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz3'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz2'), 
(SELECT `GRUPY`.`nazwa` FROM `smbfat`.`GRUPY` WHERE `GRUPY`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
-- ---------------------------------
-- Galerie
-- ---------------------------------
INSERT INTO `smbfat`.`GALERIE` 
(`przeznaczenie`, `dodal`, `nazwa_grupy`, `zdjecia_pelne_katalog`, `zdjecia_miniatury_katalog`)
VALUES ('Galeria grupy testowej nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 
'Grupa testowa nr.2', '/photos/ggtnr2','/photos/ggtnr2/thumbnails');
-- sprawdź wyniki
-- SELECT count(*) as '9=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 9 rekordów
-- SELECT count(*) as '3=' FROM `smbfat`.`GRUPY`; -- 3 rekordy
-- SELECT count(*) as '5=' FROM `smbfat`.`GALERIE`; -- 5 rekordów
-- ---------------------------------
-- Menu
-- ---------------------------------
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`)
VALUES
((SELECT ckol FROM (SELECT max(`kolejnosc`)+1 as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'ROOTMENU') AS c), 'ROOTMENU nr.1', 
(SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 
(SELECT `MENU_TYP`.`nazwa` FROM `smbfat`.`MENU_TYP` WHERE `smbfat`.`MENU_TYP`.`nazwa` = 'ROOTMENU'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`)
VALUES
((SELECT ckol FROM (SELECT max(`kolejnosc`)+1 as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'ROOTMENU') AS c), 'ROOTMENU nr.2', 
(SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 
(SELECT `MENU_TYP`.`nazwa` FROM `smbfat`.`MENU_TYP` WHERE `smbfat`.`MENU_TYP`.`nazwa` = 'ROOTMENU'));

INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
(0, 'SUBMENU nr.1 dla ROOTMENU nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1') as c));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'SUBMENU' and 
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1')) as c), 
'SUBMENU nr.2 dla ROOTMENU nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1') as c));
INSERT INTO `smbfat`.`LISCE`
(`tytul`, `dodal`, `z_ostatniej_chwili`, `zdjecie`, `nazwa_pliku`)
VALUES
('Z ostatniej chwili', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 1, 
(SELECT `ZDJECIA`.`id` FROM `smbfat`.`ZDJECIA` WHERE `smbfat`.`ZDJECIA`.`nazwa` = 'default.jpg'), 'zostatniejchwili');
INSERT INTO `smbfat`.`LISCE`
(`tytul`, `dodal`, `przyklejony`, `zdjecie`, `nazwa_pliku`)
VALUES
('Przyklejony', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 1, 
(SELECT `ZDJECIA`.`id` FROM `smbfat`.`ZDJECIA` WHERE `smbfat`.`ZDJECIA`.`nazwa` = 'default.jpg'), 'przyklejony');
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1')) as c), 
'LEAF nr.1 dla ROOTMENU nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='zostatniejchwili'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1')) as c), 
'LEAF nr.2 dla ROOTMENU nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='przyklejony'));
INSERT INTO `smbfat`.`LISCE`
(`tytul`, `dodal`, `slider`, `zdjecie`, `nazwa_pliku`)
VALUES
('Slider nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 1, 
(SELECT `ZDJECIA`.`id` FROM `smbfat`.`ZDJECIA` WHERE `smbfat`.`ZDJECIA`.`nazwa` = 'default.jpg'), 'slidernr1');
INSERT INTO `smbfat`.`LISCE`
(`tytul`, `dodal`, `slider`, `zdjecie`, `nazwa_pliku`)
VALUES
('Slider nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 1, 
(SELECT `ZDJECIA`.`id` FROM `smbfat`.`ZDJECIA` WHERE `smbfat`.`ZDJECIA`.`nazwa` = 'default.jpg'), 'slidernr2');
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
(0, 'LEAF nr.1 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='slidernr1'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2')) as c), 
'LEAF nr.2 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='slidernr2'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
(0, 'SUBMENU nr.1 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'SUBMENU' and 
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2')) as c), 
'SUBMENU nr.2 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c));
-- ---------------------------------
-- Zdjecia
-- ---------------------------------
INSERT INTO `smbfat`.`ZDJECIA` 
(`nazwa`, `typ`, `dodal`, `galeria`)
VALUES
('fotkanr1_galeri_grupy_testowejnr1', 'JPEG', 1, 
(select id from `GALERIE` where `przeznaczenie` = 'Galeria grupy testowej nr.2'));
INSERT INTO `smbfat`.`ZDJECIA` 
(`nazwa`, `typ`, `dodal`, `galeria`)
VALUES
('fotkanr2_galeri_grupy_testowejnr1', 'JPEG', 1, 
(select id from `GALERIE` where `przeznaczenie` = 'Galeria grupy testowej nr.2'));
-- ---------------------------------
-- Kontakty
-- ---------------------------------
-- INSERT INTO `smbfat`.`KONTAKTY` (`temat`, `dodal`) VALUES ('Znalazłem błąd', 1); -- już istnieje
INSERT INTO `smbfat`.`KONTAKTY`
(`temat`, `dodal`)
VALUES
('Pytanie odnośnie wpisu', 1);
INSERT INTO `smbfat`.`KONTAKTY`
(`temat`, `dodal`)
VALUES
('Smbfat.pl sugestie', 1);

-- INSERT INTO `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE` (`kontakt_temat`, `rola_nazwa`) VALUES ('Znalazłem błąd', 'AT');
INSERT INTO `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE`
(`kontakt_temat`,
`rola_nazwa`)
VALUES
('Pytanie odnośnie wpisu', 'AP');
INSERT INTO `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE`
(`kontakt_temat`,
`rola_nazwa`)
VALUES
('Pytanie odnośnie wpisu', 'EP');
INSERT INTO `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE`
(`kontakt_temat`,
`rola_nazwa`)
VALUES
('Pytanie odnośnie wpisu', 'AGP/EGP');
INSERT INTO `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE`
(`kontakt_temat`,
`rola_nazwa`)
VALUES
('Smbfat.pl sugestie', 'EP');
-- ---------------------------------
-- Ogłoszenia
-- ---------------------------------
INSERT INTO `smbfat`.`OGLOSZENIA`
(`tresc`, `ip_dodajacego`, `kategoria`, `token`, `email_dodajacego`, `telefon_dodajacego`)
VALUES
('Treść ogłoszenia nr.1', '127.0.0.1', 'Kupię','7ae645605cba6e84337a58c22e0470b1d32e6617', 'email@test.pl','+48001002003');
INSERT INTO `smbfat`.`OGLOSZENIA`
(`tresc`, `ip_dodajacego`, `kategoria`, `token`, `email_dodajacego`, `telefon_dodajacego`)
VALUES
('Treść ogłoszenia nr.2', '127.0.0.1', 'Oddam w dobre ręce','1cd34af1fb68ba2a432ac52b46da22b0168b9b47', 'email@test.pl','+48001002003');
INSERT INTO `smbfat`.`OGLOSZENIA`
(`tresc`, `ip_dodajacego`, `kategoria`, `token`, `email_dodajacego`, `telefon_dodajacego`)
VALUES
('Treść ogłoszenia nr.3', '127.0.0.1', 'Usługi','011e357f8fb61bdd15c0456ade7b8c37fa44f185', 'email@test.pl','+48001002003');
-- ---------------------------------
-- Portal
-- ---------------------------------
INSERT INTO `smbfat`.`PORTAL`
(`opis`)
VALUES
('Opis wydarzeń nr. 1');
INSERT INTO `smbfat`.`PORTAL`
(`opis`)
VALUES
('Opis wydarzeń nr. 2');
-- ---------------------------------
-- Kalendarz
-- ---------------------------------
INSERT INTO `smbfat`.`KALENDARZ`
(`cykl`,`typ`, `start_data`, `koniec_data`,`start_godzina`, `koniec_godzina`, `dodal`,`nazwa`, `opis`, `dzien_tyg_szosty`)
VALUES
('1', 'Różaniec', CURDATE(), CURDATE(), NOW(), DATE_ADD(NOW(), INTERVAL 2 HOUR), 1, 'Zdarzenie jednorazowe', 'Opis test', 1);
INSERT INTO `smbfat`.`KALENDARZ`
(`cykl`,`typ`, `start_data`, `start_godzina`, `koniec_godzina`, `dodal`,`nazwa`, `opis`, `dzien_tyg_siodmy`)
VALUES
('C', 'Msza święta', CURDATE(), NOW(), DATE_ADD(NOW(), INTERVAL 2 HOUR), 1, 'Zdarzenie cykliczne', 'Opis test', 1);
INSERT INTO `smbfat`.`KALENDARZ`
(`cykl`,`typ`, `start_data`, `koniec_data`, `start_godzina`, `koniec_godzina`, `dodal`,`nazwa`, `opis`, `dzien_tyg_czwarty`, `dzien_tyg_siodmy`)
VALUES
('O', 'Zwykłe', CURDATE(), DATE_ADD(CURDATE(), INTERVAL 10 DAY), NOW(), DATE_ADD(NOW(), INTERVAL 2 HOUR), 1, 'Zdarzenie okresowe', 'Opis test', 1, 1);
commit;
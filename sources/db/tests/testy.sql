-- ---------------------------------
-- Test relacji
-- ---------------------------------
-- relacja UZYTKOWNICY <-> ROLE_W_SYSTEMIE
UPDATE `smbfat`.`ROLE_W_SYSTEMIE` SET `nazwa` = 'TEST' WHERE `nazwa` = 'EP';
-- sprawdź wyniki
SELECT count(*) as '1=' FROM `smbfat`.`UZYTKOWNICY` where `smbfat`.`UZYTKOWNICY`.`rola_w_systemie` = 'TEST'; -- 1 rekord
UPDATE `smbfat`.`ROLE_W_SYSTEMIE` SET `nazwa` = 'EP' WHERE `nazwa` = 'TEST';
-- sprawdź wyniki
SELECT count(*) as '0=' FROM `smbfat`.`UZYTKOWNICY` where `smbfat`.`UZYTKOWNICY`.`rola_w_systemie` = 'TEST'; -- 0 rekordów
-- ---------------------------------
-- Test relacji
-- ---------------------------------
-- relacja UZYTKOWNICY <-> UZYTKOWNICY_ZDO_GRUPY <-> GRUPY_PRZYKOSCIELNE
-- sprawdź wyniki
SELECT count(*) as '7=' FROM `smbfat`.`UZYTKOWNICY`; -- 7 rekordów
SELECT count(*) as '3=' FROM `smbfat`.`GRUPY_PRZYKOSCIELNE`; -- 3 rekordy
SELECT count(*) as '9=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 9 rekordów;

DELETE FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1';
DELETE FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.3';
-- sprawdź wyniki
SELECT count(*) as '6=' FROM `smbfat`.`UZYTKOWNICY`; -- 6 rekordów
SELECT count(*) as '2=' FROM `smbfat`.`GRUPY_PRZYKOSCIELNE`; -- 2 rekordy
SELECT count(*) as '4=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 4 rekordy;
-- przywróć bazę
INSERT INTO `smbfat`.`UZYTKOWNICY`
(`imie_nazwisko`, `login`, `email`, `aktwyny`, `dane_osobowe_wniosek_wyslany`, `dane_osobowe_przetwarzanie_potwierdzone`,
`awatar`, `rola_w_systemie`, `dodal`)
VALUES
('Teściarz Jeden', 'ttesciaz1', 'testowy1@test.com', 1, 1, 1, 
(SELECT id FROM smbfat.ZDJECIA where nazwa='default.jpg'), 
(SELECT nazwa FROM smbfat.ROLE_W_SYSTEMIE where nazwa='AGP/EGP'), 1);
INSERT INTO `smbfat`.`GRUPY_PRZYKOSCIELNE` (`nazwa`, `dodal`) VALUES ('Grupa testowa nr.3', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.1'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz3'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz2'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.3'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
-- sprawdź wyniki
SELECT count(*) as '7=' FROM `smbfat`.`UZYTKOWNICY`; -- 7 rekordów
SELECT count(*) as '3=' FROM `smbfat`.`GRUPY_PRZYKOSCIELNE`; -- 3 rekordy
SELECT count(*) as '9=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 9 rekordów;
-- ---------------------------------
-- Test relacji
-- ---------------------------------
-- relacja GRUPY_PRZYKOSCIELNE <-> GALERIE
DELETE FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.2';
-- sprawdź wyniki
SELECT count(*) as '6=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 6 rekordów
SELECT count(*) as '2=' FROM `smbfat`.`GRUPY_PRZYKOSCIELNE`; -- 2 rekordy
SELECT count(*) as '4=' FROM `smbfat`.`GALERIE`; -- 4 rekordów
-- przywróć bazę
INSERT INTO `smbfat`.`GRUPY_PRZYKOSCIELNE` 
(`nazwa`, `posiada_galerie`, `dodal`) 
VALUES 
('Grupa testowa nr.2', 1, (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz2'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'AGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz1'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` 
(`id_uzytkownika`, `nazwa_grupy`,`rola_w_grupie`)
VALUES 
((SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`login` = 'ttesciaz3'), 
(SELECT `GRUPY_PRZYKOSCIELNE`.`nazwa` FROM `smbfat`.`GRUPY_PRZYKOSCIELNE` WHERE `GRUPY_PRZYKOSCIELNE`.`nazwa` = 'Grupa testowa nr.2'), 
(SELECT `ROLE_W_GRUPIE`.`nazwa` FROM `smbfat`.`ROLE_W_GRUPIE` WHERE `ROLE_W_GRUPIE`.`nazwa` = 'EGP'));
INSERT INTO `smbfat`.`GALERIE` 
(`przeznaczenie`, `dodal`, `nazwa_grupy`, `zdjecia_pelne_katalog`, `zdjecia_miniatury_katalog`)
VALUES ('Galeria grupy testowej nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 
'Grupa testowa nr.2', '/photos/ggtnr2','/photos/ggtnr2/thumbnails');
-- sprawdź wyniki
SELECT count(*) as '9=' FROM `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`; -- 9 rekordów
SELECT count(*) as '3=' FROM `smbfat`.`GRUPY_PRZYKOSCIELNE`; -- 3 rekordy
SELECT count(*) as '5=' FROM `smbfat`.`GALERIE`; -- 5 rekordów

-- ---------------------------------
-- Test relacji
-- ---------------------------------
-- relacja MENU <-> MENU
-- sprawdź wyniki
select count(*) as '15=' from MENU; -- 15 rekordów
delete from `smbfat`.`MENU` where nazwa = 'LEAF nr.2 dla ROOTMENU nr.2';
delete from `smbfat`.`MENU` where nazwa = 'SUBMENU nr.2 dla ROOTMENU nr.2';
-- sprawdź wyniki
select count(*) as '13=' from MENU; -- 13 rekordów
delete from `smbfat`.`MENU` where nazwa = 'ROOTMENU nr.2';
-- sprawdź wyniki
select count(*) as '10=' from MENU; -- 10 rekordów
-- przywróć bazę
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`)
VALUES
((SELECT ckol FROM (SELECT max(`kolejnosc`)+1 as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'ROOTMENU') AS c), 'ROOTMENU nr.2', 
(SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 
(SELECT `MENU_TYP`.`nazwa` FROM `smbfat`.`MENU_TYP` WHERE `smbfat`.`MENU_TYP`.`nazwa` = 'ROOTMENU'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
(0,'LEAF nr.1 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='slidernr1'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2')) as c), 
'LEAF nr.2 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='slidernr2'));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
(0, 'SUBMENU nr.1 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c));
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where `smbfat`.`MENU`.`TYP` = 'SUBMENU' and 
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2')) as c), 
'SUBMENU nr.2 dla ROOTMENU nr.2', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'SUBMENU', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.2') as c));
-- ---------------------------------
-- Test relacji
-- ---------------------------------
-- relacja LISCIE <-> MENU
-- sprawdź wyniki
SELECT count(*) as '7=' FROM `smbfat`.`LISCE`; -- 7 rekordów
SELECT count(*) as '15=' FROM `smbfat`.`MENU`; -- 15 rekordów
SET SQL_SAFE_UPDATES=0; 
DELETE FROM `smbfat`.`LISCE` WHERE `smbfat`.`LISCE`.`tytul` = 'Z ostatniej chwili';
SET SQL_SAFE_UPDATES=1; 
-- sprawdź wyniki
SELECT count(*) as '6=' FROM `smbfat`.`LISCE`; -- 6 rekordów
SELECT count(*) as '14=' FROM `smbfat`.`MENU`; -- 14 rekordów
-- przywróć bazę
INSERT INTO `smbfat`.`LISCE`
(`tytul`, `dodal`, `z_ostatniej_chwili`, `zdjecie`, `nazwa_pliku`)
VALUES
('Z ostatniej chwili', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1), 1, 
(SELECT `ZDJECIA`.`id` FROM `smbfat`.`ZDJECIA` WHERE `smbfat`.`ZDJECIA`.`nazwa` = 'default.jpg'), 'zostatniejchwili');
INSERT INTO `smbfat`.`MENU`
(`kolejnosc`, `nazwa`, `dodal`, `typ`, `rodzic`, `lisc`)
VALUES
((SELECT ckol+1 FROM (SELECT max(kolejnosc) as 'ckol' FROM `smbfat`.`MENU` where
`smbfat`.`MENU`.`rodzic` = (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1')) as c), 
'LEAF nr.1 dla ROOTMENU nr.1', (SELECT `UZYTKOWNICY`.`id` FROM `smbfat`.`UZYTKOWNICY` WHERE `UZYTKOWNICY`.`id` = 1),
 'LEAF', (select id from (select id from `smbfat`.`MENU` where `smbfat`.`MENU`.`nazwa` = 'ROOTMENU nr.1') as c)
, (SELECT id FROM smbfat.LISCE where nazwa_pliku='zostatniejchwili'));
-- sprawdź wyniki
SELECT count(*) as '7=' FROM `smbfat`.`LISCE`; -- 7 rekordów
SELECT count(*) as '15=' FROM `smbfat`.`MENU`; -- 15 rekordów
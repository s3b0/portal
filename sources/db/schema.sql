SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `smbfat` ;
CREATE SCHEMA IF NOT EXISTS `smbfat` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `smbfat` ;

-- -----------------------------------------------------
-- Table `smbfat`.`ROLE_W_SYSTEMIE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`ROLE_W_SYSTEMIE` (
  `nazwa` VARCHAR(7) NOT NULL,
  `pelna_nazwa` VARCHAR(46) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  UNIQUE INDEX `pelna_nazwa_UNIQUE` (`pelna_nazwa` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`MENU_TYP`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`MENU_TYP` (
  `nazwa` VARCHAR(8) NOT NULL,
  `opis` VARCHAR(100) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`LISCE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`LISCE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tytul` VARCHAR(255) NOT NULL,
  `dodal` INT UNSIGNED NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `z_ostatniej_chwili` TINYINT(1) NOT NULL DEFAULT FALSE,
  `przyklejony` TINYINT(1) NOT NULL DEFAULT FALSE,
  `slider` TINYINT(1) NOT NULL DEFAULT FALSE,
  `katalog` VARCHAR(11) NOT NULL DEFAULT '/articles/',
  `zdjecie` INT UNSIGNED NULL,
  `nazwa_pliku` VARCHAR(279) NOT NULL,
  `usuwalne` TINYINT(1) NOT NULL DEFAULT TRUE,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`, `tytul`),
  INDEX `artykul_do_uzytkownicy_idx` (`dodal` ASC),
  INDEX `artykul_do_zdjecie_idx` (`zdjecie` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `artykul_do_uzytkownicy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `artykul_do_zdjecie`
    FOREIGN KEY (`zdjecie`)
    REFERENCES `smbfat`.`ZDJECIA` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`MENU`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`MENU` (
  `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `kolejnosc` TINYINT UNSIGNED NOT NULL,
  `nazwa` VARCHAR(45) NOT NULL,
  `dodal` INT UNSIGNED NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `typ` VARCHAR(8) NOT NULL,
  `rodzic` TINYINT UNSIGNED NULL,
  `galeria` INT UNSIGNED NULL,
  `usuwalne` TINYINT(1) NOT NULL DEFAULT TRUE,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `lisc` INT UNSIGNED NULL,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  INDEX `menu_root_do_uzytkownicy_idx` (`dodal` ASC),
  INDEX `menu_do_menu_typ_idx` (`typ` ASC),
  INDEX `menu_dziecko_do_menu_rodzic_idx` (`rodzic` ASC),
  INDEX `menu_do_galeria_idx` (`galeria` ASC),
  INDEX `menu_do_lisc_idx` (`lisc` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `menu_root_do_uzytkownicy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `menu_do_menu_typ`
    FOREIGN KEY (`typ`)
    REFERENCES `smbfat`.`MENU_TYP` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `menu_dziecko_do_menu_rodzic`
    FOREIGN KEY (`rodzic`)
    REFERENCES `smbfat`.`MENU` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `menu_do_galeria`
    FOREIGN KEY (`galeria`)
    REFERENCES `smbfat`.`GALERIE` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `menu_do_lisc`
    FOREIGN KEY (`lisc`)
    REFERENCES `smbfat`.`LISCE` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`GRUPY`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`GRUPY` (
  `nazwa` VARCHAR(45) NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `posiada_galerie` TINYINT(1) NOT NULL DEFAULT FALSE,
  `aktywna` TINYINT(1) NOT NULL DEFAULT TRUE,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `dodal` INT UNSIGNED NOT NULL,
  `id_menu_grupy` TINYINT UNSIGNED NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `NAZWA_UNIQUE` (`nazwa` ASC),
  INDEX `uzytkownicy_do_grupy_idx` (`dodal` ASC),
  INDEX `grupy_do_menu_idx` (`id_menu_grupy` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `uzytkownicy_do_grupy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `grupy_do_menu`
    FOREIGN KEY (`id_menu_grupy`)
    REFERENCES `smbfat`.`MENU` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`GALERIE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`GALERIE` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `przeznaczenie` VARCHAR(45) NULL,
  `dodal` INT UNSIGNED NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nazwa_grupy` VARCHAR(45) NULL,
  `zdjecia_pelne_katalog` VARCHAR(255) NOT NULL,
  `zdjecia_miniatury_katalog` VARCHAR(255) NOT NULL,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `usuwalne` TINYINT(1) NOT NULL DEFAULT TRUE,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `uzytkownik_do_galeria_idx` (`dodal` ASC),
  INDEX `grupa_do_galeria_idx` (`nazwa_grupy` ASC),
  UNIQUE INDEX `zdjecia_pelne_katalog_UNIQUE` (`zdjecia_pelne_katalog` ASC),
  UNIQUE INDEX `zdjecia_miniatury_katalog_UNIQUE` (`zdjecia_miniatury_katalog` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `uzytkownik_do_galeria`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `grupa_do_galeria`
    FOREIGN KEY (`nazwa_grupy`)
    REFERENCES `smbfat`.`GRUPY` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`TYPY_ZDJEC`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`TYPY_ZDJEC` (
  `typ` VARCHAR(4) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`typ`),
  UNIQUE INDEX `typ_UNIQUE` (`typ` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`ZDJECIA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`ZDJECIA` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nazwa` VARCHAR(50) NOT NULL,
  `typ` VARCHAR(4) NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dodal` INT UNSIGNED NOT NULL,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `galeria` INT UNSIGNED NOT NULL,
  `usuwalne` TINYINT(1) NOT NULL DEFAULT TRUE,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `uzytkownik_id_idx` (`dodal` ASC),
  INDEX `galeria_do_zdjecie_idx` (`galeria` ASC),
  INDEX `typ_zdjecia_do_zdjecia_idx` (`typ` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `uzytkownik_id_fk`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `galeria_do_zdjecie`
    FOREIGN KEY (`galeria`)
    REFERENCES `smbfat`.`GALERIE` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `typ_zdjecia_do_zdjecia`
    FOREIGN KEY (`typ`)
    REFERENCES `smbfat`.`TYPY_ZDJEC` (`typ`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`UZYTKOWNICY`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`UZYTKOWNICY` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `imie_nazwisko` VARCHAR(45) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `ulica` VARCHAR(45) NULL,
  `numer_domu_mieszkania` VARCHAR(45) NULL,
  `miasto` VARCHAR(45) NULL,
  `aktwyny` TINYINT(1) NOT NULL DEFAULT TRUE,
  `dane_osobowe_wniosek_wyslany` TINYINT(1) NOT NULL DEFAULT FALSE,
  `dane_osobowe_przetwarzanie_potwierdzone` TINYINT(1) NOT NULL,
  `awatar` INT UNSIGNED NULL DEFAULT 1,
  `rola_w_systemie` VARCHAR(7) NOT NULL,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dodal` INT UNSIGNED NOT NULL,
  `usuwalne` TINYINT(1) NOT NULL DEFAULT TRUE,
  `haslo` VARCHAR(32) NOT NULL,
  `wojewodztwo` VARCHAR(2) NULL,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `LOGIN_UNIQUE` (`login` ASC),
  UNIQUE INDEX `EMAIL_UNIQUE` (`email` ASC),
  INDEX `rola_w_systemie_do_uzytkownicy_idx` (`rola_w_systemie` ASC),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `dodany_przez_do_uzytkownicy_idx` (`dodal` ASC),
  INDEX `zdjecia_do_uzytkownicy_idx` (`awatar` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `dodany_przez_do_uzytkownicy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rola_w_systemie_do_uzytkownicy`
    FOREIGN KEY (`rola_w_systemie`)
    REFERENCES `smbfat`.`ROLE_W_SYSTEMIE` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `zdjecia_do_uzytkownicy`
    FOREIGN KEY (`awatar`)
    REFERENCES `smbfat`.`ZDJECIA` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`ROLE_W_GRUPIE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`ROLE_W_GRUPIE` (
  `nazwa` VARCHAR(3) NOT NULL,
  `pelna_nazwa` VARCHAR(34) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  UNIQUE INDEX `NAZWA_UNIQUE` (`nazwa` ASC),
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `pelna_nazwa_UNIQUE` (`pelna_nazwa` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`UZYTKOWNICY_ZDO_GRUPY`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`UZYTKOWNICY_ZDO_GRUPY` (
  `id_uzytkownika` INT UNSIGNED NOT NULL,
  `nazwa_grupy` VARCHAR(45) NOT NULL,
  `rola_w_grupie` VARCHAR(3) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  INDEX `NAZWA_GRUPY_INDEX` (`nazwa_grupy` ASC),
  INDEX `ID_UZYTKOWNIKA_INDEX` (`id_uzytkownika` ASC),
  INDEX `ROLA_INDEX` (`rola_w_grupie` ASC),
  PRIMARY KEY (`id_uzytkownika`, `nazwa_grupy`, `rola_w_grupie`),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `fk_id_uzytkownika`
    FOREIGN KEY (`id_uzytkownika`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_grupy`
    FOREIGN KEY (`nazwa_grupy`)
    REFERENCES `smbfat`.`GRUPY` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rola_w_grupie`
    FOREIGN KEY (`rola_w_grupie`)
    REFERENCES `smbfat`.`ROLE_W_GRUPIE` (`nazwa`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`TYPY_WYDARZEN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`TYPY_WYDARZEN` (
  `nazwa` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(255) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`KONTAKTY`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`KONTAKTY` (
  `temat` VARCHAR(100) NOT NULL,
  `dodal` INT UNSIGNED NOT NULL DEFAULT 0,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`temat`),
  UNIQUE INDEX `temat_UNIQUE` (`temat` ASC),
  INDEX `kontakt_do_uzytkownicy_idx` (`dodal` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `kontakt_do_uzytkownicy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`KONTAKTY_ZDO_ROLE_W_SYSTEMIE` (
  `kontakt_temat` VARCHAR(100) NOT NULL,
  `rola_nazwa` VARCHAR(7) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`kontakt_temat`, `rola_nazwa`),
  INDEX `fk_KONTAKTY_has_ROLE_W_SYSTEMIE_ROLE_W_SYSTEMIE1_idx` (`rola_nazwa` ASC),
  INDEX `fk_KONTAKTY_has_ROLE_W_SYSTEMIE_KONTAKTY1_idx` (`kontakt_temat` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `fk_KONTAKTY_has_ROLE_W_SYSTEMIE_KONTAKTY1`
    FOREIGN KEY (`kontakt_temat`)
    REFERENCES `smbfat`.`KONTAKTY` (`temat`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_KONTAKTY_has_ROLE_W_SYSTEMIE_ROLE_W_SYSTEMIE1`
    FOREIGN KEY (`rola_nazwa`)
    REFERENCES `smbfat`.`ROLE_W_SYSTEMIE` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`SKORKI`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`SKORKI` (
  `nazwa` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(255) NOT NULL,
  `katalog` VARCHAR(10) NULL DEFAULT '/skins/',
  `nazwa_pliku` VARCHAR(45) NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wzorzec` BLOB NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  UNIQUE INDEX `nazwa_pliku_UNIQUE` (`nazwa_pliku` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`KATEGORIE_OGLOSZEN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`KATEGORIE_OGLOSZEN` (
  `nazwa` VARCHAR(30) NOT NULL,
  `wyrozniona` TINYINT(1) NOT NULL DEFAULT FALSE,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nazwa`),
  UNIQUE INDEX `nazwa_UNIQUE` (`nazwa` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`OGLOSZENIA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`OGLOSZENIA` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tresc` VARCHAR(500) NOT NULL,
  `ip_dodajacego` VARCHAR(45) NOT NULL,
  `kategoria` VARCHAR(30) NOT NULL,
  `zaakceptowal` INT UNSIGNED NULL,
  `data_zaakceptowania` TIMESTAMP NULL,
  `zaakceptowane` TINYINT(1) NOT NULL DEFAULT FALSE,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `token` VARCHAR(40) NOT NULL,
  `email_dodajacego` VARCHAR(50) NULL,
  `telefon_dodajacego` VARCHAR(50) NULL,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `ogloszenia_do_kategorie_ogloszen_idx` (`kategoria` ASC),
  INDEX `ogloszenia_do_uzytkownicy_idx` (`zaakceptowal` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `ogloszenia_do_kategorie_ogloszen`
    FOREIGN KEY (`kategoria`)
    REFERENCES `smbfat`.`KATEGORIE_OGLOSZEN` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ogloszenia_do_uzytkownicy`
    FOREIGN KEY (`zaakceptowal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`KALENDARZ_CYKLE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`KALENDARZ_CYKLE` (
  `oznaczenie` CHAR NOT NULL,
  `opis` VARCHAR(255) NOT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`oznaczenie`),
  UNIQUE INDEX `oznaczenie_UNIQUE` (`oznaczenie` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`KALENDARZ`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`KALENDARZ` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cykl` CHAR NOT NULL,
  `typ` VARCHAR(45) NULL,
  `start_data` DATE NOT NULL,
  `koniec_data` DATE NULL,
  `start_godzina` TIME NOT NULL,
  `koniec_godzina` TIME NOT NULL,
  `dodal` INT UNSIGNED NOT NULL,
  `data_dodania` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuniety` TINYINT(1) NOT NULL DEFAULT FALSE,
  `nazwa` VARCHAR(255) NOT NULL,
  `opis` VARCHAR(1024) NULL,
  `dzien_tyg_pierwszy` TINYINT(1) NULL,
  `dzien_tyg_drugi` TINYINT(1) NULL,
  `dzien_tyg_trzeci` TINYINT(1) NULL,
  `dzien_tyg_czwarty` TINYINT(1) NULL,
  `dzien_tyg_piaty` TINYINT(1) NULL,
  `dzien_tyg_szosty` TINYINT(1) NULL,
  `dzien_tyg_siodmy` TINYINT(1) NULL,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `kalendarz_do_kalendarz_cykle_idx` (`cykl` ASC),
  INDEX `kalendarz_do_typ_wydarzen_idx` (`typ` ASC),
  INDEX `kalendarz_do_uzytkownicy_idx` (`dodal` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC),
  CONSTRAINT `kalendarz_do_kalendarz_cykle`
    FOREIGN KEY (`cykl`)
    REFERENCES `smbfat`.`KALENDARZ_CYKLE` (`oznaczenie`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `kalendarz_do_typ_wydarzen`
    FOREIGN KEY (`typ`)
    REFERENCES `smbfat`.`TYPY_WYDARZEN` (`nazwa`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `kalendarz_do_uzytkownicy`
    FOREIGN KEY (`dodal`)
    REFERENCES `smbfat`.`UZYTKOWNICY` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`PORTAL`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`PORTAL` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `opis` VARCHAR(255) NOT NULL,
  `data_dodania` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `pdo_autoident` INT UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;


-- -----------------------------------------------------
-- Table `smbfat`.`PORTAL_KONFIGURACJA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`PORTAL_KONFIGURACJA` (
  `nazwa_sekcji` VARCHAR(45) NOT NULL,
  `nazwa_parametru` VARCHAR(45) NOT NULL,
  `wartosc` TEXT NULL,
  `pdo_autoident` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  UNIQUE INDEX `nazwa_klucza_UNIQUE` (`nazwa_parametru` ASC),
  FULLTEXT INDEX `wartosc_FULLTEXT` (`wartosc` ASC),
  PRIMARY KEY (`nazwa_parametru`),
  UNIQUE INDEX `pdo_autoident_UNIQUE` (`pdo_autoident` ASC))
ENGINE = MyISAM
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_polish_ci;

USE `smbfat` ;

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`MENU_FULL_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`MENU_FULL_VIEW` (`'ROOTMENU_NAME'` INT, `'SUBMENU_NAME'` INT, `'SUBMENU_LEAF_ID'` INT, `'SUBMENU_GALERY_ID'` INT, `'LEAFMENU_NAME'` INT, `'LEAFMENU_LEAF_ID'` INT, `'LEAFMENU_GALERY_ID'` INT, `'LEAF_NAME'` INT, `'LEAF_ARTICLE_ID'` INT, `'LEAF_GALERY_ID'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`ARTICLE_FULL_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`ARTICLE_FULL_VIEW` (`'ARTICLE_ID'` INT, `'ARTICLE_CREATION_DATE'` INT, `'ARTICLE_TITLE'` INT, `'ARTICLE_IS_BRAKING_NEWS'` INT, `'ARTICLE_IS_STICKY'` INT, `'ARTICLE_IS_IN_SLIDER'` INT, `'ARTICLE_CATALOG'` INT, `'ARTICLE_FILE_NAME'` INT, `'ARTICLE_PHOTO_NAME'` INT, `'ARTICLE_PHOTO_THUMBNAIL_CATALOG'` INT, `'ARTICLE_PHOTO_CATALOG'` INT, `'ARTICLE_AUTHOR_NAME'` INT, `'ARTICLE_AUTHOR_EMAIL'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`USER_FULL_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`USER_FULL_VIEW` (`'USER_ID'` INT, `'NAME_SURNAME'` INT, `'LOGIN'` INT, `'EMAIL'` INT, `'STREET'` INT, `'HOME_NUMBER'` INT, `'CITY'` INT, `'VOIVODESHIP'` INT, `'SYSTEM_ROLE'` INT, `'PASSWORD'` INT, `'USER_AVATAR_NAME'` INT, `'USER_AVATAR_THUMBNAIL_CATALOG'` INT, `'USER_AVATAR_CATALOG'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`USER_GROUP_FULL_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`USER_GROUP_FULL_VIEW` (`USER_ID` INT, `USER_GROUP_NAME` INT, `USER_GROUP_ROLE` INT, `USER_GROUP_DESC` INT, `USER_GROUP_GALLERY_ID` INT, `USER_GROUP_GALLERY_THUMBNAIL_CATALOG` INT, `USER_GROUP_GALLERY_CATALOG` INT);

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`PHOTO_FULL_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`PHOTO_FULL_VIEW` (`'PHOTO_ID'` INT, `'PHOTO_NAME'` INT, `'PHOTO_TYPE'` INT, `'PHOTO_ADD_DATE'` INT, `'PHOTO_GALLERY_DESC'` INT, `'PHOTO_GALLERY_OWNER_GROUP_NAME'` INT, `'PHOTO_GALLERY_ID'` INT, `'PHOTO_CREATOR'` INT, `'PHOTO_FULL_PATH'` INT, `'PHOTO_THUMBNAIL_PATH'` INT);

-- -----------------------------------------------------
-- Placeholder table for view `smbfat`.`CONTACTS_OPERATORS_VIEW`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `smbfat`.`CONTACTS_OPERATORS_VIEW` (`'CONTACT_SUBJECT'` INT, `'CONTACT_SUBJECT_OPERATOR_EMAIL'` INT);

-- -----------------------------------------------------
-- View `smbfat`.`MENU_FULL_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`MENU_FULL_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW `MENU_FULL_VIEW` AS
SELECT rootmenu.nazwa as 'ROOTMENU_NAME', submenu.nazwa as 'SUBMENU_NAME', submenu.lisc as 'SUBMENU_LEAF_ID', submenu.galeria  as 'SUBMENU_GALERY_ID', leafmenu.nazwa as 'LEAFMENU_NAME', leafmenu.lisc as 'LEAFMENU_LEAF_ID', leafmenu.galeria as 'LEAFMENU_GALERY_ID', leaf.nazwa as 'LEAF_NAME', leaf.lisc as 'LEAF_ARTICLE_ID', leaf.galeria as 'LEAF_GALERY_ID'
FROM smbfat.MENU AS rootmenu 
LEFT JOIN smbfat.MENU AS submenu ON rootmenu.id = submenu.rodzic 
LEFT JOIN smbfat.MENU AS leafmenu ON submenu.id = leafmenu.rodzic 
LEFT JOIN smbfat.MENU AS leaf ON leafmenu.id = leaf.rodzic 
where rootmenu.typ = 'ROOTMENU'
order by rootmenu.kolejnosc, submenu.kolejnosc, leafmenu.kolejnosc, leaf.kolejnosc;

-- -----------------------------------------------------
-- View `smbfat`.`ARTICLE_FULL_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`ARTICLE_FULL_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW `ARTICLE_FULL_VIEW` AS
    select 
        l.id as 'ARTICLE_ID',
        l.data_dodania as 'ARTICLE_CREATION_DATE',
        l.tytul as 'ARTICLE_TITLE',
        l.z_ostatniej_chwili as 'ARTICLE_IS_BRAKING_NEWS',
        l.przyklejony as 'ARTICLE_IS_STICKY',
        l.slider as 'ARTICLE_IS_IN_SLIDER',
        l.katalog as 'ARTICLE_CATALOG',
        l.nazwa_pliku as 'ARTICLE_FILE_NAME',
        z.nazwa as 'ARTICLE_PHOTO_NAME',
        g.zdjecia_miniatury_katalog as 'ARTICLE_PHOTO_THUMBNAIL_CATALOG',
        g.zdjecia_pelne_katalog as 'ARTICLE_PHOTO_CATALOG',
        u.imie_nazwisko as 'ARTICLE_AUTHOR_NAME',
        u.email as 'ARTICLE_AUTHOR_EMAIL'
    from
        smbfat.LISCE as l
            left join
        smbfat.ZDJECIA as z ON l.zdjecie = z.id
            left join
        smbfat.GALERIE as g ON g.id = z.galeria
            left join
        smbfat.UZYTKOWNICY as u ON l.dodal = u.id;

-- -----------------------------------------------------
-- View `smbfat`.`USER_FULL_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`USER_FULL_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW USER_FULL_VIEW AS
    SELECT 
        u.id AS 'USER_ID',
        u.imie_nazwisko AS 'NAME_SURNAME',
        u.login AS 'LOGIN',
        u.email AS 'EMAIL',
        u.ulica AS 'STREET',
        u.numer_domu_mieszkania AS 'HOME_NUMBER',
        u.miasto AS 'CITY',
		u.wojewodztwo AS 'VOIVODESHIP',
        u.rola_w_systemie AS 'SYSTEM_ROLE',
        u.haslo AS 'PASSWORD',
        z.nazwa AS 'USER_AVATAR_NAME',
        g.zdjecia_miniatury_katalog AS 'USER_AVATAR_THUMBNAIL_CATALOG',
        g.zdjecia_pelne_katalog AS 'USER_AVATAR_CATALOG'
    FROM
        smbfat.UZYTKOWNICY as u
            left join
        smbfat.ZDJECIA as z ON u.awatar = z.id
            left join
        smbfat.GALERIE as g ON g.id = z.galeria
    where
        u.aktwyny = 1
            and u.dane_osobowe_przetwarzanie_potwierdzone = 1
            and u.usuniety <> 1;

-- -----------------------------------------------------
-- View `smbfat`.`USER_GROUP_FULL_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`USER_GROUP_FULL_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW `USER_GROUP_FULL_VIEW` AS
    select 
        `uzdg`.`id_uzytkownika` AS `USER_ID`,
        `g`.`nazwa` AS `USER_GROUP_NAME`,
        `uzdg`.`rola_w_grupie` AS `USER_GROUP_ROLE`,
        `ga`.`przeznaczenie` AS `USER_GROUP_DESC`,
        `ga`.`id` AS `USER_GROUP_GALLERY_ID`,
        `ga`.`zdjecia_miniatury_katalog` AS `USER_GROUP_GALLERY_THUMBNAIL_CATALOG`,
        `ga`.`zdjecia_pelne_katalog` AS `USER_GROUP_GALLERY_CATALOG`
    from
        (`UZYTKOWNICY_ZDO_GRUPY` `uzdg`
        join (`GRUPY` `g`
        left join `GALERIE` `ga` ON ((`ga`.`nazwa_grupy` = `g`.`nazwa`))))
    where
        ((`uzdg`.`nazwa_grupy` = `g`.`nazwa`)
            and (`g`.`aktywna` = 1));

-- -----------------------------------------------------
-- View `smbfat`.`PHOTO_FULL_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`PHOTO_FULL_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW `PHOTO_FULL_VIEW` AS
    SELECT 
        z.id as 'PHOTO_ID',
        z.nazwa as 'PHOTO_NAME',
        z.typ as 'PHOTO_TYPE',
        z.data_dodania as 'PHOTO_ADD_DATE',
        g.przeznaczenie as 'PHOTO_GALLERY_DESC',
        g.nazwa_grupy as 'PHOTO_GALLERY_OWNER_GROUP_NAME',
        g.id as 'PHOTO_GALLERY_ID',
        u.login as 'PHOTO_CREATOR',
        CONCAT_WS('/', g.zdjecia_pelne_katalog, z.nazwa) as 'PHOTO_FULL_PATH',
        CONCAT_WS('/',
                g.zdjecia_miniatury_katalog,
                z.nazwa) as 'PHOTO_THUMBNAIL_PATH'
    FROM
        smbfat.ZDJECIA as z,
        smbfat.GALERIE as g,
        smbfat.UZYTKOWNICY as u
    where
        z.galeria = g.id and z.dodal = u.id;

-- -----------------------------------------------------
-- View `smbfat`.`CONTACTS_OPERATORS_VIEW`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `smbfat`.`CONTACTS_OPERATORS_VIEW`;
USE `smbfat`;
CREATE  OR REPLACE VIEW `CONTACTS_OPERATORS_VIEW` AS
    SELECT 
        k.temat as 'CONTACT_SUBJECT',
        u.email as 'CONTACT_SUBJECT_OPERATOR_EMAIL'
    FROM
        KONTAKTY as k,
        KONTAKTY_ZDO_ROLE_W_SYSTEMIE as kzrws,
        ROLE_W_SYSTEMIE as rws,
        UZYTKOWNICY as u
    where
        k.temat = kzrws.kontakt_temat
            and kzrws.rola_nazwa = rws.nazwa
            and rws.nazwa = u.rola_w_systemie;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
USE `smbfat`;

DELIMITER $$
USE `smbfat`$$
CREATE TRIGGER `LISCE_BINS` BEFORE INSERT ON `LISCE` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from LISCE);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from LISCE);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `MENU_BINS` BEFORE INSERT ON `MENU` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from MENU);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from MENU);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `GALERIE_BINS` BEFORE INSERT ON `GALERIE` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from GALERIE);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from GALERIE);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `ZDJECIA_BINS` BEFORE INSERT ON `ZDJECIA` 
FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from ZDJECIA);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from ZDJECIA);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `UZYTKOWNICY_BINS` BEFORE INSERT ON `UZYTKOWNICY` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from UZYTKOWNICY);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from UZYTKOWNICY);
	end if;
END
$$

USE `smbfat`$$
CREATE TRIGGER `OGLOSZENIA_BINS` BEFORE INSERT ON `OGLOSZENIA` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from OGLOSZENIA);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from OGLOSZENIA);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `KALENDARZ_BINS` BEFORE INSERT ON `KALENDARZ` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from KALENDARZ);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from KALENDARZ);
	end if;
END$$

USE `smbfat`$$
CREATE TRIGGER `PORTAL_BINS` BEFORE INSERT ON `PORTAL` FOR EACH ROW
BEGIN
	declare pdo_autoident_tmp integer;
	set pdo_autoident_tmp := (select max(pdo_autoident) from PORTAL);
	if (pdo_autoident_tmp is null)
	then
		SET NEW.pdo_autoident = 0;
	else
		SET NEW.pdo_autoident = (select max(pdo_autoident)+1 from PORTAL);
	end if;
END$$


DELIMITER ;

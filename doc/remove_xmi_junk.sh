 
#!/bin/bash
#
# ducheck.sh - Definition/Use Check for xmi.id's in Umbrello XMI files
#
# This script checks that all xmi.id's mentioned in
#    <UML:Generalization> child/parent
#    <UML:Dependency> client/supplier
#    <UML:Attribute> and <UML:Parameter> type
#    as well as all mentions of xmi.idref
# match up with the defining xmi.id.
# If no defining node can be found for the above xmi.id references
# then the script will report the line number(s) of the dangling reference.
# NOTE: This script only _checks_ the XMI file, it does not modify it
#       and also does not create a second XMI file where the problems are
#       automatically repaired. The repair must be done manually according
#       to the errors reported by the script.
#
# Usage:  ducheck.sh <xmifile>
#         Replace <xmifile> by your XMI file name.
#         If nothing is printed to stdout then no problems were found.
#
# Copyright (C) 2012, O. Kellogg <okellogg@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301.
#
perl -n -e '
  BEGIN {
    %iddef=();
    %idref=();
    sub push2 {
      my $linenum = shift;
      foreach my $ref (@_) {
        unless (exists $idref{$ref}) {
          $idref{$ref} = [];
        }
	push @{$idref{$ref}}, $linenum;
      }
    }
  }
  chop;
  if (/ (child|client)="(\w{12})".* (parent|supplier)="(\w{12})"/) {
    push2($., $2, $4);
  } elsif (/ (type|xmi.idref)="(\w{12})"/) {
    push2($., $2);
  }
  if (/ xmi\.id="(\w{12})"/) {
    $iddef{$1} = $.;
  }
  END {
    foreach my $id (keys %idref) {
      unless (exists $iddef{$id}) {
	my @linenums = @{$idref{$id}};
        print("dangling ref \"$id\" in line: " . join(" ", @linenums) . "\n");
      }
    }
  }' $*

